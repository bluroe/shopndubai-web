<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
	return [
		'name' => ucfirst($faker->word)
	];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
	$qties = [100, 125, 150, 25, 10, 5, 200, 250, 200, 400, 500, 1000];
	return [
		// 'category_id' => '',
		'name' => $faker->word,
		'description' => $faker->text,
		'qty' => $qties[rand(0, count($qties) - 1)],
		'purchase_price' => $faker->randomNumber(2),
		'price' => $faker->randomNumber(2),
		'mrp' => $faker->randomNumber(2),
		// 'min_qty' => '',
		// 'max_qty' => '',
		// 'delivery' => '',
		// 'active' => 1
	];
});

$factory->define(App\OrderItem::class, function (Faker\Generator $faker) {
	static $products;
	if(!$products) $products = \App\Product::with('variants')->get();
	$product = $products->random();
	$variants = $product->variants;
	$price = $variants->count() ? $variants->first()->price : rand(100, 500);
	// echo 'product_id:' . $product->id . ', variants:' . $product->variants->count() . "\n";
	return [
		'name' => $product->name,
		'price' => $price,
		'qty' => rand(1, 3)
	];
});

$factory->define(App\Order::class, function (Faker\Generator $faker) use ($factory) {
	return [
		'address_id' => $factory->create(App\Address::class)->id,
		'delivery_charge' => 10
	];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
	$areas = ['Dharmadam', 'Muzhappilangad', 'Chonadam', 'Thalassery', 'Gopalpeta', 'Saidarpalli', 'Punnol', 'Kathirur', 'Thiruvangad'];
	$pins = [670106,670101,670102,670103,670104,670105];
	return [
		'area' => $areas[array_rand($areas)],
		'firstname' => $faker->firstName,
		'lastname' => $faker->lastName,
		'pin' => $pins[array_rand($pins)],
		'phone' => rand(7000000000, 9999999999),
		'address1' => $faker->streetAddress,
		'city' => 'Thalassery'
	];
});

$factory->define(App\Image::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->title
	];
});