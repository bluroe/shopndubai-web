<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('settings')->delete();
        DB::table('settings')->insert([
        	['key' => 'delivery_charge', 'value' => 30],
        	['key' => 'delivery_free_above', 'value' => 1000]
        ]);
    }
}
