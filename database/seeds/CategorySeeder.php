<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->delete();
      $category = Category::create(['name' => 'Fish', 'fullname' => 'Fish']);
      $category->images()->create(['name' => 'fish.jpg', 'filename' => '/img/seed/categories/fish.jpg']);
      $category = Category::create(['name' => 'Dairy', 'fullname' => 'Dairy']);
      $category->images()->create(['name' => 'dairy.jpg', 'filename' => '/img/seed/categories/dairy.jpg']);
      $category = Category::create(['name' => 'Meat', 'fullname' => 'Meat']);
      $category->images()->create(['name' => 'meat.jpg', 'filename' => '/img/seed/categories/meat.jpg']);
      $category = Category::create(['name' => 'Vegetables', 'fullname' => 'Vegetables']);
      $category->images()->create(['name' => 'vegetables', 'filename' => '/img/seed/categories/vegetables.jpg']);
      $category = Category::create(['name' => 'Fruits', 'fullname' => 'Fruits']);
      $category->images()->create(['name' => 'fruits.jpg', 'filename' => '/img/seed/categories/fruits.jpg']);
      $category = Category::create(['name' => 'Snacks', 'fullname' => 'Snacks']);
      $category->images()->create(['name' => 'snacks.jpg', 'filename' => '/img/seed/categories/snacks.jpg']);
      $category = Category::create(['name' => 'Beverages', 'fullname' => 'Beverages']);
      $category->images()->create(['name' => 'beverages.jpg', 'filename' => '/img/seed/categories/beverages.jpg']);
      $category = Category::create(['name' => 'Cooking', 'fullname' => 'Cooking']);
      $category->images()->create(['name' => 'cooking.jpg', 'filename' => '/img/seed/categories/cooking.jpg']);
    }

}
