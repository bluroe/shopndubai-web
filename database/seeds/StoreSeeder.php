<?php

use Illuminate\Database\Seeder;
use App\Store;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('stores')->delete();
        $store = Store::create([
        	'name' => 'Azeez Stores',
        	'address' => 'Main Road',
        	'area' => 'Thalassery',
        	'pin' => '670101'
        ]);
        $store = Store::create([
            'name' => 'Madhavan Stores',
            'address' => 'Railway Station',
            'area' => 'Dharmadam',
            'pin' => '670106'
        ]);
    }
}
