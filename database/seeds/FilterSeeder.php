<?php

use Illuminate\Database\Seeder;

class FilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('filters')->delete();
      $filters = [
        ['name' => 'Color'],
        ['name' => 'Texture'],
        ['name' => 'Size'],
      ];
      DB::table('filters')->insert($filters);
    }
}
