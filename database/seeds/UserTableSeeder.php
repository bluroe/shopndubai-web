<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        App\User::create(['name' => 'Admin', 'email' => 'admin@info.com', 'password' => bcrypt('admin'), 'level' => 0]);
    }
}
