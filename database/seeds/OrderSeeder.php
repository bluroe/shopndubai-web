<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('orders')->delete();
        factory(App\Order::class, 50)->create()->each(function($o) {
        	$count = rand(2, 5);
        	// echo 'generating ' . $count . " order items\n";
        	$items = factory(App\OrderItem::class, $count)->make();
        	foreach($items as $item) {
        		$type = gettype($item);
        		// echo ($type == 'object' ? get_class($items) : $type . ' : ' . $o->id) . "\n";
        	}
        	$o->orderItems()->saveMany($items);
        	// echo "\n";
        });
    }
}
