<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\Category;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        $cat = Category::first();
        $products = [
            ['name' => 'Chocos', 'image' => '/img/seed/products/chocos.jpg'],
            ['name' => 'Corn Pops', 'image' => '/img/seed/products/corn-pops.jpg'],
            ['name' => 'Corn Flakes', 'image' => '/img/seed/products/corn-flakes.jpg'],
            ['name' => 'Boost', 'image' => '/img/seed/products/boost.jpg'],
            ['name' => 'Oreo Cookies', 'image' => '/img/seed/products/oreo.jpg'],
            ['name' => 'Coca Cola', 'image' => '/img/seed/products/cocacola.jpg'],
            ['name' => 'Pringles', 'image' => '/img/seed/products/pringles.jpg'],
            ['name' => 'American Garden Mayonnaise', 'image' => '/img/seed/products/mayonnaise.jpg'],
            ['name' => 'Quaker Oats', 'image' => '/img/seed/products/oats.jpg'],
            ['name' => 'Talbot Wafers']
        ];
        foreach($products as $p) {
            $price = rand(0, 500);
            $product = Product::create([
                'name' => $p['name'],
                'category_id' => $cat->id
            ]);
            if(isset($p['image'])) {
                $filename = strtolower(str_replace(' ', '-', $p['name']));
                $product->images()->create(['name' => $filename . '.jpg', 'filename' => $p['image']]);
            }
            $product->variants()->create([
                'price' => rand(0, 500),
                'mrp' => rand(0, 500),
                'purchase_price' => rand(0, 500),
                'type' => '500G'
            ]);
            if(rand(0, 3) == 1) {
                $product->variants()->create([
                    'price' => rand(0, 250),
                    'mrp' => rand(0, 250),
                    'purchase_price' => rand(0, 250),
                    'type' => '100G'
                ]);
            }
        }
        
        $cat = Category::where('name', 'Dairy')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Milma']),
            new Product(['name' => 'Milky Mist']),
            new Product(['name' => 'Butter']),
            new Product(['name' => 'Yoghurt', 'packet' => 0]),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Meat')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Chicken']),
            new Product(['name' => 'Beef', 'packet' => 0]),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Vegetables')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Carrot']),
            new Product(['name' => 'Beetroot']),
            new Product(['name' => 'Onion']),
            new Product(['name' => 'Potato', 'packet' => 0]),
            new Product(['name' => 'Tomato']),
            new Product(['name' => 'Bringal']),
            new Product(['name' => 'Cabbage', 'packet' => 0]),
            new Product(['name' => 'Beans']),
            new Product(['name' => 'Peas']),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Fruits')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Apple']),
            new Product(['name' => 'Orange']),
            new Product(['name' => 'Grape']),
            new Product(['name' => 'Gauva']),
            new Product(['name' => 'Pineapple']),
            new Product(['name' => 'Strawberry', 'packet' => 0]),
            new Product(['name' => 'Jackfruit']),
            new Product(['name' => 'Watermelon']),
            new Product(['name' => 'Lemon']),
            new Product(['name' => 'Mango']),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Snacks')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Lays']),
            new Product(['name' => 'Kurkure']),
            new Product(['name' => 'Cheetos']),
            new Product(['name' => 'Bingo']),
            new Product(['name' => 'Tiger Biscuit']),
            new Product(['name' => 'Hide N Seek']),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Beverages')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Appy Fizz']),
            new Product(['name' => 'Pepsi']),
            new Product(['name' => 'Coca Cola']),
            new Product(['name' => 'Miranda', 'packet' => 0]),
            new Product(['name' => 'Fanta']),
            new Product(['name' => 'Mountain Dew']),
            new Product(['name' => '7UP']),
        ]);
        $this->createVariants($cat);
        $cat = Category::where('name', 'Cooking')->first();
        $cat->products()->saveMany([
            new Product(['name' => 'Baking Powder']),
            new Product(['name' => 'Vanilla Essense']),
            new Product(['name' => 'Cake Flour']),
            new Product(['name' => 'Chilli Powder']),
            new Product(['name' => 'Cardamom']),
            new Product(['name' => 'Rice', 'packet' => 0]),
            new Product(['name' => 'Wheat', 'packet' => 0]),
            new Product(['name' => 'Barley']),
            new Product(['name' => 'Cinnamon']),
            new Product(['name' => 'Salt']),
            new Product(['name' => 'Sugar']),
        ]);

    }

    function createVariants($cat) {
        $units = ['kg', 'gm', 'l', 'ml'];
        $cat->products->each(function($product) use ($units) {
            $product->variants()->create([
                'price' => rand(0, 500),
                'mrp' => rand(0, 500),
                'purchase_price' => rand(0, 500),
                'type' => ($product->packet ? '500G' : $units[rand(0, 3)])
            ]);
            if(rand(0, 3) == 1) {
                $product->variants()->create([
                    'price' => rand(0, 250),
                    'mrp' => rand(0, 250),
                    'purchase_price' => rand(0, 250),
                    'type' => ($product->packet ? '100G' : $units[rand(0, 3)])
                ]);
            }
        });
    }


}
