<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();
            // foreign key on store migration
            $table->unsignedInteger('address_id');
            // foreign key on address migration
            $table->double('delivery_charge');
            $table->double('total');
            $table->integer('status')->default(0); // 0 ordered, 1 in transit, 2 delivered, 3 cancelled, 4 pending
            $table->timestamps();
        });
        Schema::create('order_items', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->unsignedInteger('product_id')->nullable();
            $table->string('name');
            $table->double('price');
            $table->double('qty');
            $table->integer('store_id')->nullable();
            $table->string('status')->default('unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
    }
}
