<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('otp', 4);
            $table->string('mobile', 10);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->string('mobile', 10)->after('active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn([
                'mobile',
            ]);
        });
        Schema::dropIfExists('otp');
    }
}
