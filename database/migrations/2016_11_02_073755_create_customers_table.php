<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 32);
            $table->string('geolocation')->nullable();
            $table->string('pin', 6)->nullable();
            $table->string('fcm_token', 152)->nullable(); // firebase token
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
        Schema::table('orders', function (Blueprint $table) { 
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_customer_id_foreign');
        });
        Schema::dropIfExists('customers');
    }
}
