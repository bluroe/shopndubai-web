<?php namespace App\Services;

use Illuminate\Support\Facades\Facade;

class StoreLocatorFacade extends Facade {

	protected static function getFacadeAccessor() {
		return 'storeLocator';
	}

}