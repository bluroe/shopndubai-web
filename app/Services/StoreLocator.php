<?php namespace App\Services;

use Geotools;
use App\Store;
use Illuminate\Database\Eloquent\Collection;

class StoreLocator {

	public function nearbyStores($pin) {
		return $this->findNearbyStores($pin);
	}

	public function nearbyStore($pin) {
		return $this->findNearbyStores($pin)->first();
	}

	private function findNearbyStores($pin) {
		return Store::where('pin', '=', $pin)->get();
	}

}