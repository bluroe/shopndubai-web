<?php namespace App\Services;

class OrderTask {

	public function updateOrder($order, $address, $request) {
		// debug
		$data = [
			'added' => [],
			'updated' => [],
			'removed' => [],
		];
		$order_items = $request->order_items;
		$products = \App\ProductVariant::whereIn('id', array_column($order_items, 'id'))->with('product')->get();
		foreach($order_items as $item) {
			$id = $item['id'];
			$existing = $order->orderItems->where('product_id', '=', $id)->first();
			if($existing) {
				// $data['updating'] = true;
 				$old_qty = $existing->qty;
				$existing->qty = $item['qty'];
				$existing->store_id = !empty($item['store_id']) ? $item['store_id'] : null;
				$existing->save();
				$data['updated'][] = $id;
			} else {
				// $data['adding'] = true;
				$product = $products->where('id', $id)->first();
				if(is_null($product)) continue;
				$order->orderItems()->save(new \App\OrderItem([
					'product_id' => $id,
					'name' => $product->product->name,
					'price' => $item['price'],
					'qty' => $item['qty'],
					'store_id' => !empty($item['store_id']) ? $item['store_id'] : null
				]));
				$data['added'][] = $id;
			}
		}
		$existing = $order->orderItems->pluck('product_id')->all();
		$incoming = array_column($order_items, 'id');
		$removed_items = array_values(array_diff($existing, $incoming));
		$order->orderItems()->whereIn('product_id', $removed_items)->delete();
		$data['removed'] = $removed_items;
		$order->update([
			'total' => $order->getOrderTotal(),
			'address_id' => $address->id,
			'delivery_charge' => $request->input('delivery_charge', 0)
		]);
		return $data;
	}

	public function processOrder($order_items, $address, $total, $customer = null, $delivery_charge = null) {
		$products = \App\ProductVariant::whereIn('id', array_column($order_items, 'id'))->with('product')->get();
		$items = new \Illuminate\Database\Eloquent\Collection;
		foreach($order_items as $item) {
			$id = $item['id'];
			$product = $products->where('id', $id)->first();
			$items->push(new \App\OrderItem([
				'product_id' => $id,
				'name' => $product->product->name . ' ' . $product->type,
				'price' => $product->price,
				'qty' => $item['qty'],
				'store_id' => isset($item['store_id']) && !empty($item['store_id']) ? $item['store_id'] : null
			]));
		}
		if(is_null($delivery_charge)) {
			$delivery_charge = $this->calculateDeliveryCharge($items);
		}
		$total = $items->reduce(function($carry, $product) {
			return $carry + ($product->price * $product->qty);
		}, 0);
		$order = \App\Order::create([
			'customer_id' => isset($customer) ? $customer->id : null,
			'address_id' => $address->id,
			'delivery_charge' => $delivery_charge,
			'status' => isset($customer) ? '4' : '0',
			'total' => $total
		]);
		$order->orderItems()->saveMany($items);
		return $order;
	}

	private function deliverySettings() {
		$settings = \DB::table('settings')->whereIn('key', ['delivery_charge', 'delivery_free_above'])->get();
		$data = [];
		foreach($settings as $s) {
			$data[$s->key] = $s->value;
		}
		return $data;
	}

	private function calculateTotal($items) {
		return $items->reduce(function($carry, $item) {
			return $carry + ($item->qty * $item->price);
		}, 0);
	}

	private function calculateDeliveryCharge($items) {
		return $this->calculateDeliveryChargeOfTotal($this->calculateTotal($items));
	}

	private function calculateDeliveryChargeOfTotal($total) {
		$delivery_settings = $this->deliverySettings();
		if($total >= (int) $delivery_settings['delivery_free_above']) {
			return 0;
		}
		return (int) $delivery_settings['delivery_charge'];
	}

	public function calculateDeliveryChargeOfItems($items) {
		$incoming = array_keys($items);
		$products = \App\ProductVariant::whereIn('id', $incoming)->get();
		if($products->count() != count($items)) {
			$found = $products->pluck('id')->all();
			return [
				'status' => 'error',
				'products_not_found' => array_diff($incoming, $found)
			];
		}
		$total = array_reduce(array_keys($items), function($carry, $item) use ($products, $items, &$data) {
			$product = $products->where('id', $item)->first();
			return $carry + $product->price * $items[$item];
		}, 0);
		return [
			'status' => 'ok',
			'total' => $total,
			'delivery_charge' => $this->calculateDeliveryChargeOfTotal($total)
		];
	}

}