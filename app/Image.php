<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

  protected $guarded = ['id'];

  public function categories() {
    return $this->morphedByMany('App\Category', 'imageable');
  }

  public function products() {
    return $this->morphedByMany('App\Product', 'imageable');
  }

}
