<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
    	'active' => 'boolean'
    ];

    public function scopeActive($q) {
    	return $q->where('active', 1);
    }
}
