<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $casts = [
        'qty' => 'double'
    ];
    
    protected $guarded = ['id'];

    public function order() {
    	return $this->belongsTo('App\Order');
    }

    public function productVariant() {
    	return $this->belongsTo('App\ProductVariant', 'product_id');
    }

	public function store() {
		return $this->belongsTo('App\Store');
	}

}
