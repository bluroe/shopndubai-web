<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends \Baum\Node
{
  protected $guarded = ['id'];

  public function scopeActive($query) {
    return $query->where('active', 1);
  }

  public function products() {
    return $this->hasMany('App\Product');
  }

  public function getImageAttribute() {
  	return $this->images->first();
  }

  public function images() {
    return $this->morphToMany('App\Image', 'imageable');
  }

  public function isAvailable() {
    $parent_available = $this->parent_id ? $this->parent->active : 1;
    return $parent_available && $this->active;
  }

}
