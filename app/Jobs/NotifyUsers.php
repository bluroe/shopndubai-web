<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUsers implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $address;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($address)
    {
        $this->address = $address;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = \App\User::all();
        foreach($users as $user) {
            // fcm store user
            $key = 'AAAA4e1JSgI:APA91bH5-OVgzLOUG_YvAN65WMu0ZetwTq7WFtKmhtbEottC4kK0Sd64YK6PP2fL33e8cqIzQNCT8tqduCc5FYur-7-UB0UYdEXPBBQ2WHBfK1V9CsHQ1CgGUWfAgKGInlIwjfKqKySqIW6K_m4ZEijgyI13_UrLaQ';
            $data = array(
                'to' => $user->fcm_token,
                'notification' => array(
                    'title' => 'New order',
                    'body' => 'New order from ' . $this->address,
                    'icon' => '/img/logo.png'
                )
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: key=' . $key
            ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $result = curl_exec($ch);
            // return $result;
        }
    }
}
