<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    public function category() {
      return $this->belongsTo('App\Category');
    }

    public function images() {
      return $this->morphToMany('App\Image', 'imageable');
    }

    public function filters() {
    	return $this->belongsToMany('App\Filter')->withPivot('options');
    }

    public function stores() {
        return $this->belongsToMany('App\Store')->withPivot('qty', 'price', 'active', 'reduce_qty');
    }

    public function store($id) {
        return $this->stores->filter(function($value, $key) use ($id) {
            return $value->id == $id;
        })->first();
    }

    public function variants() {
        return $this->hasMany('App\ProductVariant');
    }

}
