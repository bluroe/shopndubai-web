<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class CategoryType extends GraphQLType {

	protected $attributes = [
		'name' => 'Category',
		'description' => 'The categories'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the category'
			],
			'name' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The name of the category'
			],
			'images' => [
				'type' => Type::listOf(GraphQL::type('Image')),
				'description' => 'The image of the category'
			],
			'children' => [
				'type' => Type::listOf(GraphQL::type('Category')),
				'description' => 'The name of the category'
			],
			'products' => [
				'type' => Type::listOf(GraphQL::type('Product')),
				'description' => 'The products in the category'
			]
		];
	}

}