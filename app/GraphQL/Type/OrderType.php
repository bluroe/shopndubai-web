<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class OrderType extends GraphQLType {

	protected $attributes = [
		'name' => 'Order',
		'description' => 'The orders'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the order'
			],
			'status' => [
				'type' => Type::int(),
				'description' => 'Delivery status of the order'
			],
			'total' => [
				'type' => Type::float(),
				'description' => 'Total of the order'
			],
			'delivery_charge' => [
				'type' => Type::float(),
				'description' => 'Delivery charge of the order'
			],
			'orderItems' => [
				'type' => Type::listOf(GraphQL::type('OrderItem')),
				'description' => 'Items in the order'
			]
		];
	}

}