<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class OrderItemType extends GraphQLType {

	protected $attributes = [
		'name' => 'OrderItem',
		'description' => 'Items in an order'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the item'
			],
			'name' => [
				'type' => Type::string(),
				'description' => 'The name of the item'
			],
			'price' => [
				'type' => Type::float(),
				'description' => 'Sold price of the item'
			],
			'qty' => [
				'type' => Type::int(),
				'description' => 'Quantity of the item'
			],
			'product_id' => [
				'type' => Type::int(),
				'description' => 'Original product id'
			],
			'image' => [
				'type' => Type::string(),
				'description' => 'Image of the item',
				'resolve' => function($root, $args) {
					if(isset($root->productVariant->product->images) && $root->productVariant->product->images->count()) {
						return $root->productVariant->product->images->first()->filename;
					}
					return null;
				}
			]
		];
	}

}