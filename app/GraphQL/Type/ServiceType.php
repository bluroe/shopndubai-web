<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class ServiceType extends GraphQLType {

	protected $attributes = [
		'name' => 'Service',
		'description' => 'The services'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the service'
			],
			'name' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The name of the service'
			],
			'available' => [
				'type' => Type::int(),
				'description' => 'Currently available units'
			],
			'total' => [
				'type' => Type::int(),
				'description' => 'The total amount of providers in units'
			],
			'area' => [
				'type' => Type::string(),
				'description' => 'The area of service'
			]
		];
	}

}