<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class VariantType extends GraphQLType {

	protected $attributes = [
		'name' => 'Variant',
		'description' => 'Variants of a product'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the variant'
			],
			'type' => [
				'type' => Type::string(),
				'description' => 'Type of the variant'
			],
			'price' => [
				'type' => Type::float(),
				'description' => 'Sale price of the variant'
			],
			'purchase_price' => [
				'type' => Type::float(),
				'description' => 'Purchase price of the variant'
			],
			'mrp' => [
				'type' => Type::float(),
				'description' => 'MRP of the variant'
			],
			'offer' => [
				'type' => GraphQL::type('Offer'),
				'description' => 'Offers of the variant'
			],
		];
	}

}