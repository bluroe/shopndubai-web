<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ImageType extends GraphQLType {

	protected $attributes = [
		'name' => 'Image',
		'description' => 'The images'
	];

	public function fields() {
		return [
			'filename' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The filename of the image'
			]
		];
	}

}