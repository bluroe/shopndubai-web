<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;

class ProductType extends GraphQLType {

	protected $attributes = [
		'name' => 'Product',
		'description' => 'The products'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the product'
			],
			'name' => [
				'type' => Type::string(),
				'description' => 'Name of the product'
			],
			'description' => [
				'type' => Type::string(),
				'description' => 'Description of the product'
			],
			'variants' => [
				'type' => Type::listOf(GraphQL::type('Variant')),
				'description' => 'Variants of the product'
			],
			'images' => [
				'type' => Type::listOf(GraphQL::type('Image')),
				'description' => 'Images of the product'
			],
			'category' => [
				'type' => GraphQL::type('Category'),
				'description' => 'Category of the product'
			],
			'packet' => [
				'type' => Type::boolean(),
				'description' => 'Whether the product comes in packets, boxes, bottles or any other packaging'
			],
			'offer' => [
				'type' => Type::string(),
				'description' => 'Default offer text'
			],
			'tags' => [
				'type' => Type::string(),
				'description' => 'Search tags for the given product'
			]
		];
	}

}