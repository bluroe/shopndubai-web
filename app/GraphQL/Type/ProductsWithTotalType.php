<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use GraphQL;


class ProductsWithTotalType extends GraphQLType {

    protected $attributes = [
        'name' => 'ProductsWithTotal',
        'description' => 'The products'
    ];

    public function fields() {
        return [
            'items' => [
                'type' => Type::listOf(GraphQL::type('Product')),
                'description' => 'The products',
                'resolve' => function($root) {
                    return $root['items'];
                }
            ],
            'total' => [
                'type' => Type::int(),
                'description' => 'The total products count',
                'resolve' => function($root) {
                    return $root['count'];
                }
            ],
        ];
    }

}