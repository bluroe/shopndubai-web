<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class OfferType extends GraphQLType {

	protected $attributes = [
		'name' => 'Offer',
		'description' => 'The offers'
	];

	public function fields() {
		return [
			'text' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The text of the offer'
			],
			'qty' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The qty of the offer'
			],
			'price' => [
				'type' => Type::float(),
				'description' => 'The price of the offer'
			],
			'add_qty' => [
				'type' => Type::int(),
				'description' => 'The add_qty of the offer'
			]
		];
	}

}