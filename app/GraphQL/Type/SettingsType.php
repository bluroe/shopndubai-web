<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class SettingsType extends GraphQLType {

	protected $attributes = [
		'name' => 'Settings',
		'description' => 'Global settings'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::int()),
				'description' => 'The id of the settings'
			],
			'key' => [
				'type' => Type::string(),
				'description' => 'key of the setting'
			],
			'value' => [
				'type' => Type::string(),
				'description' => 'value of the setting'
			]
		];
	}

}