<?php namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Category;

class CategoriesQuery extends Query {

	protected $attributes = [
		'name' => 'categories'
	];

	public function type() {
		return Type::listOf(GraphQL::type('Category'));
	}

	public function args() {
		return [
			'id' => ['name' => 'id', 'type' => Type::int()],
			'depth' => ['name' => 'depth', 'type' => Type::int()],
			'name' => ['name' => 'name', 'type' => Type::int()],
		];
	}

	public function resolve($root, $args) {
		$query = Category::active();
		if(isset($args['id'])) {
			$query->where('id', $args['id']);
		}
		if(isset($args['depth'])) {
			$query->where('depth', $args['depth']);
		}
		if(isset($args['name'])) {
			$query->where('name', 'like', '%' . $args['name'] . '%');
		}
		$query->orderBy('order');
		return $query->get();
	}

}