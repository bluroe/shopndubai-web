<?php namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Service;

class ServicesQuery extends Query {

	protected $attributes = [
		'name' => 'services'
	];

	public function type() {
		return Type::listOf(GraphQL::type('Service'));
	}

	public function args() {
		return [
			'id' => ['name' => 'id', 'type' => Type::int()]
		];
	}

	public function resolve($root, $args) {
		$query = Service::active();
		return $query->get();
	}

}