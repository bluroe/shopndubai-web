<?php namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Product;
use App\Category;

class ProductsQuery extends Query {

	protected $attributes = [
		'name' => 'products'
	];

	public function type() {
		return GraphQL::type('ProductsWithTotal');
	}

	public function args() {
		return [
			'id' => ['name' => 'id', 'type' => Type::int()],
			'category_id' => ['name' => 'category_id', 'type' => Type::int()],
			'name' => ['name' => 'name', 'type' => Type::string()],
			'offset' => ['name' => 'offset', 'type' => Type::int()],
			'limit' => ['name' => 'limit', 'type' => Type::int()],
        	'no_images' => ['name' => 'no_images', 'type' => Type::boolean()],
        	'inactive' => ['name' => 'inactive', 'type' => Type::boolean()]
		];
	}

	public function resolve($root, $args) {
		if(isset($args['inactive'])) {
			$query = Product::where('active', 0);
		} else {
			$query = Product::active();
		}
		if(isset($args['no_images'])) {
			$query->doesntHave('images');
		}
		if(isset($args['id'])) {
			$query->where('id', $args['id']);
		}
		if(isset($args['category_id'])) {
			$cats = Category::find($args['category_id'])->descendantsAndSelf()->pluck('id')->all();
			$query->whereIn('category_id', $cats);
		}
		if(isset($args['name'])) {
			$query->where(function($q) use ($args) {
				$tokens = explode(' ', $args['name']);
				foreach($tokens as $token) {
					$q->where('name', 'like', '%' . $token . '%');
					$q->orWhere('tags', 'like', '%' . $token . '%');
				}
			});
		}
		$count = $query->count();
		if(isset($args['offset'])) {
			$query->skip($args['offset']);
		}
		if(isset($args['offset']) || isset($args['limit'])) {
			$query->take(array_get($args, 'limit', 10));
		}
		$query->orderBy('id', 'desc');
		return ['items' => $query->get(), 'count' => $count];
	}

}