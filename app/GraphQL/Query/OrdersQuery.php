<?php namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Order;
use App\Customer;

class OrdersQuery extends Query {

	protected $attributes = [
		'name' => 'orders'
	];

	public function type() {
		return Type::listOf(GraphQL::type('Order'));
	}

	public function args() {
		return [
			'id' => ['name' => 'id', 'type' => Type::int()],
			'status' => ['name' => 'status', 'type' => Type::int()],
			'customer' =>['name' => 'customer', 'type' => Type::string()], 
		];
	}

	public function resolve($root, $args) {
		$query = Order::query();
		$query->with('orderItems.productVariant.product.images');
		if(isset($args['id'])) {
			$query->where('id', $args['id']);
		}
		if(isset($args['status'])) {
			$query->where('status', $args['status']);
		}
		if(isset($args['customer'])) {
			$customer = Customer::where('key', $args['customer'])->first();
			if($customer) {
				$id = $customer->id;
				$query->where('customer_id', $id);
			}
		}
		$query->orderBy('id', 'desc');
		return $query->get();
	}

}