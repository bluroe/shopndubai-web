<?php namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use DB;

class SettingsQuery extends Query {

	protected $attributes = [
		'name' => 'settings'
	];

	public function type() {
		return Type::listOf(GraphQL::type('Settings'));
	}

	public function args() {
		return [
			'key' => ['name' => 'key', 'type' => Type::string()],
		];
	}

	public function resolve($root, $args) {
		$query = DB::table('settings');
		if(isset($args['key'])) {
			$query->where('key', $args['key']);
		}
		return $query->get();
	}

}