<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StoreLocatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('storeLocator', function() {
            return new \App\Services\StoreLocator;
        });
    }
}
