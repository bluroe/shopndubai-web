<?php namespace App\Transformers;

use League\Fractal;

class CustomerTransformer extends Fractal\TransformerAbstract {

	public function transform(\App\Customer $customer) {
		$data = $customer->toArray();
		if($customer->relationLoaded('orders')) {
			$data['orders'] = $customer->orders->count();
		}
		if($customer->relationLoaded('addresses')) {
			$data['addresses'] = $customer->orders->count();
		}
		return $data;
	}

}