<?php namespace App\Transformers;

use League\Fractal;
use App\Product;

class ProductTransformer extends Fractal\TransformerAbstract {

	protected $images;

	public function __construct($images = false) {
		$this->images = $images;
	}

	public function transform(Product $product) {
		$data = $product->toArray();
		if($product->relationLoaded('stores')) {
			$store = $product->stores->first();
			if($store) {
				$data['qty'] = $store->pivot->qty;
				$data['price'] = $store->pivot->price;
				$data['reduce_qty'] = $store->pivot->reduce_qty; // TODO : remove from customer
			}
			unset($data['stores']);
		}
		if($product->relationLoaded('variants')) {
			foreach($data['variants'] as $idx => $variant) {
				if(isset($variant['offer'])) {
					$data['variants'][$idx]['price'] = $variant['offer']['price'];
					unset($data['offer']);
				}
			}
		}
		$data['category'] = $product->category->name;
		if($this->images) {
			$data['image'] = $product->images->count() ? $product->images->first()->filename : '';
			unset($data['images']);
		}
		return $data;
	}

}