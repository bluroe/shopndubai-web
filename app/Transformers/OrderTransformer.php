<?php namespace App\Transformers;

use League\Fractal;

class OrderTransformer extends Fractal\TransformerAbstract {

	public function transform(\App\Order $order) {
		$data = $order->toArray();
		$data['order_items'] = array_map(function($orderItem) {
			$orderItem['id'] = array_get($orderItem, 'product_id', '');
			$orderItem['store'] = array_get($orderItem, 'store.name', '');
			$orderItem['image'] = array_get($orderItem, 'product_variant.product.images.0.filename', '');
			$orderItem['setType'] = array_get($orderItem, 'product_variant.type', '');
			$orderItem['setUnit'] = array_get($orderItem, 'product_variant.qty', '');
			$orderItem['type'] = array_get($orderItem, 'product_variant.type', '');
			unset($orderItem['product_variant']);
			return $orderItem;
		}, $data['order_items']);
		return $data;
	}

}