<?php namespace App\Transformers;

use League\Fractal;

class CategoryTreeTransformer extends Fractal\TransformerAbstract {

	public function transform($category) {
		$data = $category->toArray();
		return $this->crawl(array_values($data)[0]);
	}

	private function crawl($cat) {
		$c['name'] = $cat['name'];
		if(isset($cat['children'])) {
			$ch = [];
			foreach($cat['children'] as $child) {
				$ch[] = $this->crawl($child);
			}
			if(count($ch)) $c['children'] = $ch;
		}
		return $c;
	}

}