<?php namespace App\Transformers;

use League\Fractal;

class OrderItemsTransformer extends Fractal\TransformerAbstract {

	public function transform(\App\OrderItem $orderItem) {
		$data = $orderItem->toArray();
		$data['image'] = isset($orderItem->product->images) ? $orderItem->product->images->first()->filename : '';
		unset($data['product']);
		return $data;
	}

}