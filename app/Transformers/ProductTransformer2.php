<?php namespace App\Transformers;

use League\Fractal;
use App\Product;

class ProductTransformer2 extends Fractal\TransformerAbstract {

	public function transform(Product $product) {
		$data = $product->toArray();
		if($product->relationLoaded('stores')) {
			$store = $product->stores->first();
			if($store) {
				$data['qty'] = $store->pivot->qty;
				$data['price'] = $store->pivot->price;
				$data['reduce_qty'] = $store->pivot->reduce_qty; // TODO : remove from customer
			}
			unset($data['stores']);
		}
		$data['image'] = $product->images->count() ? $product->images->first()->filename : '';
		unset($data['images']);
		return $data;
	}

}