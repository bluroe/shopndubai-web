<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Image;
use App\Transformers\ProductTransformer;
use App\Transformers\ProductTransformer2;
use App\Store;
use App\ProductVariant;
use DB;

class ProductController extends Controller
{

  protected $rules = [
    'name' => 'required|string',
    'category_id' => 'required|numeric',
    'active' => 'required|boolean',
    'variant' => 'required'
  ];

  public function __construct() {
    $this->middleware('admin.privilege', ['only' => ['destroy']]);
  }

  public function index(Request $request) {
    if($request->ajax()) {
      $relations = ['category', 'variants.offer']; // to eager load
      $query = Product::query();
      if($request->q) { // filter names
        $tokens = explode(' ', $request->q);
        if(in_array('no:image', $tokens)) {
          $query->doesntHave('images');
          unset($tokens[array_search('no:image', $tokens)]);
        }
        $query->where(function($q) use ($request, $tokens) {
          foreach($tokens as $token) {
            $q->where('name', 'like', "%{$token}%");
          }
        });
        // $query->where('name', 'like', "%{$request['q']}%");
        $query->orWhereHas('category', function($query) use ($request) {
            $query->where('name', 'like', "%{$request['q']}%");
        });
      }
      if($request->has('status')) {
        $query->where('active', $request->status == 'active' ? 1 : 0);
      }
      $count = $query->count();
      if(!$request->has('fetch_all')) {
        $query->take($request->get('limit', 10)); 
      }
      if($request->offset) { // offset
        $query->skip($request->offset);
      }
      $images = false;
      if($request->thumb) {
        array_push($relations, 'images');
        $images = true;
      }
      $query->with($relations);
      $query->orderBy('id', 'desc');
      $data = fractal($query->get(), new ProductTransformer($images))->toArray();
      $data['count'] = $count;
      return $data;
      // return [];
      // return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    } else {
      return view('admin.product.index');
    }
  }

  public function stores(Product $product) {
    $product->load('stores');
    return $product;
  }

  public function show(Product $product, Request $request) {
    if($request->store) {
      $store = $request->store;
      // return $product->stores;
      // $product->load('stores');
      $product->load(['stores' => function($query) use ($store) {
        $query->whereId($store);
      }]); 
    }
    return response()->json($product, 200, [], JSON_PRETTY_PRINT);
    return $product;
  }

  public function create(Request $request) {
    $request->session()->put('q', $request->get('q'));
    $stores = Store::all();
    $categories = Category::all()->pluck('fullname', 'id')->all();
    return view('admin.product.form', compact('categories', 'stores'));
  }


  public function store(Request $request) {
    $this->validate($request, $this->rules);
    $data = $request->only('name', 'description', 'category_id', 'min_qty', 'tags', 'offer', 'active', 'packet');
    $data['packet'] = $request->packet ? "1" : "0";
    foreach($data as $key => $value) {
      if($value == "") {
        unset($data[$key]);
      }
    }
    $product = Product::create($data);
    if($request->variant) {
      $variants = $request->variant;
      foreach($variants as $variant) {
        $dataunit['product_id'] = $product->id;
        $dataunit['type'] = $variant['type'];
        $dataunit['purchase_price'] = $variant['purchase_price'];
        $dataunit['price'] = $variant['price'];
        $dataunit['mrp'] = $variant['mrp'];
        $prod_variant = ProductVariant::create($dataunit);

      }
    }
    if($request->images) {
      $images = Image::whereIn('id', $request->images)->get();
      $product->images()->saveMany($images);
    }
    if($request->filters) {
      $product->filters()->sync([]);
      foreach($request->filters as $id => $value) {
        $product->filters()->attach([$id => ['options' => json_encode($value)]]);
      }
    }
    if($request->stores) {
      $stores = $request->stores;
      Store::whereIn('id', array_keys($stores))->each(function($store) use ($product, $stores) {
        $values = $stores[$store->id];
        if($values['qty'] && $values['price']) {
          $store->products()->syncWithoutDetaching([$product->id => $values]); 
        }
      });
    }
    $params = ['q' => $request->session()->get('q', '')];
    return redirect()->route('products.index', $params)->withSuccess('Product created');
  }

  public function edit(Product $product, Request $request) {
    $request->session()->put('q', $request->get('q'));
    $request->session()->put('page', $request->get('page'));
    // $product->load('filters', 'images');
    // return response()->json($product, 200, [], JSON_PRETTY_PRINT);
    $product->load('stores');
    $stores = Store::all();
    $categories = Category::all()->pluck('fullname', 'id')->all();
    $variants = ProductVariant::where('product_id', $product->id)->get();
    return view('admin.product.form', compact('product', 'categories', 'stores', 'variants'));
  }

  public function update(Request $request, Product $product) {
    // return response()->json($request->all(), 200, [], JSON_PRETTY_PRINT);
    $this->validate($request, $this->rules);

    $data = $request->only('name', 'description', 'category_id', 'min_qty', 'tags', 'offer', 'active', 'packet');
    $data['packet'] = $request->packet ? "1" : "0";
    // dont pass empty string for int columns
    foreach(array_only($data, ['min_qty']) as $key => $value) {
      if($value == "") {
        unset($data[$key]);
      }
    }
    $product->update($data);
    // delete variants
    $exist_variants = $product->variants->pluck("id")->all();
    $income_variants = array_pluck($request->variant, "id");
    $delete_variant = array_values(array_diff($exist_variants, $income_variants));
    ProductVariant::whereIn("id", $delete_variant)->delete();
    // update variants
    $update_variants = array_filter($request->variant, function($value) {
      return !empty($value['id']);
    });
    foreach($update_variants as $v) {
      DB::table('product_variants')
        ->where('id', $v['id'])
        ->update(array_only($v, ['type', 'purchase_price', 'price', 'mrp']));
    }
    // create variants
    $create_variants = array_filter($request->variant, function($value) {
      return empty($value['id']);
    });
    foreach($create_variants as $v) {
      $product->variants()->create($v);
    }
    $product->images()->sync($request->input('images', []));
    $product->filters()->sync([]);
    if($request->filters) {
      foreach($request->filters as $id => $value) {
        $product->filters()->attach([$id => ['options' => json_encode($value)]]);
      }
    }
    $params = ['q' => $request->session()->get('q', '')];
    $params = ['page' => $request->session()->get('page', '')];
    return redirect()->route('products.index', $params)->withSuccess('Product updated');
  }

  public function destroy(Product $product) {
    $product->delete();
    return ['status' => 'ok'];
  }

  public function images(Product $product) {
    return $product->images;
  }

}
