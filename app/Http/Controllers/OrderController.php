<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Store;
use App\Customer;
use App\Product;
use App\ProductVariant;
use App\OrderItem;
use App\StoreOrder;
use App\Address;
use StoreLocator;
use App\Services\Messenger;
use App\Coupon;
use App\Transformers\OrderTransformer;

class OrderController extends Controller
{

	protected $rules = [
		'firstname' => 'required',
		'lastname' => 'required',
		'address1' => 'required',
		'area' => 'required',
		'city' => 'required',
		'phone' => 'required',
		'pin' => 'required|numeric',
		'order_items' => 'required|array'
	];

	public function index(Request $request) {
		if($request->ajax()) {
			$orders = Order::with('address', 'orderItems.store');
			if($request->get('type') != null) {
				$orders->where('status', $request->type);
			}
			if($request->q) {
				$q = $request->q;
				if(strpos($request->q, '#') !== false) {
					$orders->where('id', str_replace('#', '', $request->q));
				} else {
					$orders->whereHas('address', function($query) use ($q){
						$query->where('firstname', 'like', '%' . $q . '%');
						$query->orWhere('address1', 'like', '%' . $q . '%');
						$query->orWhere('address2', 'like', '%' . $q . '%');
						$query->orWhere('lastname', 'like', '%' . $q . '%');
						$query->orWhere('area', 'like', '%' . $q . '%');
						$query->orWhere('city', 'like', '%' . $q . '%');
						$query->orWhere('phone', 'like', '%' . $q . '%');
						$query->orWhere('pin', 'like', '%' . $q . '%');
					});
				}
			}
			if($request->product) {
				$variants = ProductVariant::whereIn('product_id', $request->product)->pluck('id')->all();
				$orders->whereHas('orderItems', function($query) use($variants){
					$query->whereIn('product_id', $variants);
				});
			}
			if($request->startDate) {
				$s = $request->startDate;
				$e = $request->endDate;
				$orders->whereDate('created_at', '>=', $s)->whereDate('created_at', '<=', $e);
			}
			$count = $orders->count();
			if($request->get('orderby') == 'latest') {
				$orders->orderBy('id', 'desc');
			}
			if($request->has('offset')) {
				$orders->skip($request->offset);
			}
			$orders->take($request->input('limit', 10));
			return ['orders' => $orders->get(), 'length' => $count];
		}
		return view('admin.orders.index');
	}

	public function show(Order $order) {
		$order->load('orderItems.store');
		// return response()->json($order, 200, [], JSON_PRETTY_PRINT);
		return view('admin.orders.show', compact('order'));
	}

	public function create(Request $request) {
		// $customers = Customer::pluck('key', 'id')->all();
		$stores = Store::all();
		// return view('admin.orders.form', ['customers' => $customers, 'stores' => $stores, 'statuses' => $this->statuses]);
		// return $stores;
		return view('admin.orders.form2', compact('stores'));
	}

	public function store(Request $request) {
		$this->validate($request, $this->rules);
		$address_data = $request->only('firstname', 'lastname', 'address1', 'address2', 'area', 'city', 'phone', 'pin');
		$items = $request->order_items;
		$total = $request->total;
		$address = Address::firstOrCreate($address_data);

		$orderTask = new \App\Services\OrderTask;
		$order = $orderTask->processOrder($items, $address, $total, null, $request->delivery_charge);

		$phone = $request->phone;
		$delivery_charge = $order->delivery_charge;
		$this->msgCoupon($phone, $total, $delivery_charge);
		return ['status' => 'ok'];
		return redirect()->route('orders.index')->withSuccess('Order created');
	}

	public function edit(Order $order) {
		$stores = Store::all();
		$order->load('orderItems.store', 'orderItems.productVariant.product.images', 'address');
		$order = fractal($order, new OrderTransformer)->toArray();
		$order = $order['data'];
		// return response()->json($order, 200, [], JSON_PRETTY_PRINT);
		// return response()->json($order['order_items'], 200, [], JSON_PRETTY_PRINT);
		return view('admin.orders.form2', compact('order', 'stores'));
	}

	public function update(Request $request, Order $order) {
		// return $request->all();
		$this->validate($request, $this->rules);
		$address_data = $request->only('firstname', 'lastname', 'address1', 'address2', 'area', 'city', 'phone', 'pin');
		$address = Address::firstOrCreate($address_data);

		$task = new \App\Services\OrderTask;
		$ret = $task->updateOrder($order, $address, $request);
		// return $ret; // debug
		return ['status' => 'ok'];
		return redirect()->route('orders.index')->withSuccess('Order updated');
	}

	public function destroy($order) { 
		Order::destroy(explode(',', $order));
		return redirect()->route('orders.index')->withSuccess('Order deleted');
	}

	public function products(Order $order) {
		return ['status' => 'success', 'products' => $order->products];
	}

	public function changeProduct(Order $order, Product $product, Request $request) {
		$data = $request->only('qty');
		$order->products()->syncWithoutDetaching([$product => $data]);
		return ['status' => 'success'];
	}

	public function removeProduct(Order $order, Product $product) {
		$order->products()->detach($product->id);
		return ['status' => 'success'];
	}

	public function latestOrder() {
		$order = Order::last();
		return $order;
	}

	// fetch the sub orders of a main order
	public function storeOrders(Order $order) {
		return $order->storeOrders()->with('store')->get();
	}

	public function orderItems(Order $order) {
		$orderItems =  $order->orderItems()->with('product.images')->get();
		$data = fractal($orderItems, new \App\Transformers\OrderItemsTransformer);
		return $data;
	}

	public function updateOrderStatus(Order $order, Request $request) {
		$exs_status = $order->status;
		if($exs_status == "4" && $request->status == "0") {
			$phone = $order->address->phone;
			$total = $order->total;
			$delivery_charge = $order->delivery_charge;
			$total = $total - $delivery_charge; 
			$this->msgCoupon($phone, $total, $delivery_charge);
		}
		$order->update($request->only('status'));
		return ['status' => 'ok'];
	}

	public function cancelOrder(Order $order) {
		$order->update(['status' => 3]);
		return ['status' => 'ok'];
	}

	public function storeSale(Request $request) {
		if($request->ajax()) {
			$products = Product::where('active', true);
			$sales = OrderItem::where('store_id', $request->store);
			if($request->q) {
				$sales->where('order_id', $request->q);
			}
			if($request->get('type') != null) {
				$sales->where('status', $request->type);
			}
			if($request->get('orderby') == 'latest') {
				$sales->orderBy('created_at', 'desc');
			}
			if($request->startDate) {
				$s = $request->startDate;
				$e = $request->endDate;
				$sales->whereDate('created_at', '>=', $s)->whereDate('created_at', '<=', $e);
			}
			if($request->product) {
				$p = $request->product;
				$sales->whereIn('product_id', $p);
			}
			return ['sales' => $sales->get(), 'products' => $products->get()];
		}
		return view('admin.stores.sale');
	}

	public function updateStatus(Request $request) {
		OrderItem::where('id', $request->id)
            ->update(['status' => $request->status]);
		return "success";
	}

	public function generateCoupons()
    {
    	$count = 1;
    	$length = 6;
        $coupons = [];
        while(count($coupons) < $count) {
            do {
                $coupon = strtoupper(str_random($length));
            } while (in_array($coupon, $coupons));
            $coupons[] = $coupon;
        }

        $existing = Coupon::whereIn('coupon_no', $coupons)->count();
        if ($existing > 0)
            $coupons += $this->generateCoupons($existing, $length);

        return (count($coupons) == 1) ? $coupons[0] : $coupons;
    }

    public function resendMessage(Order $order) {
    	$this->msgCoupon($order->address->phone, $order->total, $order->delivery_charge);
    	return ['status' => 'ok'];
    }

    public function msgCoupon($phone, $total, $delivery_charge) {
    	$existing = Coupon::where('phone', $phone)->count();
		if(!$existing) {
			$coupon = $this->generateCoupons();
			//SMS after order confirmation
			$coupon_data['phone'] = $phone;
			$coupon_data['coupon_no'] = $coupon;
			Coupon::firstOrCreate($coupon_data);
		}
		$delivery_text = $delivery_charge ? " + " . $delivery_charge . "rs (delivery charge)" : "rs";
		$message = "Your order from Tely Bazaar has confirmed. Kindly pay " . $total . $delivery_text . " at the time of delivery.";
		if(!$existing) {
			$message .= " Use coupon no: " . $coupon . " for discount on next purchase.";
		}
		$messenger = new Messenger;
		$messenger->sendMessage($phone, $message);
    }

    public function saveStore(Request $request) {
    	foreach($request->orderStore as $key=>$value) {
    		OrderItem::where('id', $key)->update(['store_id' => $value]);
    	}
    	return "success";
    }

}
