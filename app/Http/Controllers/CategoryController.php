<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Image;
use App\Transformers\CategoryTreeTransformer;

class CategoryController extends Controller
{

  protected $rules = [
    'name' => 'required',
  ];

  public function index(Request $request) {
    if($request->ajax()) {
      $query = Category::with('children', 'products');
      if($request->q) {
        $query->where('name', 'like', "%{$request['q']}%");
      }
      $count = $query->count();
      $query->take($request->get('limit', 10));
      if($request->offset) { // offset
        $query->skip($request->offset);
      }
      return ['categories' => $query->get(), 'count' => $count];
    } else {
      return view('admin.category.index');
    }
  }

  public function tree() {
    $roots = Category::roots()->get();
    $tree = [];
    foreach($roots as $root) {
      $tree[] = $root->getDescendantsAndSelf()->toHierarchy();
    }
    $data = fractal($tree, new CategoryTreeTransformer)->toArray();
    // return $data;
    return response()->json($data, 200, [], JSON_PRETTY_PRINT);
  }

  public function create() {
    $images = Image::all();
    $categories = Category::all()->pluck('fullname', 'id')->all();
    return view('admin.category.form', compact('categories', 'images'));
  }

  public function store(Request $request) {
    $this->validate($request, $this->rules);
    $data = $request->only('name', 'parent_id', 'order', 'active');
    if($data['order'] == '') unset($data['order']);
    // remove null values problem
    // foreach(array_except($data, 'parent_id') as $key => $value) {
    //   if(is_null($value)) {
    //     // echo 'popping ' . $key . '<br>';
    //     unset($data[$key]);
    //   }
    // }
    // return 'exit';
    $this->setFullName($request, $data);
    // return $data;
    $category = Category::create($data);
    $image = Image::find($request->image);
    if($image) {
      $category->images()->save($image);
    }
    return redirect()->route('categories.index')->withSuccess('Category created');
  }

  public function show(Category $category) {
    $category->load('images');
    return response()->json($category, 200, [], JSON_PRETTY_PRINT);
  }

  public function edit(Category $category) {
    $categories = Category::all()->pluck('fullname', 'id')->all();
    // return $category;
    return view('admin.category.form', compact('category', 'categories'));
  }

  public function update(Category $category, Request $request) {
    // return $request->only('name', 'parent_id', 'active');
    $this->validate($request, $this->rules);
    $data = $request->only('name', 'parent_id', 'order', 'active');
    if($data['order'] == '') $data['order'] = null;
    $this->setFullName($request, $data);
    $category->update($data);
    $image = Image::find($request->image);
    if($image) {
      $category->images()->sync([]);
      $category->images()->save($image);
    }
    return redirect()->route('categories.index')->withSuccess('Category updated');
  }

  private function setFullName($request, &$data) {
    if(!$data['parent_id']) {
      $data['parent_id'] = null;
    }
    $parent = Category::find($request->parent_id);
    if($parent) {
      $names = [$parent->name, $request->name];
      $cur = $parent;
      while($cur->parent_id) {
        array_unshift($names, $cur->parent->name);
        $cur = $cur->parent;
      }
      $data['fullname'] = implode(' > ', $names);
    } else {
      $data['fullname'] = $request->name;
    }
  }

  public function destroy(Category $category) {
    $category->delete();
    return ['status' => 'success'];
  }

}
