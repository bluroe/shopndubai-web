<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Customer;

class AddressController extends Controller
{

	protected $rules = [
		'firstname' => 'required',
		'lastname' => 'required',
		'address1' => 'required',
		// 'address2' => 'required',
		'email' => 'required',
		'phone' => 'required',
		// 'area' => 'required',
		// 'city' => 'required',
		'pin' => 'required',
	];
    
	public function index(Customer $customer, Request $request) {
		$addresses = $customer->addresses;
		return ['status' => 'success', 'addresses' => $addresses];
	}

	public function create(Request $request) {
		
	}

	public function store(Customer $customer, Request $request) {
		$this->validate($request, $this->rules);
		$data = $request->only('firstname', 'lastname', 'address1', 'address2', 'area', 'city', 'pin', 'email', 'phone');
		// return $data;
		$address = $customer->addresses()->create($data);
		// return redirect()->route('addresses.index')->withSuccess('Address created');
		return ['status' => 'success', 'id' => $address->id];
	}

	public function edit(Address $address) {
		
	}

	public function update(Request $request, Address $address) {
		$this->validate($request, $this->rules);
		
		// return redirect()->route('addresses.index')->withSuccess('Address updated');
		return ['status' => 'success'];
	}

	public function destroy(Customer $customer, Address $address) {
		$address->delete();
		return ['status' => 'success'];
	}

}
