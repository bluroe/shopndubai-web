<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductVariant;

class CartController extends Controller
{
    
	public function index() {
		return ['status' => 'success'];
	}

	public function add() {
		return ['status' => 'success'];
	}

	public function total(Request $request) {
		// $data = json_decode($request->instance()->getContent(), true);
		
	}

	public function deliveryCharge(Request $request) {
		$order = new \App\Services\OrderTask;
		return $order->calculateDeliveryChargeOfItems($request->items);
	}

	public function checkProductAvailability(Request $request) {
		$unavailable = [];
		$items = $request->input('items', []);
		if(count($items)) {
			$variants = ProductVariant::whereIn('id', $request->items)->with('product.category')->get();
			foreach($variants as $variant) {
				$product = $variant->product;
				if(!$product->active || !$product->category->isAvailable()) {
					$unavailable[] = $variant->id;
				}
			}
		}
		return ['status' => 'ok', 'unavailable' => $unavailable];
	}

}
