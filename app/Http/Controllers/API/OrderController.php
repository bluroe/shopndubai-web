<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StoreLocator;
use Geotools;
use App\Order;
use App\Customer;
use App\Address;
use App\StoreOrder;
use App\OrderItem;
use App\Product;
use App\Store;
use DB;
use Carbon\Carbon;

class OrderController extends Controller
{

	public function index(Request $request) {
		if($request->customer) {
			// return ['status' => 'ok'];
			$customer = Customer::whereKey($request->customer)->first();
			return $customer->orders()->orderBy('id', 'desc')->get();
		}
	}

	public function placeOrder(Request $request) {
		$data = json_decode($request->instance()->getContent(), true);
		// return $data;

		$customer = Customer::whereKey($data['key'])->first();
		if(!$customer) {
			return [
				'status' => 'customer_not_found',
				'title' => 'We\'re terribly sorry',
				'msg' => 'Looks like we forgot who you are. You cannot proceed unless you verify your number again. Sorry for the inconvenience.'
			];
		}

		$delivery = $this->checkDelivery();
		$force = isset($data['force']) && $data['force'] == 1;
		if($delivery['status'] != 'ok' && isset($data['force']) && !$force) {
			return $delivery;
		}

		if(!isset($data['address'])) {
			return [
				'status' => 'error',
				'msg' => 'no_address_provided'
			];
		}

		$address = Address::firstOrCreate(array_merge($data['address'], ['customer_id' => $customer->id]));

		if(!isset($data['items'])) {
			return [
				'status' => 'error',
				'msg' => 'no_items_provided'
			];
		}
		$order = new \App\Services\OrderTask;
		$order->processOrder($data['items'], $address, 0, $customer);

		dispatch(new \App\Jobs\NotifyUsers($address->area));
		// $this->callWebhooks($address);

		return response()->json([
			'status' => 'ok',
			'msg' => [
				'You may receive a confirmation call',
				'Your orders will be delivered within 2hrs'
			]
		]);
	}

	private function callWebhooks($address) {
        $webhooks = [
            [
                'url' => "https://hooks.slack.com/services/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",
                'data' => 'payload=' . json_encode([
                    'text' => 'New order from ' . $address->area,
                    'username' => 'Tely Bazaar',
                    'icon_url' => 'https://telybazaar.com/img/logo-flat_57.png'
                ])
            ]
        ];

        foreach($webhooks as $webhook) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $webhook['url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $webhook['data']);

            $result = curl_exec($ch);
            curl_close ($ch);
        }
	}

	public function checkDelivery() {
		$now = Carbon::now('Asia/Kolkata');
		if($now->hour <= 8 || $now->hour >= 19) {
			return [
				'status' => 'no_delivery',
				'msg' => 'Delivery time is between 9am to 7pm. If you proceed, your order will be delivered in the next working hours.',
				'title' => 'Delivery Unavailable'
			];
		}
		return ['status' => 'ok'];
	}

	public function test(Request $request) {
		// return response()->json(\App\Store::all(), 200, [], JSON_PRETTY_PRINT);
		return response()->json(StoreLocator::nearbyStores($request->pin), 200, [], JSON_PRETTY_PRINT);
		// return StoreLocator::nearbyStores($request->pin);
	}

}
