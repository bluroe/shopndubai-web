<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\Banner;

class AdvertisementController extends Controller
{
	public function randomAd(Request $request) {
		// $customer_key = $request->key
		return Advertisement::where('active', '=', 1)->inRandomOrder()->first();
	}

	public function banners() {
		return Banner::all()->pluck('image')->all();
		
	}
}
