<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    
	public function index() {
		$categories = Category::with('images', 'children')->whereDepth(0)->get();
		return response()->json(['status' => 'success', 'categories' => $categories], 200, [], JSON_PRETTY_PRINT);
	}

}
