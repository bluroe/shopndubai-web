<?php namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use StoreLocator;
use App\Services\Messenger;
use DB;
use Validator;
use DateTime;

class CustomerController extends Controller {
    
	public function register(Request $request) {
		$key = $this->generateKey();
		$data = ['key' => $key];
		$customer = Customer::create($data);
		return response()->json(['status' => 'ok', 'key' => $key]);
	}

	private function generateKey() {
		return md5(microtime() . rand());
	}

	public function verifyMobile(Request $request) {
		$validator = Validator::make($request->all(), [
			'mobile' => 'required|digits:10'
		]);
		if($validator->fails()) {
			return [
				'status' => 'error',
				'msg' => $validator->errors()
			];
		}
		$mobile = $request->mobile;
		// $exists = DB::table('customers')
		// 	->where('mobile', $mobile)
		// 	->exists();
		// if($exists) {
		// 	return [
		// 		'status' => 'error',
		// 		'msg' => 'This number is already registered with Tely Bazaar'
		// 	];
		// }
		$otp = $this->generateOtp();
		$data = compact('otp', 'mobile');
		$data['created_at'] = new DateTime;
		$data['updated_at'] = new DateTime;
		$msg = 'Your OTP for Tely Bazaar is ' . $otp;
		DB::table('otp')
			->where('mobile', $mobile)
			->update([
				'active' => 0,
				'updated_at' => new DateTime
			]);
		DB::table('otp')->insert($data);
		$messenger = new Messenger;
		$messenger->sendMessage($request->mobile, $msg);
		return ['status' => 'ok'];
	}

	public function verifyOtp(Request $request) {
		$validator = Validator::make($request->all(), [
			'mobile' => 'required|digits:10',
			'otp' => 'required|digits:4'
		]);
		if($validator->fails()) {
			return [
				'status' => 'error',
				'msg' => $validator->errors()
			];
		}
		$mobile = $request->mobile;
		$otp = $request->otp;
		$exists = DB::table('otp')
			->where('otp', $otp)
			->where('mobile', $mobile)
			->where('active', 1)
			->exists();
		if($exists) {
			$key = $this->generateKey();
			DB::table('otp')
				->where('mobile', $mobile)
				->update([
					'active' => 0,
					'updated_at' => new DateTime
				]);
			Customer::create(compact('key', 'mobile'));
			return ['status' => 'ok', 'key' => $key];
		}
		return ['status' => 'error', 'msg' => 'Invalid OTP'];
	}

	private function generateOtp() {
		return rand(1000, 9999);
	}

	// this is probably the first call during startup after registration
	public function updatePin(Request $request) {
		// $customer = Customer::whereKey($request->key)->first();
		$customer = Customer::firstOrCreate(['key' => $request->key]); // enable zombies
		$customer->update(['pin' => $request->pin]);
		$stores = StoreLocator::nearbyStores($request->pin)->count();
		return response()->json(['status' => 'ok', 'stores' => $stores]);
	}

}
