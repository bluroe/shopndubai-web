<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Store;
use App\Product;
use Illuminate\Database\Eloquent\Collection;
use Geotools;
use App\Transformers\ProductTransformer;
use StoreLocator;
use App\Customer;

class ProductController extends Controller
{

    // fetch from single nearby store
	public function index(Category $category, Request $request) {
		$query = $category->products()->with('images', 'stores');
		if($request->offset) $query->skip($request->offset);
		$query->take(10);
		$products = fractal($query->get(), new ProductTransformer)->toArray();
		return response()->json($products, 200, [], JSON_PRETTY_PRINT);
	}
    
	public function search(Request $request) {
		$products = Product::with('images', 'stores');
		if($request->search) {
			$products->where('name', 'like', '%' . $request->search . '%');
		}
		$products = $products->get();
		$products = fractal($products, new ProductTransformer)->toArray();
		return response()->json($products, 200, [], JSON_PRETTY_PRINT);
	}

	private function findProducts($pin, $category = null) {
		if(is_null($category)) {
			$product_builder = Product::query();
		} else {
			$product_builder = $category->products();
		}
		$product_builder->with('stores', 'images');
		// $product_builder->has('stores'); // ignore location
		$store = StoreLocator::nearbyStore($pin);
		if($store) {
			$product_builder->whereHas('stores', function($query) use ($store) {
				$query->whereId($store->id);
			});	
			return $product_builder;
		}
		// return $product_builder; // ignore location
		return new Collection;
	} 

	public function test() {
		$coords3 = ['lat' => '11.694518', 'lng' => '75.537801']; // mahe
		$coords2 = ['lat' => '11.749463', 'lng' => '75.492148']; // thalassery
		$coords1 = ['lat' => '11.763996', 'lng' => '75.480596']; // my location
		// echo $this->getDistance($coords1, $coords3);
		// return ['asd', 'sdf' => 'asd'];
		$product = Product::with('stores')->whereIn('id', [1,2,3])->whereHas('stores', function($query) {
			$query->whereId(1);
		})->get();
		$product = Product::with(['stores' => function($query) {
			$query->whereId(1);
		}])->whereIn('id', [1,2,3])->get();
		return response()->json($product, 200, [], JSON_PRETTY_PRINT);
	}

	/* TEST, SAMPLE, EXAMPLE CODE
	 * GET PRODUCTS FROM STORES
	 */

	public function storeBased() {
		$stores = Store::whereIn('id', [1, 2])->with('products')->get();
		$filtered = $stores->filter(function($store, $key) {
			return $store->products->count();
		})->values();
		return response()->json($filtered, 200, [], JSON_PRETTY_PRINT);
	}

	// multiple stores, large overhead
	public function productBased() {
		$stores = [1, 2]; // stores(id) nearby
		$products = Product::with('stores')->get(); // get all products, eager load stores
		$filtered = $products->filter(function($product, $key) use ($stores) {
			$product->stores = $product->stores->filter(function($store, $key) use ($stores) {
				return in_array($store->id, $stores); // filter store
			});
			return $product->stores->count(); // check availability in store
		});
		return response()->json($filtered, 200, [], JSON_PRETTY_PRINT);
	}

	public function productBasedSingleStore() {
		$store_id = 2; // stores(id) nearby
		$products = Product::with('stores')->whereHas('stores', function($query) use ($store_id) {
			$query->whereId($store_id)->where('qty', '>', 0); // store and qty filter
		})->get();
		$resource = fractal($products, new ProductTransformer)->toArray();
		return response()->json($resource, 200, [], JSON_PRETTY_PRINT);
	}

}
