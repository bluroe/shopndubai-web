<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StoreLocator;

class StoreController extends Controller
{
    
	public function index(Request $request) {
		$customer = Customer::whereKey($request->key)->first();
		$store = StoreLocator::nearbyStore($customer->pin);
		return compact('store');
	}

}
