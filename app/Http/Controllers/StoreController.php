<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Product;

class StoreController extends Controller
{

	protected $rules = [
		'name' => 'required',
		'pin' => 'required',
		'address' => 'required',
		'active' => 'required'
	];

	public function index(Request $request) {
		if($request->ajax()) {
			$query = Store::query();
			if($request->has('active')) {
				$query->where('active', 1);
			}
			if($request->q) {
				$query->where('name', 'like', '%' . $request->q . '%');
			}
			$stores = $query->get();
			return ['status' => 'success', 'stores' => $stores];
			return response()->json(['status' => 'success', 'stores' => $stores], 200, [], JSON_PRETTY_PRINT);
		}
		return view('admin.stores.index');
	}

	public function show(Store $store) {
		// return $store->products;
		// $store->load('products');
		return response()->json($store, 200, [], JSON_PRETTY_PRINT);
		// return $store;
	}

	public function create(Request $request) {
		return view('admin.stores.form');
	}

	public function store(Request $request) {
		// return response()->json($request->all(), 200, [], JSON_PRETTY_PRINT);
		$this->validate($request, $this->rules);
		$data = $request->only('name', 'pin', 'address', 'area', 'active');
		Store::create($data);
		return redirect()->route('stores.index')->withSuccess('Store created');
	}

	public function edit(Store $store) {
		$products = Product::all();
		return view('admin.stores.form', compact('store', 'products'));
	}

	public function update(Request $request, Store $store) {
		// return $request->all();
		$this->validate($request, $this->rules);
		$data = $request->only('name', 'pin', 'address', 'area', 'active');
		$store->update($data);
		return redirect()->route('stores.index')->withSuccess('Store updated');
	}

	public function destroy(Store $store) {
		$store->delete();
		return ['status' => 'success'];
	}

	/*
	 *	change product constraints for a store
	 */
	public function changeProduct(Store $store, Product $product, Request $request) {
		$data = $request->only('price', 'qty', 'reduce_qty', 'active');
		// return ['type' => gettype($data)];
		// return $data;
		$store->products()->syncWithoutDetaching([$product->id => $data]);
		// $store->save();
		return $store->products;
		return ['status' => 'success'];
	}

}
