<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    
	public function index(Request $request) {
		if($request->ajax()) {
			$services = Service::all();
			return ['services' => $services];
		} else {
			return view('admin.services.index');
		}
	}

	public function create() {
		return view('admin.services.form');
	}

	public function store(Request $request) {
		$data = $request->except('_token');
		$data['available'] = $data['total'];
		Service::create($data);
		return redirect()->route('services.index')->with('message', 'Service created');
	}

	public function edit(Service $service) {
		return view('admin.services.form', compact('service'));
	}

	public function update(Request $request, Service $service) {
		$service->update($request->except('_token', '_method'));
		return redirect()->route('services.index')->with('message', 'Service updated');
	}

	public function delete(Service $service) {
		$service->delete();
		return ['status' => 'ok'];
	}

	public function updateStatus(Request $request, Service $service) {
		$service->update(['active' => $request->active]);
	}

	public function updateAvailability(Request $request, Service $service) {
		if($request->has('available')) {
			$service->update(['available' => $request->available]);
		} else {
			if($request->increase) {
				$service->increment('available');
			} else {
				$service->decrement('available');
			}
		}
	}

}
