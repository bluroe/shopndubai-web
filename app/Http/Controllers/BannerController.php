<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function index(Request $request) {
    	if($request->ajax()) {
			return Banner::all();
		}
	    return view('admin.banner');
	}

	public function store(Request $request) {
		$file = $request->file('image');
		$new_filename = uniqid() . '.' . $file->getClientOriginalExtension();
		Banner::create([
			'name' => $request->name,
			'image' => '/img/uploads/' . $new_filename
		]);
		$file->move(public_path() . '/img/uploads/', $new_filename);
		return redirect()->route('banners.index');
	}

	public function destroy(Banner $banner) {
		$banner->delete();
		return ['status' => 'ok'];
	}
}
