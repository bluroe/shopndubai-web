<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\Store;
use App\ProductVariant;
use App\Product;

class OfferController extends Controller
{
    public function index(Request $request) {
    	if($request->ajax()) {
            $products = Product::where('active', true);
    		$offers = Offer::with('variant.product')->with('store');
            if($request->product) {
                $p = $request->product;
                $offers->whereHas('variant', function($query) use($p){
                    $query->whereIn('product_id', $p);
                });
            }
            if($request->startDate) {
                $s = $request->startDate;
                $e = $request->endDate;
                $offers->whereDate('from_date', '>=', $s)->whereDate('to_date', '<=', $e);
            }
    		return ['offers' => $offers->get(), 'length' => Offer::count(), 'products' => $products->get()];
    	}
    	return view('admin.offers.index');
    }

    public function create() {
    	$stores = Store::all();
    	return view('admin.offers.form', compact('stores'));
    }

    public function store(Request $request) {
    	$data = $request->only('variant_id', 'text', 'qty', 'store_id', 'from_date', 'to_date', 'offer_by');
    	$data['price'] = $request->price ? $request->price : null;
        $data['percent'] = $request->percent ? $request->percent : null;
    	$data['add_qty'] = $request->add_qty ? $request->add_qty : null;
    	$offer = Offer::firstOrCreate($data);

    	return ['status' => 'ok'];
		return redirect()->route('offers.index')->withSuccess('Offer created');
    }

    public function edit($id, Request $request) {
        $offer = Offer::with('variant.product')->with('store')->where('id', $id)->first();
        $prodId = $offer->variant->product_id;
        $variant = ProductVariant::where('product_id', $prodId)->get();
        $stores = Store::all();
        return view('admin.offers.form', compact('offer', 'stores', 'variant'));
    }

    public function update($id, Request $request) {
        $offers = Offer::find($id);
        $data = $request->only('variant_id', 'text', 'qty', 'store_id', 'from_date', 'to_date', 'offer_by');
        $data['price'] = $request->price ? $request->price : null;
        $data['percent'] = $request->percent ? $request->percent : null;
        $data['add_qty'] = $request->add_qty ? $request->add_qty : null;
        $offers->update($data);
        return "success";
    }

    public function destroy(Offer $offer) {
        $offer->delete();
        return redirect()->route('offers.index')->withSuccess('Offer deleted');
    }
}
