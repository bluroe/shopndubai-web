<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class AdminController extends Controller {

  protected $user_rules = [
    'name' => 'required',
    'email' => 'required|email',
    'password' => 'required|min:6|confirmed'
  ];

  public function __construct() {
    $this->middleware('admin.privilege', ['only' => ['storeUser', 'createUser', 'storeUser', 'deleteUser']]);
    $this->middleware('admin.or.self', ['only' => ['changePassword']]);
  }

  public function dashboard() {
    return view('admin.dashboard');
  }

  public function notifications() {
    $pending_orders = \App\Order::where('status', 4)->count();
    return compact('pending_orders');
  }

  public function settings() {
    $settings = DB::table('settings')->pluck('value', 'key')->all();
    return view('admin.settings', compact('settings'));
  }

  public function profile() {
    return view('admin.profile');
  }

  public function users() {
    $users = User::all();
    return view('admin.users.index', compact('users'));
  }

  public function createUser() {
    return view('admin.users.form');
  }

  public function storeUser(Request $request) {
    $this->validate($request, $this->user_rules);
    User::create([
      'name' => $request->name,
      'email' => $request->email,
      'password' => bcrypt($request->password)
    ]);
    return redirect()->route('users.index')->with('success', 'User created');
  }

  public function changePassword(User $user, Request $request) {
    // return $request->all();
    $this->validate($request, array_except($this->user_rules, ['name', 'email']));
    $user->update(['password' => bcrypt($request->password)]);
    return redirect()->back()->with('success', 'Password updated');
  }

  public function deleteUser(User $user) {
    $user->delete();
    return redirect()->back()->with('success', 'User deleted');
  }

  /*
   * update firebase token
   */
  public function updateToken(Request $request) {
  	auth()->user()->update(['fcm_token' => $request->token]);
    return ['status' => 'ok', 'message' => 'token updated'];
  }

  public function pincodes() {
    return array_unique(DB::table('stores')->pluck('pin')->all());
  }

  public function pincodeValidate(Request $request) {
    $exists = DB::table('pincodes')->where('pin', $request->pin)->exists();
    if(!$exists) {
      abort(404);
    }
  }

  public function checkPincode(Request $request) {
    if(!$request->pin) return ['status' => 'error'];
    $exists = DB::table('pincodes')->where('pin', $request->pin)->exists();
    return ['status' => $exists ? 'ok' : 'not_found'];
  }
  
  public function updateDeliverySettings(Request $request) {
    $data = $request->only('delivery_charge', 'delivery_free_above');
    foreach($data as $key => $value) {
      DB::table('settings')->where('key', $key)->update(['value' => $value]);
    }
    return redirect('/admin/settings');
  }

  public function updateSettings(Request $request) {
    $this->_updateSettings($request);
    return redirect('/admin/settings')->with('service_success', 'Updated');
  }

  private function _updateSettings($request) {
    foreach($request->except('_token', '_method') as $key => $val) {
      $exists = DB::table('settings')->where('key', $key)->exists();
      if($exists) {
        DB::table('settings')->where('key', $key)->update(['value' => $val]);
      } else {
        DB::table('settings')->insert([
          'key' => $key,
          'value' => $val
        ]);
      }
    }
  }

}
