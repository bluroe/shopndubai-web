<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;

class FilterController extends Controller
{

	protected $rules = [
		'name' => 'required'
	];

	public function index(Request $request) {
		$filters = Filter::all();
		if($request->wantsJson()) {
			return ['status' => 'success', 'filters' => $filters];
		} else {
			return view('admin.filters.index', compact('filters'));
		}
	}

	public function create() {
		return view('admin.filters.create');
	}

	public function store(Request $request) {
		$this->validate($request, $this->rules);
		// return $request->all();
		$data = $request->only('name', 'type');
		$filter = Filter::create($data);
		return redirect()->route('filters.index')->withSuccess('Filter created');
	}

	public function edit(Filter $filter) {
		// return json_decode($filter->options);
		return view('admin.filters.edit', compact('filter'));
	}

	public function update(Filter $filter, Request $request) {

		$this->validate($request, $this->rules);
		// return $request->all();
		$data = $request->only('name', 'type');
		$filter->update($data);
		return redirect()->route('filters.index')->withSuccess('Filter updated');

	}

	public function destroy(Filter $filter) {
		$filter->delete();
		return ['status' => 'success'];
	}

}
