<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use Carbon\Carbon;

class AdvertisementController extends Controller
{
    
	public function index(Request $request) {
		if($request->ajax()) {
			return Advertisement::all();
		}
		return view('admin.advertisements.index');
	}

	public function store(Request $request) {
		// return $request->all();
		$data = $request->only('name', 'from_date', 'to_date');
		$data['from_date'] = Carbon::createFromFormat('d/m/Y', $data['from_date'])->toDateTimeString();
		$data['to_date'] = Carbon::createFromFormat('d/m/Y', $data['to_date'])->toDateTimeString();
		$data['image'] = $this->uploadImage($request->file('image'));
		$data['active'] = 1;
		$ad = Advertisement::create($data);
		// return response()->json($ad);
		return redirect()->route('advertisements.index');
	}

	private function uploadImage($file) {
		$filename = uniqid() . '.' . $file->getClientOriginalExtension();
		$file->move(public_path() . '/uploads/', $filename);
		return '/uploads/' . $filename;
	}

	public function destroy(Advertisement $advertisement) {
		$advertisement->delete();
		return ['status' => 'ok'];
	}

	public function deactivate(Advertisement $advertisement) {
		$advertisement->update(['active' => 0]);
		return ['status' => 'ok'];
	}

	public function reactivate(Advertisement $advertisement) {
		$advertisement->update(['active' => 1]);
		return ['status' => 'ok'];
	}
}
