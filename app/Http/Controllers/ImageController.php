<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{

  protected $image_sizes = [
    'product' => array('width' => 600, 'height' => 300),
    'category' => array('width' => 500, 'height' => 250)
  ];

  protected $tiny_png = true;

  public function index(Request $request) {
    $query = Image::query();
    if($request->q) { // filter names
      $query->where('name', 'like', "%{$request['q']}%");
    }
    $count = $query->count();
    $query->orderBy('id', 'desc');
    $query->take($request->get('limit', 10));
    if($request->offset) { // offset
      $query->skip($request->offset);
    }
    return ['images' => $query->get(), 'count' => $count];
  }

  public function store(Request $request) {
    if($request->hasFile('image')) {
      $file = $request->file('image');
      $filename = uniqid() . '.' . $file->getClientOriginalExtension();
      if($this->tiny_png) {
        try {
          $image_size = $this->image_sizes[$request->type ?: 'product'];
          \Tinify\setKey("4Bb3T_T25r7OSTQzK4t9Ytj4TMOKmn3T");
          $source = \Tinify\fromFile($file);
          $resized = $source->resize(array(
              "method" => "fit",
              "width" => $image_size['width'],
              "height" => $image_size['height']
          ));
          $resized->toFile(public_path() . '/uploads/' . $filename);
        } catch(Exception $e) {
          return ['status' => 'error', 'message' => 'tinypng error'];
        }
      } else {
        $file->move(public_path() . '/uploads/', $filename);
      }
      $image = Image::create([
        'name' => $file->getClientOriginalName(),
        'filename' => '/uploads/' . $filename,
      ]);
      return ['status' => 'ok', 'image' => $image];
    } else {
      return 'no image specified';
    }
  }

  public function update(Image $image, Request $request) {
    $image->update($request->only('name'));
    return ['status' => 'ok'];
  }

  public function destroy(Image $image) {
    $image->delete();
    return ['status' => 'ok'];
  }

}
