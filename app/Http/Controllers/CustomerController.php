<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Customer;
use App\User;
use App\Transformers\CustomerTransformer;

class CustomerController extends Controller {

	public function index(Request $request) {
		if($request->ajax()) {
			$query = Customer::query();
			$query->with('orders');
			if($request->has('q')) {
				$q = $request->q;
				if(strpos($q, '#') !== false) {
					$query->where('id', str_replace('#', '', $q));
				} else {
					$query->where('mobile', 'like', '%' . $q . '%');
					$query->orWhere('key', 'like', '%' . $q . '%');
				}
			}
			$count = $query->count();
			if($request->has('offset')) {
				$query->skip($request->offset);
			}
			$query->take($request->input('limit', 10));
			$customers = $query->orderBy('id', 'desc')->get();
			$customers = fractal($customers, new CustomerTransformer)->toArray();
			return ['total' => $count, 'customers' => $customers['data']];
		}
		return view('admin.customers.index');
	}

	public function update(Customer $customer, Request $request) {

	}

	public function destroy(Customer $customer) {
		$customer->delete();
		return ['status' => 'ok'];
	}

}
