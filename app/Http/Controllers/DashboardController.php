<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Order;
use Carbon\Carbon;

class DashboardController extends Controller
{
    
    public function installsData(Request $request) {
        $days = $request->input('days', 30);
    	$labels = [];
    	$data = [];
    	$time = Carbon::now();
    	$timeline = array_values(array_fill(0, $days, 0));
    	$customers = Customer::whereDate('created_at', '>=', Carbon::now()->subDays($days))->get();
    	foreach($customers as $customer) {
    		$day = $customer->created_at->diffInDays();
    		$timeline[$day]++;
    	}
    	$instance = Customer::count();
    	for($i = 0; $i < $days; $i++) {
    		array_unshift($data, $instance);
    		$instance -= $timeline[$i];
    		array_unshift($labels, $time->format('d M Y'));
            $time->subDays(1);
    	}
    	$ret = compact('data', 'labels');
    	// return $ret;
    	return response()->json($ret, 200, [], JSON_PRETTY_PRINT);
    }

    public function salesData(Request $request) {
    	$days = $request->input('days', 30);
    	$labels = [];
    	$data = array_values(array_fill(0, $days, 0));
        $now = Carbon::now()->setTimezone('Asia/Kolkata');
        $now->hour = 0;
    	$time = $now;
    	$start = $now->copy()->subDays($days);
    	$sales = Order::whereDate('created_at', '>=', $start)
    		->where('status', 2)
    		->get();
    	foreach($sales as $sale) {
            $t = $sale->created_at->setTimezone('Asia/Kolkata');
            $t->hour = 0;
    		$day = $t->diffInDays($start);
    		$data[$day]++;
    	}
    	for($i = 0; $i < $days; $i++) {
    		array_unshift($labels, $time->format('d M Y'));
            $time->subDays(1);
    	}
    	$ret = compact('data', 'labels');
    	// return $ret;
    	return response()->json($ret, 200, [], JSON_PRETTY_PRINT);
    }

}
