<?php

namespace App\Http\Middleware;

use Closure;

class AdminOrSelf
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->user;
        if(auth()->user()->level == 0 || auth()->user()->id == $id) {
            return $next($request);
        }
        return response()->json(['status' => 'error', 'message' => 'You do not have enough privilieges to do this action'], 400, [], JSON_PRETTY_PRINT);
    }
}
