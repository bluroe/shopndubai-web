<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    
	protected $guarded = ['id'];
	protected $casts = [
		'options' => 'array',
	];

	public function products() {
		return $this->belongsToMany('App\Product')->withPivot('options');
	}

}
