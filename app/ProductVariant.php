<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $guarded = ['id'];
    protected $table = 'product_variants';

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function offer() {
    	return $this->hasOne('App\Offer', 'variant_id');
    }

}
