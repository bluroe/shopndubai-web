<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Offer;
use Carbon\Carbon;

class ClearExpiredOffers extends Command {


	protected $name = "clear:offer";

	protected $description = "clear expired offers";

	public function handle() {
		Offer::where('to_date', '=', Carbon::now()->toDateString())->delete();
		$this->info('expired offer deleted');
	}

}