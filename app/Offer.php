<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    
	protected $guarded = ['id'];

	public function variant() {
        return $this->belongsTo('App\ProductVariant');
    }

    public function store() {
        return $this->belongsTo('App\Store');
    }

}