<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* An order may be routed to many stores depending on the availability of the products.
 * So a store may receive a sub order. This class represents the sub order.
 */ 
class StoreOrder extends Model
{
    
	protected $guarded = ['id'];

	protected $appends = ['total'];

	public function order() {
		return $this->belongsTo('App\Order');
	}

	public function store() {
		return $this->belongsTo('App\Store');
	}

	public function orderItems() {
		return $this->hasMany('App\OrderItem');
	}

	public function getTotalAttribute() {
		return $this->orderItems->reduce(function($carry, $orderItem) {
			return $carry + ($orderItem->qty * $orderItem->price);
		}, 0);
	}

}
