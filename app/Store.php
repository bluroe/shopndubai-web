<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    
	protected $guarded = ['id'];

	public function storeOrder() {
		return $this->hasMany('App\StoreOrder');
	}

	public function products() {
		return $this->belongsToMany('App\Product')->withPivot('qty', 'price', 'active', 'reduce_qty');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function orders() {
		return $this->hasMany('App\Order');
	}

	public function offer() {
    	return $this->hasOne('App\Offer');
    }

}
