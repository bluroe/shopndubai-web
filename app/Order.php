<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
	protected $guarded = ['id'];

	protected $appends = ['total'];

	protected $casts = [
		'status' => 'int',
		'delivery_charge' => 'double'
	];

	public function customer() {
		return $this->belongsTo('App\Customer');
	}

	public function address() {
		return $this->belongsTo('App\Address');
	}

	public function orderItems() {
		return $this->hasMany('App\OrderItem');
	}

	public function getOrderTotal() {
		return $this->orderItems->reduce(function($carry, $orderItem) {
			return $carry + ($orderItem->qty * $orderItem->price);
		}, 0);
	}

	public function getTotalAttribute() {
		$total = $this->getOrderTotal();
		$total += $this->delivery_charge;
		return $total;
	}

	public function statusMessage() {
		switch($this->status) {
			case 0:
				return "Ordered";
			case 1:
				return "In transit";
			case 2:
				return "Delivered";
			case 3:
				return "Cancelled";
			case 4:
				return "pending";
		}
	}

	public function statusClass() {
		switch($this->status) {
			case 0:
				return "text-warning";
			case 1:
				return "text-primary";
			case 2:
				return "text-success";
			case 3:
				return "text-default";
			case 4:
				return "text-danger";
		}
	}

	public function store() {
		return $this->belongsTo('App\Store');
	}

}
