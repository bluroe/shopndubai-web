<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    
	protected $guarded = ['id'];

	public function customer() {
		return $this->belongsTo('App\Customer');
	}

	public function orders() {
		return $this->hasMany('App\Order');
	}

	public function format() {
		$result = "";
		$result .= $this->address1 . '<br>';
		if($this->address2) {
			$result .= $this->address2 . '<br>';
		}
		$result .= $this->area . ', ' . $this->city . '<br>';
		$result .= $this->pin . '<br>';
		return $result;
	}

}
