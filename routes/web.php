<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['domain' => 'spa.telybazaar.app'], function() {
  Route::get('/', function() {
    return view('admin.spa');
  });
});

Route::get('/', function () {
  // return view('welcome');
  return view('coming-soon');
});

// Route::get('/test', 'API\ProductController@test');
Route::get('test', function() {

    return \App\Category::find(1)->descendantsAndSelf()->pluck('id')->all();
  // return array_unique(\App\Store::all()->pluck('pin')->all());
  // return \App\OrderItem::first()->product;
});
Route::get('/store-based', 'API\ProductController@storeBased');
Route::get('/product-based', 'API\ProductController@productBased');
Route::get('/product-based-single-store', 'API\ProductController@productBasedSingleStore');
Route::get('/calculate-distance', 'API\OrderController@calculateDistance');
Route::get('/reindex-category-names', function() {
  $categories = App\Category::all();
  function findFullName($category) {
    static $names = [];
    array_unshift($names, $category->name);
    if($category->parent_id) {
      return findFullName($category->parent);
    } else {
      $data = implode(' > ', $names);
      $names = [];
      return $data;
    }
  }
  foreach($categories as $category) {
    $category->fullname = findFullName($category);
    echo $category->fullname . '<br>';
    $category->save();
  }
});

Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/customers', function() {
  return response()->json(\App\Customer::all(), 200, [], JSON_PRETTY_PRINT);
});

Route::get('/address', function() {
  return response()->json(\App\Address::all(), 200, [], JSON_PRETTY_PRINT);
});

Route::get('/products', function() {
  return response()->json(\App\Product::with('stores')->get(), 200, [], JSON_PRETTY_PRINT);
});

Route::get('/orders', function() {
  return response()->json(\App\Order::with('storeOrders.orderItems')->get(), 200, [], JSON_PRETTY_PRINT);
});

Route::get('/stores', function() {
  return response()->json(\App\Store::all(), 200, [], JSON_PRETTY_PRINT);
});

Route::get('/sample-product-seed', function() {
  foreach(range(0, Request::get('count', 10) - 1) as $i) {
    DB::table('products')->insert([
      'name' => 'foozie',
      'qty' => 20,
      'price' => 20,
      'purchase_price' => 20,
      'category_id' => 1
    ]);
  }
  echo 'seeded ' . (Request::get('count', 10)) . ' items';
});

Route::get('delete-all-products', function() {
  DB::table('products')->delete();
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
  Route::get('/dashboard', ['uses' => 'AdminController@dashboard', 'as' => 'admin.dashboard']);
  Route::get('/dashboard/installs-data', 'DashboardController@installsData');
  Route::get('/dashboard/sales-data', 'DashboardController@salesData');
  Route::get('/notifications', 'AdminController@notifications');

  Route::get('category/tree', 'CategoryController@tree');
  Route::resource('categories', 'CategoryController');

  Route::get('product/{product}/stores', 'ProductController@stores');
  Route::get('product/{product}/images', 'ProductController@images');
  Route::resource('products', 'ProductController');

  Route::resource('images', 'ImageController');
  
  Route::resource('filters', 'FilterController');

  Route::resource('customers', 'CustomerController', ['only' => ['index', 'update', 'destroy']]);

  Route::get('/orders/{order}/products', 'OrderController@products');
  Route::post('/orders/{order}/products/{products}', 'OrderController@changeProduct');
  Route::delete('/orders/{order}/products/{product}', 'OrderController@removeProducts');
  Route::post('/orders/savestore', 'OrderController@saveStore');
  Route::get('/orders/{order}/store-orders', 'OrderController@storeOrders');
  Route::get('/orders/{order}/order-items', 'OrderController@orderItems');
  Route::patch('/order/{order}', 'OrderController@updateOrderStatus');
  Route::get('/orders/{order}/resend-message', 'OrderController@resendMessage');
  Route::patch('/orders/{order}/cancel-order', 'OrderController@cancelOrder');
  Route::get('/orders/sale', 'OrderController@storeSale');
  Route::post('/orders/status', 'OrderController@updateStatus');
  Route::resource('orders', 'OrderController');

  Route::post('/stores/{store}/products/{product}', 'StoreController@changeProduct');

  Route::resource('stores', 'StoreController');

  Route::resource('customers.addresses', 'AddressController');

  Route::post('/advertisements/{advertisement}/reactivate', 'AdvertisementController@reactivate');
  Route::post('/advertisements/{advertisement}/deactivate', 'AdvertisementController@deactivate');
  Route::resource('advertisements', 'AdvertisementController');

  Route::get('/settings', ['uses' => 'AdminController@settings', 'as' => 'settings.index']);
  Route::get('/profile', 'AdminController@profile');
  Route::get('/users', ['uses' => 'AdminController@users', 'as' => 'users.index']);
  Route::get('/users/create', ['uses' => 'AdminController@createUser', 'as' => 'users.create']);
  Route::patch('/users/{user}/change-password', ['uses' => 'AdminController@changePassword', 'as' => 'users.change-password']);
  Route::post('/users/', ['uses' => 'AdminController@storeUser', 'as' => 'users.store']);
  Route::delete('/users/{user}', ['uses' => 'AdminController@deleteUser', 'as' => 'users.destroy']);
  Route::get('/users/{user}', 'AdminController@deleteUser');
  Route::resource('/banners', 'BannerController');
  Route::resource('/offers', 'OfferController');

  Route::post('/update-token', 'AdminController@updateToken'); // fcm token update

  Route::post('/settings/delivery', 'AdminController@updateDeliverySettings');
  Route::patch('/settings/service', 'AdminController@updateSettings');
  Route::get('/pincodes', 'AdminController@pincodes');
  Route::get('/pincode-validate', 'AdminController@pincodeValidate');

  Route::post('/services/{service}/update-status', 'ServiceController@updateStatus');
  Route::post('/services/{service}/update-availability', 'ServiceController@updateAvailability');
  Route::resource('services', 'ServiceController');
});

Route::get('offerdelete', function() {
  Artisan::call('clear:offer');
  echo '<br>offer deleted';
});
