<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', function() {
  return abort(404);
});

Route::get('/authenticate', function() {
  $user = App\User::first();
  $token = $user->createToken('Token Name')->accessToken;
  return compact('token');
});

Route::get('/register', 'API\CustomerController@register');
Route::post('/request-otp', 'API\CustomerController@verifyMobile');
Route::post('/send-otp', 'API\CustomerController@verifyOtp');

Route::get('/check-products', 'API\CartController@checkProductAvailability');
Route::get('/check-pin', 'AdminController@checkPincode');
Route::get('/check-delivery', 'API\OrderController@checkDelivery');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/categories', 'API\CategoryController@index');
Route::get('/categories/{category}/products', 'API\ProductController@index');
Route::post('/cart/add/{product}', 'API\CartController@add');
// Route::get('/cart', 'API\CartController@index');
// Route::get('/cart/total', 'API\CartController@total');
Route::post('/delivery-charge', 'API\CartController@deliveryCharge');
Route::get('/products', 'API\ProductController@search');
Route::post('/order', 'API\OrderController@placeOrder');
Route::get('/orders', 'API\OrderController@index');
Route::get('/stores', 'API\StoreController@index');
Route::get('/update-pin', 'API\CustomerController@updatePin');
Route::get('/advertisement', 'API\AdvertisementController@randomAd');
Route::get('/banners', 'API\AdvertisementController@banners');

Route::get('/calculate-distance', 'API\OrderController@calculateDistance');
Route::get('/geocode', 'API\OrderController@test');
Route::get('/products-test', 'API\ProductController@test');
// Route::post('')