$(document).ready(function() {
  $.ajaxSetup({
    statusCode: {
      401: function() {
        window.location.href = '/login?next=' + window.location.pathname;
      }
    }
  })

  var URL = window.location.href.split('?')[0];

  $('[data-toggle=tooltip]').tooltip();
  
  $('#logo').click(function() {
      $('#wrapper').toggleClass('toggled');
  });

  $('.sidebar a').filter(function() {
    return URL.startsWith(this.href);
  }).each(function() {
    var self = $(this);
    self.parent('li').addClass('active');
    if(self.closest('.child_menu').length) {
      self.closest('.primary-menu > li').addClass('open')
      .find('.icon .fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }
  });

  $('.primary-menu > li > a').click(function(e) {
    var parent = $(this).parent();
    if(parent.find('.child_menu').length) {
      e.preventDefault();
      // console.log(parent)
      if(parent.hasClass('open')) {
        parent.find('.icon .fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        parent.removeClass('open');
      } else {
        parent.find('.icon .fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        parent.addClass('open');
      }
    } else {
      $('.primary-menu > li').removeClass('open');
    }
  });

  var url = document.location.toString();
  $('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
  });
  if(url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
  }

}); // end

(function() {
  var delta = 500;
  var tPress = 0;
  document.addEventListener('keyup', function(e) {
    var now = new Date();
    if(e.keyCode == 84) { // T
      tPress = new Date();
    } else if(now - tPress <= delta && document.activeElement == document.body) {
      if(e.keyCode == 80) { // P
        window.location.href = '/admin/products';
      } else if(e.keyCode == 83) { // S
        window.location.href = '/admin/stores';
      } else if(e.keyCode == 79) { // O
        window.location.href = '/admin/orders';
      }
    }
  })
})();

document.addEventListener('keyup', function(e) {
  if(e.keyCode == 191) { // slash key(/) listener
    var search_input = document.getElementById('search');
    if(document.activeElement != search_input) {
      search_input.focus();
    }
  }
});

function getParams() {
  var search = location.search.substring(1);
  if(!search) return {};
  return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
}