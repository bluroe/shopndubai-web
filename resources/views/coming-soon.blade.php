<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tely Bazaar</title>
        <meta name="description" content="TelyBazaar is an online bazaar in Thalassery. Get your groceries, households, Thalassery special foods and more delivered right at your doorsteps. Orders will be delivered within 2 hours. Available on Play Store and App Store." />

        <!-- CSS -->
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:400,700'>
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">


        <meta property="og:image" content="{{ asset('grocery-store_edited.jpg') }}" />
        <meta name="google-play-app" content="app-id=com.bluroe.telybazaar">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/img/apple-touch-icon-144-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/img/apple-touch-icon-114-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/img/apple-touch-icon-72-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('/img/apple-touch-icon-57-precomposed.png') }}">

    </head>

    <body>

        <!-- Header -->
        <div class="container">
            <div class="row header">
                <div class="col-sm-4">
		    <img src="{{ asset('/img/tely_strip.png') }}" style="margin-top:13px;" />
                </div>
                <div class="col-sm-8 call-us">
                    <p style="margin-top:12px;margin-bottom:12px;">Email: <span>info@telybazaar.com</span></p>
                </div>
            </div>
        </div>

        <!-- Coming Soon -->
        <div class="coming-soon">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2>We're here</h2>
                            <p style="margin-bottom:0px;">TelyBazaar will bring your grocery, households and tely-special foods at your doorstep. </p>
                            <p style="margin-top:0px;">Download Android or IOS App for our valuable service.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="container">
            <div class="row">
                <div class="apps">
                    <div class="col-sm-6">
                        <div class="apple">
                            <a href="https://itunes.apple.com/in/app/tely-bazaar/id1236035735?mt=8" style="display:inline-block;overflow:hidden;background:url(//linkmaker.itunes.apple.com/assets/shared/badges/en-us/appstore-lrg.svg) no-repeat;width:243px;height:72px;background-size:contain;"></a>
                        </div>
                    </div>
                    <div class="col-sm-6 android">
                        <a href='https://play.google.com/store/apps/details?id=com.bluroe.telybazaar&utm_source=website&utm_campaign=tlybzr&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
                    </div>
                </div>
                <div class="col-sm-12 subscribe">
                    <h3>Subscribe to our newsletter</h3>
                    <p>Sign up now to our newsletter and you'll be one of the first to know when the site is ready:</p>                    
                    <form class="form-inline" role="form" action="https://bluroe.us13.list-manage.com/subscribe/post?u=0c1405f963c1dbbc4e017943b&amp;id=952640066b" method="post">
                    	<div class="form-group">
                    		<label class="sr-only" for="subscribe-email">Email address</label>
                        	<input type="text" name="EMAIL" placeholder="Enter your email..." class="subscribe-email form-control" id="subscribe-email">
                        </div>
                        <button type="submit" class="btn">Subscribe</button>
                    </form>
                    <div class="success-message"></div>
                    <div class="error-message"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 social">
                    <a href="https://www.facebook.com/telybazaar/" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                </div>
            </div>
        </div>

        
        <!-- Javascript -->
        <script src="{{ asset('/js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/jquery.backstretch.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('/js/scripts.js') }}"></script>
        
        <!--[if lt IE 10]>
            <script src="{{ asset('/js/placeholder.js') }}"></script>
        <![endif]-->

        
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-80054129-6', 'auto');
          ga('send', 'pageview');
        </script>

    </body>

</html>