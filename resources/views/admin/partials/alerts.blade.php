
@if($errors->any())
<div class="alert alert-danger">
@foreach($errors->all() as $error)
<p><i class="fa fa-info-circle"></i> {{ $error }}</p>
@endforeach
</div>
@endif