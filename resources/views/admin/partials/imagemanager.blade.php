<template id="imagemanager">
<div class="modal" id="images-modal">
  <div class="modal-dialog modal-lg" role="document" style="z-index:1040;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select image</h4>
      </div>
      <div class="modal-body">
        <div class="">
          <h4 style="margin-top:0;">Add new</h4>
          <div>
            <form action="{{ route('images.store', ['type' => isset($type) ? $type : '']) }}" class="dropzone" id="dropzone">
              {{ csrf_field() }}
              <div class="fallback">
                <input type="file" name="image" multiple="" />
              </div>
            </form>
          </div>
        </div>
        <hr>
        <div class="gallery">
          <h4>Gallery</h4>
          <div class="row">
            <div class="col-md-4">
              <input type="text" class="form-control" placeholder="Search" v-model="search" />
            </div>
          </div>
          <div class="row" style="padding-top:10px;">
            <div class="col-md-3" v-for="image in images">
              <div class="gallery-thumb">
                <a class="image" href="#" @click="select($event, image)">
                  <img v-image="image.filename" v-bind:alt="image.name" />
                </a>
                <div class="text">
                  <button class="btn btn-danger btn-xs" @click="deleteImage(image.id)"><i class="fa fa-trash"></i></button> <span v-bind:title="image.name" v-if="!image.editing" @click="image.editing = true;image.old_name = image.name">@{{ nameShrink(image.name) }}</span><input type="text" class="input-mini" v-model="image.name" v-if="image.editing" @keyup.13="changeName(image)">
                </div>
              </div>
            </div>

            <div class="col-md-12" style="margin-top:10px;" v-if="!images.length">
              <div class="well" style="margin-bottom:0;">
                No images
              </div>
            </div>
            
            <div class="col-md-12">
              <nav aria-label="Page navigation">
                <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous" v-on:click="prevPage()">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li v-if="curPage > 3 && lastPage > 5"><a href="#" v-on:click="changePage(0)">1</a></li>
                  <li v-if="curPage > 3 && lastPage > 5"><a href="#">...</a></li>
                  <li v-for="page in pages" v-bind:class="[curPage == page ? 'active' : '']"><a href="#" v-on:click="changePage(page)">@{{ page + 1 }}</a></li>
                  <li v-if="curPage < lastPage - 3 && lastPage > 5"><a href="#">...</a></li>
                  <li v-bind:class="[curPage == lastPage ? 'active' : '']" v-if="total % limit > 0"><a href="#" v-on:click="changePage(lastPage)">@{{ lastPage + 1 }}</a></li>
                  <li>
                    <a href="#" aria-label="Next" v-on:click="nextPage()">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" @click="saveimages()">Save</button>
      </div>
    </div>
  </div>
</div>
</template>

<script>
  function loadImage(el, binding) {
    el.src = '/img/loading.gif';
    if(binding.value){
    var image = new Image();
    image.onload = function() {
      el.src = binding.value
    }
    image.src = binding.value;
    }
  }
  var ImageManager = Vue.extend({
    template: '#imagemanager',
    data: function() {
      return {
        images: [],
        uploaded: [],
        search: '',
        limit: 8,
        offset: 0,
        total: 0
      }
    },
    props: {
      bus: {
        type: Object,
        required: true
      }
    },
    directives: {
      image: {
        update: loadImage,
        inserted: loadImage
      }
    },
    mounted: function() {
      var self = this;
      this.loadImages();
      Dropzone.options.dropzone = {
        paramName: 'image',
        success: function(file, rsp) {
          console.log(rsp)
          self.images.unshift(rsp.image);
          self.uploaded.push(rsp.image);
          if(self.curPage != 0) {
            self.changePage(0);
          } else if(self.images.length > 8) {
            self.images.pop();
          }
        },
        error: function(file, rsp) {
          if(rsp.status == 'error') {
            alert('Tinypng Error');
          }
          // console.log(rsp)
        }
      }
    },
    watch: {
      search: _.debounce(function() {
        this.loadImages();
      }, 500)
    },
    computed: {
      pages: function() {
        var curPage = this.curPage,
            lastPage = this.lastPage;
        if(lastPage < 7) { // minimal
          return _.range(0, lastPage);
        }
        if(curPage < 4) { // leading
          return _.range(0, 5);
        }
        if(curPage > lastPage - 4) { // trailing
          return _.range(lastPage - 4, lastPage);
        }
        return [curPage - 1, curPage, curPage + 1];
      },
      curPage: function() {
        return this.offset / this.limit;
      },
      lastPage: function() {
        return Math.floor(this.total / this.limit);
      }
    },
    methods: {
      changeName: function(image) {
        if(image.name && image.name != image.old_name) {
          $.ajax({
            url: `/admin/images/${image.id}`,
            method: 'POST',
            data: {
              _token: '{{ csrf_token() }}',
              _method: 'PATCH',
              name: image.name
            },
            success: function() {
              image.editing = false;
            }
          })
        } else {
          image.editing = false;
          image.name = image.old_name;
        }
      },
      nameShrink: function(name) {
        if(name.length > 20) {
          return name.slice(0, 20).concat('...');
        }
        return name;
      },
      loadImages: function() {
        var self = this;
        function dummies() {
            var dummies = [];
            var dummy = {editing:false,name:'',filename:'',id:''};
            for(var i = 0; i < 8; i++) {
              dummies.push(Object.assign({}, dummy));
            } 
            return dummies;
        }
        this.images = dummies();
        var conditionals = {};
        if(this.search) conditionals.q = this.search;
        if(this.offset) conditionals.offset = this.offset;
        if(this.limit) conditionals.limit = this.limit;
        $.ajax({
          url: '{{ route('images.index') }}?' + $.param(conditionals),
          method: 'GET',
          cache: false,
          success: function(rsp) {
            _.each(rsp.images, function(image) {
              image.editing = false;
            })
            self.images = rsp.images;
            if(self.total != rsp.count) self.offset = 0;
            self.total = rsp.count;
          }
        })
      },
      nextPage: function() {
        var curPage = this.curPage;
        if(curPage < this.lastPage) {
          this.offset = (curPage + 1) * this.limit;
          this.loadImages();
        }
      },
      prevPage: function() {
        var curPage = this.curPage;
        if(curPage > 0) {
          this.offset = (curPage - 1) * this.limit;
          this.loadImages();
        }
      },
      changePage: function(page) {
        // console.log(page)
        this.offset = page * this.limit;
        this.loadImages();
      },
      select: function(e, img) {
        // this.selectAction(img);
        this.bus.$emit('changeImage', img);
        // $(this.invoker).find('img').attr('src', img.filename);
        // $(this.invoker).find('input[type=hidden]').val(img.id);
        $("#images-modal").modal('hide');
      },
      saveimages: function(e) {
        this.bus.$emit('submitImages', this.uploaded);
        this.uploaded = [];
        $("#images-modal").modal('hide');
      },
      deleteImage: function(id) {
        var self = this;
        if(confirm('Are you sure you want to delete this image?')) {
          $.ajax({
            url: `/admin/images/${id}`,
            method: 'POST',
            data: {
              _token: '{{ csrf_token() }}',
              _method: 'DELETE'
            },
            success: function(rsp) {
              self.bus.$emit('deleteImage', id);
              self.loadImages();
            }
          })
        }
      }
    }
  });

</script>