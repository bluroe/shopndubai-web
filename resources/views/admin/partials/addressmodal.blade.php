
<div class="modal" id="address-modal">
  <div class="modal-dialog modal-lg" role="document" style="z-index:1040;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Address</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" id="address-form">
        @include('admin.customers.partials.address')
        </form>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>