@extends('layouts.admin-master')

@section('title', 'Categories')

@section('head')
<style>

  .node {
    cursor: pointer;
  }

  .overlay{
      background-color:#EEE;
  }
   
  .node circle {
    fill: #fff;
    stroke: steelblue;
    stroke-width: 1.5px;
  }
   
  .node text {
    font-size:10px; 
    font-family:sans-serif;
  }
   
  .link {
    fill: none;
    stroke: #ccc;
    stroke-width: 1.5px;
  }

  .templink {
    fill: none;
    stroke: red;
    stroke-width: 3px;
  }

  .ghostCircle.show{
      display:block;
  }

  .ghostCircle, .activeDrag .ghostCircle{
       display: none;
  }

</style>
@endsection

@section('content')
<categories-view></categories-view>
@endsection

@section('vue-components')
<template id="categories">
<div class="row">
  <div class="col-md-12">
    <div class="toolbar">
      <div class="leading">
        <div class="dropdown">
          <a class="head" href="#" data-toggle="dropdown">Change View <i class="caret"></i></a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">View</li>
            <li><a href="#" v-on:click="changeView('table')">Table<i class="fa fa-check" v-if="view == 'table'"></i></a></li>
            <li><a href="#" v-on:click="changeView('tree')">Tree<i class="fa fa-check" v-if="view == 'tree'"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <category-table :search="search" v-if="view == 'table'"></category-table>
    <category-tree v-if="view == 'tree'"></category-tree>
  </div>
</div>
</template>
<template id="category-table">
  <div>
    <div class="toolbar">
      <div class="leading">
          <a class="head" href="#">Categories</a>
      </div>
      <div class="form-control search-input">
        <i class="fa fa-search"></i>
        <input type="text" v-model="search" v-on:input="loading = true" id="search">
        <button v-on:click="search = ''" class="clear" v-show="search">
          <i class="fa fa-times-circle"></i>
        </button>
      </div>
      <div class="buttons">
        <a class="btn btn-success" href="{{ route('categories.create') }}"><i class="fa fa-plus"></i> Add new</a>
      </div>
    </div>
    <div class="table-container">
      <div class="lo" v-show="loading">
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Subcategories</th>
            <th>Products</th>
            <th>Order</th>
            <th>Active</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody ref="table_body">
          <tr v-for="(category, index) in categories">
            <td>@{{ category.fullname }}</td>
            <td>@{{ category.children.length }}</td>
            <td>@{{ category.products.length }}</td>
            <td>@{{ category.order }}</td>
            <td><span v-bind:class="[category.active ? 'text-success' : 'text-danger']">@{{ category.active ? 'Yes' : 'No' }}</span></td>
            <td>
              <a v-bind:href="'/admin/categories/' + category.id + '/edit'" class="btn btn-xs btn-primary">Edit</a>
              <button type="button" class="btn btn-xs btn-danger" v-on:click="deleteCategory(category, index)">Delete</button>
            </td>
          </tr>
          <tr v-if="categories.length == 0">
            <td colspan="5" align="center">@{{ loading ? 'Loading...' : 'No items' }}</td>
          </tr>
        </tbody>
      </table>
    </div>

      <div class="table-foot">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous" v-on:click="prevPage()">
              <i class="fa fa-chevron-left"></i>
            </a>
          </li>
          <li>
            <a href="#" aria-label="Next" v-on:click="nextPage()">
              <i class="fa fa-chevron-right"></i>
            </a>
          </li>
        </ul>
        <label>Page</label>
        <input class="form-control" v-model="page" type="number">
        <label>of @{{ Math.ceil(total / limit) }}</label>
      </div>
  </div>
</template>
<template id="category-tree">
  <div></div>
</template>
<script>
  Vue.component('category-tree', {
    mounted: function() {
      var self = this;
      treeJSON = d3.json('/admin/category/tree', function(error, treeData) {
        treeData.name = 'Categories';
        treeData.children = treeData.data;
        delete treeData.data;
        // Calculate total nodes, max label length
        var totalNodes = 0;
        var maxLabelLength = 0;
        // variables for drag/drop
        var selectedNode = null;
        var draggingNode = null;
        // panning variables
        var panSpeed = 200;
        var panBoundary = 20; // Within 20px from edges will pan when dragging.
        // Misc. variables
        var i = 0;
        var duration = 750;
        var root;

        // size of the diagram
        var viewerWidth = $(self.$el).width();
        var viewerHeight = $(document).height() - 113;

        var tree = d3.layout.tree()
            .size([viewerHeight, viewerWidth]);

        // define a d3 diagonal projection for use by the node paths later on.
        var diagonal = d3.svg.diagonal()
            .projection(function(d) {
                return [d.y, d.x];
            });

        // A recursive helper function for performing some setup by walking through all nodes

        function visit(parent, visitFn, childrenFn) {
            if (!parent) return;

            visitFn(parent);

            var children = childrenFn(parent);
            if (children) {
                var count = children.length;
                for (var i = 0; i < count; i++) {
                    visit(children[i], visitFn, childrenFn);
                }
            }
        }

        // Call visit function to establish maxLabelLength
        visit(treeData, function(d) {
            totalNodes++;
            maxLabelLength = Math.max(d.name.length, maxLabelLength);

        }, function(d) {
            return d.children && d.children.length > 0 ? d.children : null;
        });


        // sort the tree according to the node names

        function sortTree() {
            tree.sort(function(a, b) {
                return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
            });
        }
        // Sort the tree initially incase the JSON isn't in a sorted order.
        sortTree();

        // TODO: Pan function, can be better implemented.

        function pan(domNode, direction) {
            var speed = panSpeed;
            if (panTimer) {
                clearTimeout(panTimer);
                translateCoords = d3.transform(svgGroup.attr("transform"));
                if (direction == 'left' || direction == 'right') {
                    translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                    translateY = translateCoords.translate[1];
                } else if (direction == 'up' || direction == 'down') {
                    translateX = translateCoords.translate[0];
                    translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
                }
                scaleX = translateCoords.scale[0];
                scaleY = translateCoords.scale[1];
                scale = zoomListener.scale();
                svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
                d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
                zoomListener.scale(zoomListener.scale());
                zoomListener.translate([translateX, translateY]);
                panTimer = setTimeout(function() {
                    pan(domNode, speed, direction);
                }, 50);
            }
        }

        // Define the zoom function for the zoomable tree

        function zoom() {
            svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        }


        // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
        var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

        // define the baseSvg, attaching a class for styling and the zoomListener
        var baseSvg = d3.select(self.$el).append("svg")
            .attr("width", viewerWidth)
            .attr("height", viewerHeight)
            .attr("class", "overlay")
            .call(zoomListener);

        // Helper functions for collapsing and expanding nodes.

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        function expand(d) {
            if (d._children) {
                d.children = d._children;
                d.children.forEach(expand);
                d._children = null;
            }
        }

        var overCircle = function(d) {
            selectedNode = d;
            updateTempConnector();
        };
        var outCircle = function(d) {
            selectedNode = null;
            updateTempConnector();
        };

        // Function to update the temporary connector indicating dragging affiliation
        var updateTempConnector = function() {
            var data = [];
            if (draggingNode !== null && selectedNode !== null) {
                // have to flip the source coordinates since we did this for the existing connectors on the original tree
                data = [{
                    source: {
                        x: selectedNode.y0,
                        y: selectedNode.x0
                    },
                    target: {
                        x: draggingNode.y0,
                        y: draggingNode.x0
                    }
                }];
            }
            var link = svgGroup.selectAll(".templink").data(data);

            link.enter().append("path")
                .attr("class", "templink")
                .attr("d", d3.svg.diagonal())
                .attr('pointer-events', 'none');

            link.attr("d", d3.svg.diagonal());

            link.exit().remove();
        };

        // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

        function centerNode(source) {
            scale = zoomListener.scale();
            x = -source.y0;
            y = -source.x0;
            x = x * scale + viewerWidth / 2;
            y = y * scale + viewerHeight / 2;
            d3.select('g').transition()
                .duration(duration)
                .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
            zoomListener.scale(scale);
            zoomListener.translate([x, y]);
        }

        function alignRoot(source) {
            scale = zoomListener.scale();
            x = -source.y0;
            y = -source.x0;
            x = 100;
            y = y * scale + viewerHeight / 2;
            d3.select('g').transition()
                .duration(duration)
                .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
            zoomListener.scale(scale);
            zoomListener.translate([x, y]);
        }

        // Toggle children function

        function toggleChildren(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else if (d._children) {
                d.children = d._children;
                d._children = null;
            }
            return d;
        }

        // Toggle children on click.

        function click(d) {
            if (d3.event.defaultPrevented) return; // click suppressed
            d = toggleChildren(d);
            update(d);
            centerNode(d);
        }

        function update(source) {
            // Compute the new height, function counts total children of root node and sets tree height accordingly.
            // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
            // This makes the layout more consistent.
            var levelWidth = [1];
            var childCount = function(level, n) {

                if (n.children && n.children.length > 0) {
                    if (levelWidth.length <= level + 1) levelWidth.push(0);

                    levelWidth[level + 1] += n.children.length;
                    n.children.forEach(function(d) {
                        childCount(level + 1, d);
                    });
                }
            };
            childCount(0, root);
            var newHeight = d3.max(levelWidth) * 25; // 25 pixels per line  
            tree = tree.size([newHeight, viewerWidth]);

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);

            // Set widths between levels based on maxLabelLength.
            nodes.forEach(function(d) {
                d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
                // alternatively to keep a fixed scale one can set a fixed depth per level
                // Normalize for fixed-depth by commenting out below line
                // d.y = (d.depth * 500); //500px per level.
            });

            // Update the nodes…
            node = svgGroup.selectAll("g.node")
                .data(nodes, function(d) {
                    return d.id || (d.id = ++i);
                });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on('click', click);

            nodeEnter.append("circle")
                .attr('class', 'nodeCircle')
                .attr("r", 0)
                .style("fill", function(d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

            nodeEnter.append("text")
                .attr("x", function(d) {
                    return d.children || d._children ? -10 : 10;
                })
                .attr("dy", ".35em")
                .attr('class', 'nodeText')
                .attr("text-anchor", function(d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function(d) {
                    return d.name;
                })
                .style("fill-opacity", 0);

            // phantom node to give us mouseover in a radius around it
            nodeEnter.append("circle")
                .attr('class', 'ghostCircle')
                .attr("r", 30)
                .attr("opacity", 0.2) // change this to zero to hide the target area
            .style("fill", "red")
                .attr('pointer-events', 'mouseover')
                .on("mouseover", function(node) {
                    overCircle(node);
                })
                .on("mouseout", function(node) {
                    outCircle(node);
                });

            // Update the text to reflect whether node has children or not.
            node.select('text')
                .attr("x", function(d) {
                    return d.children || d._children ? -10 : 10;
                })
                .attr("text-anchor", function(d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function(d) {
                    return d.name;
                });

            // Change the circle fill depending on whether it has children and is collapsed
            node.select("circle.nodeCircle")
                .attr("r", 4.5)
                .style("fill", function(d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function(d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            // Fade the text in
            nodeUpdate.select("text")
                .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function(d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            nodeExit.select("circle")
                .attr("r", 0);

            nodeExit.select("text")
                .style("fill-opacity", 0);

            // Update the links…
            var link = svgGroup.selectAll("path.link")
                .data(links, function(d) {
                    return d.target.id;
                });

            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function(d) {
                    var o = {
                        x: source.x0,
                        y: source.y0
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                });

            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function(d) {
                    var o = {
                        x: source.x,
                        y: source.y
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                })
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function(d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

        // Append a group which holds all nodes and which the zoom Listener can act upon.
        var svgGroup = baseSvg.append("g");

        // Define the root
        root = treeData;
        root.x0 = viewerHeight / 2;
        root.y0 = 0;

        // Layout the tree initially and center on the root node.
        update(root);
        // centerNode(root);
        alignRoot(root);
      });
    },
    template: '#category-tree'
  });
  Vue.component('category-table', {
    data: function() {
      return {
        search: '',
        categories: [],
        loading: false,
        total: 0,
        limit: 10,
        page: 1, 
      }
    },
    watch: {
      page: function() {
        this.loadCategories();
      },
      loading: function(val, oldVal) {
        var table_body = this.$refs.table_body
        if(val) {
            var height = table_body.getBoundingClientRect().height;
            var lo = document.createElement('div');
            lo.className = 'lo';
            lo.style.height = height + 'px';
            table_body.appendChild(lo);
            this.$refs.lo = lo;
          } else {
          table_body.removeChild(this.$refs.lo);
        }
      },
      search: _.debounce(function() {
        this.loadCategories();
      }, 500)
    },
    mounted: function() {
      this.loadCategories();
    },
    methods: {
      prevPage: function() {
        if(this.page > 1) {
          this.page = this.page - 1;
        }
      },
      nextPage: function() {
        var lastPage = Math.ceil(this.total / this.limit);
        if(this.page < lastPage) {
          this.page = this.page + 1;
        }
      },
      loadCategories: function() {
        var self = this;
        self.loading = true;
        var offset = (this.page - 1) * this.limit;
        var conditionals = {offset:offset};
        if(this.search) conditionals.q = this.search;
        if(this.limit) conditionals.limit = this.limit;
        $.ajax({
          url: '{{ route('categories.index') }}?' + $.param(conditionals),
          method: 'GET',
          success: function(rsp) {
            self.categories = rsp.categories;
            self.total = rsp.count;
            self.loading = false;
          },
          cache: false
        });
      },
      deleteCategory: function(category, index) {
        var self = this;
        if(confirm('Are you sure you want to delete this category? This will delete all the subcategories and products. This cannot be undone!')) {
          $.post('/admin/categories/' + category.id, {
            _method: 'DELETE',
            _token: '{{ csrf_token() }}'
          }).success(function(data) {
            self.categories = self.categories.slice(0, index).concat(self.categories.slice(index + 1));
          }).error(function(err) {
            console.log(err.responseText);
          });
        }
      }
    },
    template: '#category-table'
  });
  var urlParams = new URLSearchParams(window.location.search);
  Vue.component('categories-view', {
    data: function() {
      return {
        view: urlParams.has('view') ? urlParams.get('view') : 'table',
        search: '',
      }
    },
    methods: {
      changeView: function(view) {
        console.log(view)
        this.view = view;
        history.replaceState({}, 'Categories', location.pathname + '?view=' + view);
      }
    },
    template: '#categories'
  })
</script>
<script src="https://d3js.org/d3.v3.min.js"></script>
@endsection
