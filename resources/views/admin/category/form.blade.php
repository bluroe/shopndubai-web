@extends('layouts.admin-master')

@section('title', isset($category) ? 'Edit Category - ' . $category->name : 'Create Category')

@section('scripts')
<template id="categoryform">
  <div class="row">
    <div class="col-lg-8 col-md-10">
      <div class="title">
        <h3>{{ isset($category) ? 'Edit' : 'Create' }} Category</h3>
      </div>
      @include('admin.partials.alerts')
      @if(isset($category))
      <form method="POST" action="{{ route('categories.update', $category) }}" class="form-horizontal">
        <input type="hidden" name="_method" value="PATCH">
      @else
      <form method="POST" action="{{ route('categories.store') }}" class="form-horizontal">
      @endif
        {{ csrf_field() }}

        <div class="form-group">
          <label class="col-md-4 control-label">Name</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="name" value="{{ $category->name or old('name') }}" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label">Subcategory of</label>
          <div class="col-md-8">
            {!! Form::select('parent_id', ['' => 'None'] + $categories, isset($category) ? $category->parent_id : old('parent_id'), ['class' => 'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label">Image</label>
          <div class="col-md-8">
            <button type="button" class="img-btn" v-on:click="showModal">
              <input type="hidden" name="image" v-model="image.id">
              <img v-bind:src="image.filename ? image.filename : '/img/no-image.png'">
            </button>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label">Order</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="order" value="{{ $category->order or old('order') }}" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label">Active</label>
          <div class="col-md-8 checkbox">
            <label>
              <input type="hidden" name="active" value="0">
              {!! Form::checkbox('active', 1, isset($category) ? $category->active : true) !!}
            </label>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-success">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</template>
@include('admin.partials.imagemanager', ['type' => 'category'])
  
<script>

  var bus = new Vue(); // comm bus

  var CategoryForm = Vue.extend({
    template: '#categoryform',
    data: function() {
      return {
        @if(isset($category->image))
        image: {id:{{ $category->image->id }}, filename: '{{ $category->image->filename }}'}
        @else
        image: {id:null, filename: null}
        @endif
      }
    },
    methods: {
      showModal: function(image) {
        $('#images-modal').modal('show');
      }
    },
    mounted: function() {
      var self = this;
      bus.$on('changeImage', function(image) {
        self.image.id = image.id;
        self.image.filename = image.filename;
      });
    }
  });

  var child = new Vue({
    parent: vm,
    el: '#v-container',
    render: function(createElement) {
      return createElement('div', {}, [
        createElement(CategoryForm),
        createElement(ImageManager, {
          props: {
            bus: bus
          }
        })
      ]);
    }
  });
</script>
@endsection
