@extends('layouts.admin-master')

@section('title', 'Customers')

@section('content')
<customer-view></customer-view>
@endsection

@section('vue-components')
<template id="customers">
	<div class="row">
		<div class="col-md-12">
			<div class="toolbar">
				<div class="leading">
					<a href="#" class="head">Customers</a>
				</div>
				<div class="form-control search-input">
					<i class="fa fa-search"></i>
					<input type="text" v-model="search" v-on:keyup="filterCustomers" placeholder="Search">
					<button v-on:click="search = ''" class="clear" v-show="search">
						<i class="fa fa-times-circle"></i>
					</button>
				</div>
			</div>
			<div class="table-container">
				<table class="table" id="customer-table">
					<thead>
						<tr>
							<th>Key</th>
							<th>Mobile</th>
							<th>Orders</th>
							<th>Status</th>
							<th>Joined</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody ref="table_body">
						<tr v-if="customers.length == 0">
							<td colspan="5" align="center">@{{ loading ? 'Loading...' : 'No Results' }}</td>
						</tr>
						<template>
						<tr v-for="customer in customers">
							<td>@{{ customer.key }}</td>
							<td>@{{ customer.mobile }}</td>
							<td>@{{ customer.orders }}</td>
							<td><span v-bind:class="[customer.active ? 'text-success' : 'text-danger']">@{{ customer.active ? 'Active' : 'Inactive' }}</span></td>
							<td>@{{ dateFormat(customer.created_at) }}</td>
							<td>
								<a v-bind:href="'#'" class="btn btn-xs btn-primary">Edit</a>
								<button type="button" class="btn btn-xs btn-danger" v-on:click="deleteC(customer.id)">Delete</button>
							</td>
						</tr>
						</template>
					</tbody>
				</table>
			</div>
			<div class="table-foot">
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous" v-on:click="prevPage()">
							<i class="fa fa-chevron-left"></i>
						</a>
					</li>
					<li>
						<a href="#" aria-label="Next" v-on:click="nextPage()">
							<i class="fa fa-chevron-right"></i>
						</a>
					</li>
				</ul>
				<label>Page</label>
				<input class="form-control" v-model="page" type="number">
				<label>of @{{ Math.ceil(total / limit) }}</label>
			</div>
		</div>
	</div>
</template>
<script>
	Vue.component('customer-view', {
		data: function() {
			return {
				customers: [],
				loading: false,
				limit: 10,
				page: 1,
				total: 0,
				search: ''
			}
		},
	    watch: {
	    	page: function() {
	    		this.load();
	    	},
	    	limit: function() {
	    		this.load();
	    	},
			loading: function(val, oldVal) {
				var table_body = this.$refs.table_body
				if(val) {
				    var height = table_body.getBoundingClientRect().height;
				    var lo = document.createElement('div');
				    lo.className = 'lo';
				    lo.style.height = height + 'px';
				    table_body.appendChild(lo);
				    this.$refs.lo = lo;
					} else {
					table_body.removeChild(this.$refs.lo);
				}
			}
	  	},
		mounted: function() {
			this.load();
		},
		methods: {
			filterCustomers: _.debounce(function() {
				this.load();
			}, 500),
			prevPage: function() {
				if(this.page > 1) {
					this.page = this.page - 1;
				}
			},
			nextPage: function() {
				var lastPage = Math.ceil(this.total / this.limit);
				if(this.page < lastPage) {
					this.page = this.page + 1;
				}
			},
			load: function() {
				var self = this;
				this.loading = true;
				var offset = (this.page - 1) * this.limit;
				var data = {
					limit: this.limit,
					offset: offset,
					q: this.search
				}
				$.ajax({
					url: '/admin/customers?' + $.param(data),
					method: 'GET',
					success: function(rsp) {
						self.customers = rsp.customers;
						self.loading = false;
						self.total = rsp.total;
					}
				})
			},
			dateFormat: function(date) {
				return moment(date).format('DD/MM/YYYY');
			},
			deleteC: function(id) {
				var self = this;
				if(confirm('Are you sure you want to delete this customer? All data related to this customer will be lost. This cannot be undone!')) {
					$.post('/admin/customers/' + id, {
						_method: 'DELETE',
						_token: '{{ csrf_token() }}'
					}).success(function(data) {
						console.log(data);
						self.customers = self.customers.filter(function(c) {
							return c.id != id
						});
					}).error(function(err) {
						console.log(err.responseText);
					});
				}
			}
		},
		template: '#customers'
	});
</script>
@endsection
