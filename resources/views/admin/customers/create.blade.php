@extends('layouts.admin-master')

@section('title', 'Create Customer')

@include('admin.customers.partials.form')