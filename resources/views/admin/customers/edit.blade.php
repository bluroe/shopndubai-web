@extends('layouts.admin-master')

@section('title', 'Edit Customer - ' . $customer->user->name)

@include('admin.customers.partials.form')