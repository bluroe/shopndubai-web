
<div class="form-group">
	<label class="col-md-4 control-label">First Name <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<input type="text" name="firstname" class="form-control" required>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Last Name <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<input type="text" name="lastname" class="form-control" required>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Address Line 1 <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<textarea class="form-control" name="address1" rows="5" required></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Address Line 2</label>
	<div class="col-md-8">
		<textarea class="form-control" name="address2" rows="5"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">City</label>
	<div class="col-md-8">
		<input type="text" name="city" class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Area</label>
	<div class="col-md-8">
		<input type="text" name="area" class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">PIN <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<input type="text" name="pin" class="form-control" required>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Email <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<input type="email" name="email" class="form-control" required>
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label">Phone no <span class="text-danger">*</span></label>
	<div class="col-md-8">
		<input type="text" name="phone" class="form-control" required>
	</div>
</div>