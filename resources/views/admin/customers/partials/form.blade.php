@section('content')
@if(isset($customer))
<ul class="nav nav-tabs">
	<li class="active"><a href="#data" data-toggle="tab">Data</a></li>
	<li><a href="#address-book" data-toggle="tab">Address Book</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="data">
@endif
		<div class="col-md-8">
			@include('admin.partials.alerts')
			@if(isset($customer))
			<form method="POST" action="{{ route('customers.update', $customer) }}" class="form-horizontal">
				<input type="hidden" name="_method" value="PATCH">
			@else
			<form method="POST" action="{{ route('customers.store') }}" class="form-horizontal">
			@endif
				{{ csrf_field() }}
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" name="name" class="form-control" value="{{ $customer->user->name or old('name') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input type="email" name="email" class="form-control" value="{{ $customer->user->email or old('email') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-8">
						<input type="password" name="password" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Confirm Password</label>
					<div class="col-md-8">
						<input type="password" name="password_confirmation" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Active</label>
					<div class="col-md-8 checkbox">
						<label>
							<input type="hidden" name="active" value="0">
							{!! Form::checkbox('active', 1, isset($customer) ? $customer->active : old('active')) !!}
						</label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
@if(isset($customer))
	</div>
	<div class="tab-pane" id="address-book">
		<div class="toolbar">
			<div class="pull-right">
				<button class="btn btn-success" data-toggle="modal" data-target="#address-modal">Add new</button>
			</div>
			<div class="clearfix"></div>
		</div>
		<table class="table table-bordered" id="address-table">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Address Line 1</th>
					<th>Phone</th>
					<th>Email</th>
					<th>PIN</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($customer->addresses as $address)
				<tr>
					<td>{{ $address->firstname }}</td>
					<td>{{ $address->lastname }}</td>
					<td>{{ $address->address1 }}</td>
					<td>{{ $address->phone }}</td>
					<td>{{ $address->email }}</td>
					<td>{{ $address->pin }}</td>
					<td>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#address-modal">Edit</button>
						<button type="button" class="btn btn-danger del-btn" data-id="{{ $address->id }}">Delete</button>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="7" align="center">No data</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@include('admin.partials.addressmodal')
@endif
@endsection

@if(isset($customer))
@section('scripts')
<script>
function deleteHandler(e) {
	var btn = $(this);
	var id = btn.attr('data-id');
	if(confirm('Are you sure you want to delete this address? This cannot be undone!')) {
		$.post('/admin/customers/{{ $customer->id }}/addresses/' + id, {
			_token: '{{ csrf_token() }}',
			_method: 'DELETE'
		})
		.success(function(data) {
			// console.log(data);
			btn.closest('tr').remove();
			var tbody = $("#address-table tbody");
			if(tbody.children().length == 0) {
				tbody.append('<tr><td colspan="7" align="center">No data</td></tr>');
			}
		})
		.error(function(err) {
			console.log(err.responseText);
		});
	}
}
$('.del-btn').click(deleteHandler);
$("#address-modal .btn-primary").click(function() {
	console.log('validating')
	var valid = $("#address-form").parsley().validate();
	if(valid) {
		var data = {};
		$.each($('#address-form').serializeArray(), function() {
			data[this.name] = this.value;
		});
		data._token = '{{ csrf_token() }}';
		// console.log(data);
		$.post('{{ route('customers.addresses.store', $customer) }}', data)
			.success(function(rsp) {
				// console.log(rsp)
				var rows = $("#address-table tbody tr");
				if(rows.length == 1 && rows.eq(0).find('td').length == 1) {
					rows.remove();
				}
				$("#address-table tbody").append(
					'<tr>' + 
						'<td>' + data.firstname + '</td>' +
						'<td>' + data.lastname + '</td>' +
						'<td>' + data.address1 + '</td>' +
						'<td>' + data.phone + '</td>' +
						'<td>' + data.email + '</td>' +
						'<td>' + data.pin + '</td>' +
						'<td>' +
							'<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#address-modal">Edit</button>&nbsp;' +
							'<button type="button" class="btn btn-danger del-btn" data-id="' + rsp.id + '">Delete</button>' +
						'</td>' +
					'</tr>'
				);
				$('#address-form').get(0).reset();
				$("#address-form").parsley().reset();
				$('.del-btn').unbind().click(deleteHandler);
				$("#address-modal").modal('hide');
			})
			.error(function(err) {
				console.log(err.responseText)
			});
	}
});
</script>
@endsection
@endif

