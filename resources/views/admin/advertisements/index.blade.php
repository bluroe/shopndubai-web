@extends('layouts.admin-master')

@section('title', 'Advertisements')

@section('scripts')

<template id="advertisements">
	<div>
		<div class="modal" tabindex="-1" role="dialog" id="advertisement-modal">
		  <div class="modal-dialog" role="document">
		    <form class="modal-content" v-bind:action="'/admin/advertisements/' + (advertisement ? advertisement.id : '')" method="POST" enctype="multipart/form-data">
		      <div class="modal-header">
		        <button type="button" class="close" @click="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">@{{ modal_title }}</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="form-horizontal">
		      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<input type="hidden" name="_method" value="PATCH" v-if="advertisement">
			        <div class="form-group">
			        	<label class="control-label col-md-4">Name</label>
			        	<div class="col-md-8">
			        		<input type="text" name="name" class="form-control" />
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Image</label>
			        	<div class="col-md-8">
			        		<div>
			        			<input type="file" name="image" @change="changeImage($event)">
			        		</div>
			        		<img v-bind:src="preview ? preview : '/img/no-image.png'" style="max-width:100%;margin-top:10px;">
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Starting Date</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" name="from_date" placeholder="dd/mm/yyyy" />
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Expiry Date</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" name="to_date" placeholder="dd/mm/yyyy" />
			        	</div>
			        </div>
			    </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" @click="closeModal()">Close</button>
		        <button type="submit" class="btn btn-primary">Save changes</button>
		      </div>
		    </form><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="toolbar">
			<div class="leading">
				<a href="#" class="head">Advertisements</a>
			</div>
		  <div class="buttons">
		    <button class="btn btn-success" @click="openModal()"><i class="fa fa-plus"></i> Add new</button>
		  </div>
		  <div class="clearfix"></div>
		</div>

		<table id="advertisements-table" class="table">
		  <thead>
		    <tr>
		      <th>Name</th>
		      <th>Starting Date</th>
		      <th>Expiry Date</th>
		      <th>Status</th>
		      <th>Actions</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<tr v-for="(ad, index) in ads">
		  		<td>@{{ ad.name }}</td>
		  		<td>@{{ ad.from_date }}</td>
		  		<td>@{{ ad.to_date }}</td>
		  		<td><span v-bind:class="[ad.active ? 'text-success' : 'text-danger']">@{{ ad.active ? 'Active' : 'Disabled' }}</span></td>
		  		<td><button class="btn btn-danger btn-xs" @click="deleteAd(ad, index)">Delete</button></td>
		  	</tr>
		  	<tr v-if="!ads.length">
		  		<td colspan="5" align="center">No ads</td>
		  	</tr>
		  </tbody>
		</table>
	</div>
</template>

<script type="text/javascript">
	var child = new Vue({
		el: '#v-container',
		data: {
			ads: [],
			modal_title: 'Create Ad',
			preview: null,
			advertisement: null
		},
		methods: {
			openModal: function() {
				$('#advertisement-modal').modal('show');
			},
			closeModal: function() {
				this.name = null;
				this.image = null;
				this.from_date = null;
				this.to_date = null;
				this.preview = null;
				$('#advertisement-modal').modal('hide');
			},
			changeImage: function(e) {
				var self = this;
				var file = e.target.files[0]
				var reader = new FileReader();
				reader.onloadend = function() {
					self.preview = reader.result;
					console.log('loaded')
				}
				reader.readAsDataURL(file);
			},
			saveAd: function() {
				var data = {
					_token: '{{ csrf_token() }}',
					image: this.image,
					from_date: this.from_date,
					to_date: this.to_date,
					name: this.name,
					processData: false
				}
				console.log(data)
				$.ajax({
					url: '/admin/advertisements',
					method: 'POST',
					data: data,
    				contentType: 'multipart/form-data',
					success: function(rsp) {
						console.log(rsp);
					}
				})
			},
			deleteAd: function(ad, index) {
				var self = this;
				if(confirm('Are you sure you want to delete this ad?')) {
					$.ajax({
						url: '/admin/advertisements/' + ad.id,
						method: 'POST',
						data: {
							_token: '{{ csrf_token() }}',
							_method: 'DELETE'
						},
						success: function(rsp) {
							console.log(rsp)
							self.ads = self.ads.slice(0, index).concat(self.ads.splice(index + 1));
						}
					})
				}
			}
		},
		mounted: function() {
			var self = this;
			var date_format = 'DD/MM/YYYY';
    		$('input[name="from_date"]').daterangepicker({
        		singleDatePicker: true,
	            format: date_format
        	})
    		$('input[name="to_date"]').daterangepicker({
        		singleDatePicker: true,
	            format: date_format,
	            drops: 'up'
        	})
        	$.ajax({
        		method: 'GET',
        		url: '/admin/advertisements',
        		success: function(rsp) {
        			console.log(rsp)
        			self.ads = rsp;
        		}
        	})
		},
		template: '#advertisements'
	});
</script>

@endsection