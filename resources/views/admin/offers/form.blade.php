@extends('layouts.admin-master')

@section('title', isset($offer) ? 'Edit Offer - ' : 'Create Offer')

@section('scripts')
	<template id="offerform">
		<div>
			<div class="title">
				<h3>{{ isset($offer) ? 'Edit Offer' : 'Create Offer' }}</h3>
			</div>
			<div class="col-md-9">
				<form class="form-horizontal" action="{{ route('offers.store') }}" method="POST" id="order-form" data-parsley-validate>
					{{ csrf_field() }}
					<div class="form-group">
			        	<label class="control-label col-md-4">Product</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" id="product_name" v-model="product.name" required>
							<div v-if="invalidProduct">
								<ul class="parsley-errors-list filled">
									<li class="parsley-required">Invalid product</li>
								</ul>
							</div>
			        	</div>
			        </div>
			        <div id="variantDiv" class="form-group">
			        	<label class="control-label col-md-4">Type
			        	</label>
			        	<div class="col-md-8">
			        		<select class="form-control" v-model="product.variant" id="variantSelect">
			        			<option v-bind:value="variant.id" v-for="variant in product.variants">@{{ variant.type + ' - ' + variant.price }}</option>
			        		</select>
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Offer Text</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" v-model="offer_text" required>
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Offer Qty</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" v-model="offer_qty" required>
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Offer By</label>
			        	<div class="col-md-8">
			        		<select class="form-control" id="offerBy" v-model="offerBy">
			        			<option value="q">Quantity</option>
			        			<option value="dp">Discount percentage</option>
			        			<option value="dpr">Discount price</option>
			        		</select>
			        	</div>
			        </div>
			        <div class="form-group" id="addQty">
			        	<label class="control-label col-md-4">Additional Qty</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" v-model="add_qty">
			        	</div>
			        </div>
			        <div class="form-group" id="prDiv" hidden>
			        	<label class="control-label col-md-4"></label>
			        	<div class="col-md-4">
			        		<input type="text" class="form-control" v-model="percent">
			        	</div>
			        	<div class="col-md-4">
			        		<input type="text" class="form-control" placeholder="Amount after Offer" v-model="amount">
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Starting Date</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" v-model="from_date" name="start_date" placeholder="dd/mm/yyyy" />
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Expiry Date</label>
			        	<div class="col-md-8">
			        		<input type="text" class="form-control" v-model="to_date" name="end_date" placeholder="dd/mm/yyyy" />
			        	</div>
			        </div>
			        <div id="storeDiv" class="form-group">
			        	<label class="control-label col-md-4">Store
			        	</label>
			        	<div class="col-md-8">
			        		<select class="form-control" id="store_id" name="store_id" v-model="store">
			        		<option value>Select a store</option>
								<option v-for="store in order_stores" v-bind:value="store.id">
					                @{{ store.name }}
					            </option>

					        </select>
			        	</div>
			        </div>
			        <div class="form-group">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-success" v-on:click="submit($event)">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</template>	
	<script>
		var stub_product = {
			name: '{!! $offer->variant->product->name or '' !!}',
			qty: '{!! $offer->variant->qty or 0 !!}',
			price: '{!! $offer->variant->price or 0 !!}',
			variants: {!! json_encode( isset($offer) ? $variant : []) !!},
			variant: '{!! $offer->variant->id or 0 !!}',
		};
		var engine;

	  	var child = new Vue({
	    	el: '#v-container',
	    	data: function() {
				return {
					mode: '{!! isset($offer) ? 'edit' : 'new' !!}',
					product: stub_product,
					invalidProduct: false,
					offer_text: '{!! $offer->text or '' !!}',
					offer_qty: '{!! $offer->qty or '' !!}',
					add_qty: '{!! $offer->add_qty or '' !!}',
					offerBy: '{!! $offer->offer_by or '' !!}',
					percent: '{!! $offer->percent or '' !!}',
					order_stores: {!! json_encode($stores) !!},
					store: '{!! $offer->store->id or '' !!}',
					from_date: '{!! $offer->from_date or '' !!}',
					to_date: '{!! $offer->to_date or '' !!}',
					amount: '{!! $offer->price or '' !!}'
				}
			},
			watch: {
				percent: function(val) {
					var self = this;
					if(self.product.variant != "") {
						self.variant = self.product.variants.filter(function(p) {
							return p.id == self.product.variant;
						})[0];
					}

					if(self.offerBy == 'dpr') {
						self.amount =  self.variant.price - self.percent;
					} else if (self.offerBy == 'dp') {
						self.amount = self.variant.price - (self.variant.price * self.percent / 100);
					}
				}
			},
			methods: {
				submit: function(e) {
					e.preventDefault();
					var form = $("#order-form").parsley();
					form.validate();

					if(form.isValid()) {
						var self = this;
						var curr_store = this.store;
						var prod_store = this.order_stores.filter(function(s) {
							return s.id == curr_store;
						})[0];
						data = {
							variant_id: this.product.variant,
							text: this.offer_text,
							qty: this.offer_qty,
							offer_by: this.offerBy,
							add_qty: this.offerBy == 'q' ? this.add_qty : '',
							percent: this.offerBy != 'q' ? this.percent : '',
							price: this.amount != undefined ? this.amount : '',
							from_date: this.from_date,
							to_date: this.to_date,
							store_id: prod_store.id,
							_token: '{{ csrf_token() }}'
						}
						if(self.mode == 'edit') data._method = 'PATCH';
						$.ajax({
							url: '/admin/offers/{{ $offer->id or '' }}',
							method: 'POST',
							data: data,
							success: function(rsp) {
								console.log(rsp);
								window.location.href = '/admin/offers';
							}
						})
					} 
				}
			},
	    	mounted: function() {
	    		if($('#offerBy').val() == 'dp') {
					$('#addQty').hide();
					$('#prDiv label').text("Percentage");
					$('#prDiv').show();
				} else if($('#offerBy').val() == 'dpr') {
					$('#addQty').hide();
					$('#prDiv label').text("Amount");
					$('#prDiv').show();
				} else {
					$('#prDiv').hide();
					$('#addQty').show();
				}
    			var self = this;
				engine = new Bloodhound({
					datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					remote: {
						url: '/admin/products?q=%QUERY&thumb=true',
						wildcard: '%QUERY',
						transform: function(rsp) {
							return rsp.data;
						}
					}
				})
    			$("#product_name").typeahead({
					highlight: true
				},
				{
					name: 'product-name',
					display: 'name',
					source: engine
				})
				.bind('typeahead:select', function(ev, suggestion) {
					self.product.price = suggestion.price;
					self.product.id = suggestion.id;
					self.product.name = suggestion.name;
					self.product.image = suggestion.image;
					self.product.packet = suggestion.packet;
					self.product.type = suggestion.type;
					self.product.variants = suggestion.variants;
					self.product.variant = suggestion.variants[0].id;
					// self.$forceUpdate();
				});
				$('#offerBy').change(function() {
					if($('#offerBy').val() == 'dp') {
						$('#addQty').hide();
						$('#prDiv label').text("Percentage");
						$('#prDiv').show();
					} else if($('#offerBy').val() == 'dpr') {
						$('#addQty').hide();
						$('#prDiv label').text("Amount");
						$('#prDiv').show();
					} else {
						$('#prDiv').hide();
						$('#addQty').show();
					}
				});
				var date_format = 'DD/MM/YYYY';
				$('input[name="start_date"]').daterangepicker({
	        		singleDatePicker: true,
		            format: date_format
	        	}).on('apply.daterangepicker', function(ev, picker) {
				    self.from_date = picker.startDate.format('YYYY-MM-DD');
				});
	    		$('input[name="end_date"]').daterangepicker({
	        		singleDatePicker: true,
		            format: date_format,
		            drops: 'up'
	        	}).on('apply.daterangepicker', function(ev, picker) {
				    self.to_date = picker.endDate.format('YYYY-MM-DD');
				});;
    		},
	    	template: '#offerform'
	  	});

	</script>	
@endsection
