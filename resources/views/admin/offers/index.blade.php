@extends('layouts.admin-master')

@section('title', 'Offers')

@section('head')
<style>
.calendar, .ranges {
	float: left;
}
.table thead tr th {
	white-space: nowrap;
}
.table-responsive .table {
  box-shadow: none;
  margin-bottom: 0;
}
.table-responsive {
  box-shadow: 0 0 3px 1px rgba(0, 0, 0, 0.1);
  margin-bottom: 20px;
}
</style>
@endsection

@section('scripts')
	<template id="offerview">
	  	<div>
	    	<div class="toolbar">
	      		<div class="leading">
	      			<h3> Offers </h3>
	      		</div>
	      		<div class="buttons">
		        	<a class="btn btn-success" href="{{ route('offers.create') }}"><i class="fa fa-plus"></i> Add new</a>
		        	<a class="btn btn-default" data-toggle="modal" data-target="#productModal"><i class="fa fa-filter"></i> Product Filter <span class="prodlbl"></span></a>
		      	</div>
	      	</div>
	      	<div class="toolbar">
	      		<div class="leading">
	      		</div>
	      		<div class="form-control search-input">
			        <i class="fa fa-calendar"></i>
			        <input id="date" name="date_filter" type="text" placeholder="Enter date to search">
			        <button class="clear" v-on:click="clearDate()">
			          <i class="fa fa-times-circle"></i>
			        </button>
			    </div>
	      	</div>

      		<div class="table-responsive" style="">
	      	<div class="table-container">
		      	<table id="offer-table" class="table">
		        	<thead>
			          	<tr>
			            	<th>Product</th>
			            	<th>Type</th>
			            	<th>Offer Qty</th>
			            	<th>Offer Price</th>
			            	<th>Additional Qty</th>
			            	<th>Offer Text</th>
			            	<th>Valid from</th>
			            	<th>Valid to</th>
			            	<th>Store</th>
			            	<th>Actions</th>
			          	</tr>
		        	</thead>
		        	<tbody ref="table_body">
		          		<tr v-for="offer in offers">
		          			@verbatim
		          				<td>{{ offer.variant.product.name }}</td>
			            		<td>{{ offer.variant.type }}</td>
			            		<td>{{ offer.qty }}</td>
			            		<td>{{ offer.price }}</td>
			            		<td>{{ offer.add_qty }}</td>
			            		<td>{{ offer.text }}</td>
			            		<td>{{ offer.from_date }}</td>
			            		<td>{{ offer.to_date }}</td>
			            		<td v-if="offer.store != null">{{ offer.store.name }} </td><td v-else></td>
			            		<td>
			              			<a v-bind:href="'/admin/offers/' + offer.id + '/edit'" class="btn btn-xs btn-primary">Edit</a>&nbsp;<button type="button" class="btn btn-xs btn-danger" v-on:click="deleteOffer(offer)">Delete</button>
			            		</td>
		            		@endverbatim
		          		</tr>
		          		<tr v-if="!offers.length">
		          			<td colspan="10" align="center">No offers</td>
		          		</tr>
		        	</tbody>
		      	</table>
		    </div>
	      	</div>
		    <div id="productModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Select products to filter</h4>
			      </div>
			      <div class="modal-body">
			      	<label class="control-label">Products </label>
			        <select multiple id="productSelect" style="width:400px">
				        	<option v-bind:value="product.id" v-for="product in products">@{{ product.name }}</option>
		    		</select>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-success" v-on:click="reloadOffers()">OK</button>
			      </div>
			    </div>

			  </div>
			</div>
	    </div>
	</template>

<script>
	var OfferView = Vue.extend({
		data: function() {
			return {
				offers: [],
				products: []
			}
		},
		methods: {
			clearDate: function() {
				$('#date').val('');
				this.start = '';
				this.stop = '';
				this.reloadOrders();
			},
			deleteOffer: function(offer) {
				var self = this;
				if(confirm('Are you sure you want to delete this offer? This cannot be undone!')) {
					$.post('/admin/offers/' + offer.id, {
						_method: 'DELETE',
						_token: '{{ csrf_token() }}'
					}).success(function(data) {
						self.offers = self.offers.filter(function(o) {
							return o.id != offer.id;
						});
					}).error(function(err) {
						console.log(err.responseText);
					});
				}
			},
			reloadOffers: function() {
				var self = this;
				var productId = $( "#productSelect" ).val();
				var data = {
					product: productId,
					startDate: this.start,
					endDate: this.stop
				}
				$.ajax({
					url: '/admin/offers?' + $.param(data),
					method: 'GET',
					cache: false,
					success: function(response) {
						self.offers = response.offers;
						self.products = response.products;
					}
				});
				$('#productModal').modal('hide');
				$('.prodlbl').html($('#productModal option:selected').length);
			}
		},
		mounted: function() {
			vm.$on('reloadOffers', this.reloadOffers);
			this.reloadOffers();

			var self = this;
			$('#date').daterangepicker({
		      calender_style: "picker_1",
		  	  format: 'YYYY-MM-DD',
		  	  opens: 'left',
		    }, function(start, end, label) {
		      console.log(start.toISOString(), end.toISOString(), label);
		    }).on('apply.daterangepicker', function(ev, picker) {
		        self.start = picker.startDate.format('YYYY-MM-DD');
		        self.stop = picker.endDate.format('YYYY-MM-DD');
		        self.reloadOffers();
		    });

			$("#productSelect").select2({
		    	placeholder: "Select product",
		    	allowClear: true
		    });
		},
		template: '#offerview',
	});
  var child = new Vue({
	parent: vm,
	el: '#v-container',
	render: function(createElement) {
		return createElement(OfferView);
	}
});

</script>
@endsection