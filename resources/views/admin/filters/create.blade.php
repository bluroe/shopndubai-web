@extends('layouts.admin-master')

@section('title', 'Create Filter')

@section('content')
<div class="row">
	<div class="col-md-8">
    	@include('admin.partials.alerts')
		<form class="form-horizontal" method="POST" action="{{ route('filters.store') }}">
			{{ csrf_field() }}
			@include('admin.filters.partials.form')
			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
	</div>
</div>
@endsection
