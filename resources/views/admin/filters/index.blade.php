@extends('layouts.admin-master')

@section('title', 'Filters')

@section('wrapper')
<div style="position:relative;">
	<div style="position:absolute;width:100%;height:calc(100vh - 55px);background:radial-gradient(rgba(0,0,0,.2), rgba(0,0,0,.6));z-index:10;display:flex;align-items:center;justify-content:center;flex-direction: column;">
		<h1><i class="fa fa-puzzle-piece"></i> Abandoned Feature</h1>
		<p>This page does not work anymore <i class="fa fa-frown-o"></i></p>
	</div>
</div>
@endsection

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="toolbar">
			<div class="pull-right">
				<a href="{{ route('filters.create') }}" class="btn btn-success">Add new</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<table class="table table-bordered" id="filter-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@forelse($filters as $filter)
				<tr>
					<td>{{ $filter->name }}</td>
					<td>{{ $filter->type ? 'Color' : 'Text' }}</td>
					<td>
						<a class="btn btn-primary btn-sm" href="{{ route('filters.edit', $filter) }}">Edit</a>
						<button class="btn btn-danger btn-sm" onclick="deleteFilter(this, {{ $filter->id }})">Delete</button>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="3" align="center">No data</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')
<script>
	function deleteFilter(btn, id) {
		if(confirm('Are you sure you want to delete this filter? This cannot be undone!')) {
			$.post('/admin/filters/' + id, {
				_token: '{{ csrf_token() }}',
				_method: 'DELETE'
			})
			.success(function() {
				$(btn).closest('tr').remove();
				var tbody = $("#filter-table tbody");
				if(!tbody.children().length) {
					tbody.append('<tr><td colspan="3" align="center">No data</td></tr>');
				}
			})
			.error(function(rsp) {
				console.log(rsp.responseText)
			});	
		}
	}
	// var filterTable = $('#filter-table').DataTable({
	// 	ajax: {
	// 		url: '{{ route('filters.index') }}',
	// 		dataSrc: 'filters'
	// 	},
	// 	columns: [
	// 		{data:null},
	// 		{data:null},
	// 	],
	// 	colDefs: [
	// 		{
	// 			targets: 2,
	// 			render: function() {
	// 				return '<font color="red">fooz</font>';
	// 			}
	// 		}
	// 	]
	// });
</script>
@endsection
