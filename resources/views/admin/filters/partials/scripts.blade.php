@section('scripts')
<script>
	var vm = new Vue({
		el: 'body',
		data: {
			mode: 0,
			options: {!! isset($filter) ? $filter->options : old('options', '[\'\']') !!}
		},
		methods: {
			addNew: function() {
				this.options.push("");
			},
			remove: function(option) {
				this.options.splice(this.options.indexOf(option), 1);
			},
			changeMode: function(e) {
				if(e.target.value == 1 && this.options.length == 1 && this.options[0] == '') {
					this.$set('options[0]', 'black')
				}
			}
		}
	});
</script>
@endsection