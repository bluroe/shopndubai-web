<div class="form-group">
	<label class="col-md-4 control-label">Name</label>
	<div class="col-md-8">
		<input type="text" name="name" class="form-control" value="{{ $filter->name or old('name') }}" />
	</div>
</div>

<div class="form-group">
	<label class="col-md-4 control-label">Type</label>
	<div class="col-md-8">
		<select name="type" class="form-control" v-model="mode" @change="changeMode">
			<option value="0" {{ isset($filter) && $filter->type == '0' ? 'selected' : '' }}>Text</option>
			<option value="1" {{ isset($filter) && $filter->type == '1' ? 'selected' : '' }}>Color</option>
		</select>
	</div>
</div>