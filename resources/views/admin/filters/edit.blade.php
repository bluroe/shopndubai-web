@extends('layouts.admin-master')

@section('title', 'Edit Filter - ' . $filter->name)

@section('content')
<div class="row">
	<div class="col-md-8">
    	@include('admin.partials.alerts')
		<form class="form-horizontal" method="POST" action="{{ route('filters.update', $filter) }}">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PATCH" />
			@include('admin.filters.partials.form')
			<div class="col-md-offset-4 col-md-8">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
	</div>
</div>
@endsection
