@extends('layouts.admin-master')

@section('title', 'Settings')

@section('head')
<style>
	.pinrow {
		display: flex;
	}
	.pinrow + .pinrow {
		margin-top: 10px;
	}
	.pinrow button {
		margin-left: 5px;
		width: 40px;
	}
	.toolbar-body {
		padding: 15px 0;
	}
</style>
@endsection

@section('scripts')
<template id="settings">
	<div>
		<div class="toolbar">
			<div class="leading">
				<a href="#" class="head">Delivery Settings</a>
			</div>
		</div>
		<div class="toolbar-body">
			<div class="row">
				<div class="col-md-8">
					<form action="{{ url('/admin/settings/delivery') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="control-label col-md-4">Delivery Charge</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="delivery_charge" value="{{ $settings['delivery_charge'] }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Delivery Free Above</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="delivery_free_above" value="{{ $settings['delivery_free_above'] }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="toolbar">
			<div class="leading">
				<a href="#" class="head">Service Info</a>
			</div>
		</div>
		<div class="toolbar-body">
			<div class="row">
				<div class="col-md-8">
					@if(session('service_success'))
					<div class="alert alert-success">
						<p><i class="fa fa-check"></i> {{ session('service_success') }}</p>
					</div>
					@endif
					<form action="{{ url('/admin/settings/service') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="PATCH">
						<div class="form-group">
							<label class="control-label col-md-4">Contact no</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="contact_no" value="{{ $settings['contact_no'] or '' }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</template>
<script>
	var child = new Vue({
		el: '#v-container',
		template: '#settings'
	})
</script>
@endsection
