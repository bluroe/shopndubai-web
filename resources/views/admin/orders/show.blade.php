@extends('layouts.admin-master')

@section('title', 'Order #' . $order->id)

@section('content')

<div class="row">
<div class="col-md-6">
	<h3>Order Details</h3>
	<table class="table">
		<tbody>
			<tr>
				<td>Order ID</td>
				<td>{{ '#' . $order->id }}</td>
			</tr>
			<tr>
				<td>Customer ID</td>
				<td>{{ $order->customer->id or 'N/A' }}{{ $order->customer ? ' (' . $order->customer->orders->count() . ')' : '' }}</td>
			</tr>
			<tr>
				<td>Order Date</td>
				<td>{{ $order->created_at->setTimezone('Asia/Kolkata')->format('d/m/Y h:i a') }}</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>{!! '<span class="' . $order->statusClass() . '">' . $order->statusMessage() . "</span>" !!}</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="col-md-6">
	<h3>Delivery Address</h3>
	<table class="table">
		<tbody>
			<tr>
				<td>Name</td>
				<td>{{ $order->address->firstname . ' ' . $order->address->lastname }}</td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>{{ $order->address->phone }}</td>
			</tr>
			<tr>
				<td>Address</td>
				<td>{!! $order->address->format() !!}</td>
			</tr>
		</tbody>
	</table>
</div>
</div>

<div class="toolbar">
  <div class="leading">
	<div class="dropdown">
		<a class="head" href="#">Orders Items ({{ $order->orderItems->count() }})</a>
	</div>
	<button class="btn btn-default" onClick="printItems()" style="margin-left: 10px;"><i class="fa fa-print"></i> Print</button>
  </div>
</div>
<table class="table table-bordered table-condensed table-striped table-white" id="items-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Qty</th>
			<th>Store</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		@foreach($order->orderItems as $orderItem)
		<tr>
			<td>{{ $orderItem->name }}</td>
			<td>{{ $orderItem->price }}</td>
			<td>{{ $orderItem->qty }}</td>
			<td>{{ $orderItem->store->name or '' }}</td>
			<td>{{ $orderItem->qty * $orderItem->price }}</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4" align="right">Total</td>
			<td>{{ $order->getOrderTotal() }}</td>
		</tr>
		<tr>
			<td colspan="4" align="right">Delivery Charge</td>
			<td>{{ $order->delivery_charge }}</td>
		</tr>
		<tr>
			<td colspan="4" align="right">Grand Total</td>
			<td>{{ $order->total }}</td>
		</tr>
	</tfoot>
</table>

@endsection

@section('scripts')
<script>
function printItems() {
	var table = document.getElementById('items-table').outerHTML;
	var printWindow = window.open();
	printWindow.document.write([
		'<html>',
		'<head><title>Order #{{ $order->id }}</title>',
		'<link rel="stylesheet" href="/css/bootstrap.css">',
		'</head><body>',
		'<h3>Order #{{ $order->id }}</h3>',
		table,
		'</body>',
		'</html>'
	].join(''));
	printWindow.onload = function() {
		printWindow.print();
		printWindow.close();
	}
	printWindow.document.close();
}
</script>
@endsection
