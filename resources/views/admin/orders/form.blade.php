@extends('layouts.admin-master')

@section('title', isset($order) ? 'Edit Order - #' . $order->id : 'Create Order')

@section('scripts')
<template id="orderedit">
<div>
		@if(isset($order))
		<ul class="nav nav-tabs">
			<li class="active"><a href="#data" data-toggle="tab">Data</a></li>
			<li><a href="#products" data-toggle="tab">Products</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="data">
		@endif
				<div class="col-md-8">
				@include('admin.partials.alerts')
				@if(isset($order))
					<form method="POST" action="{{ route('orders.update', $order) }}" class="form-horizontal">
						<input type="hidden" name="_method" value="PATCH">
				@else
					<form method="POST" action="{{ route('orders.store') }}" class="form-horizontal">
				@endif
						{{ csrf_field() }}
						<div class="form-group">
							<label class="col-md-4 control-label">Customer</label>
							<div class="col-md-8">
								@if(isset($order))
								<label class="form-label">{{ $order->customer->key }}</label>
								@else
								{!! Form::select('customer_id', ['' => 'Select a customer'] + $customers, null, ['class' => 'form-control']) !!}
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Address</label>
							<div class="col-md-8">
								@if(isset($order))
								<div class="well">
									<p>{{ $order->address->firstname . ' ' . $order->address->lastname }}</p>
									<p>{{ $order->address->address1 }}</p>
									<p>{{ $order->address->city }}</p>
									<p>{{ $order->address->area }}</p>
									<p>{{ $order->address->pin }}</p>
									<p>{{ $order->address->phone }}</p>
									<p>{{ $order->address->email }}</p>
								</div>
								@else
								<select class="form-control" name="address_id"></select>
								@endif
								<div style="padding-top:15px;display:none;">
									<button class="btn btn-default" type="button">Create new</button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Stores</label>
							<div class="col-md-8">
								<table class="table">
									<thead>
										<tr>
											<td>Store name</td>
											<td>Delivery Status</td>
											<td>Action</td>
										</tr>
									</thead>
									<tbody>
										@verbatim
										<tr v-for="storeOrder in storeOrders">
										<td>{{ storeOrder.store.name }}</td>
										<td><span v-bind:class="statusClasses[storeOrder.status]">{{ orderStatusMessage(storeOrder.status) }}</span></td>
										<td>
											<div class="dropdown">
												<button type="button" class="btn btn-info btn-xs btn-const" data-toggle="dropdown"><i class="fa fa-cog"></i> Change Status</button>
												<ul class="dropdown-menu">
													<li><a href="#" @click="changeStatus(storeOrder, 0)"><span class="text-danger">Ordered</span></a></li>
													<li><a href="#" @click="changeStatus(storeOrder, 1)"><span class="text-warning">In transit</span></a></li>
													<li><a href="#" @click="changeStatus(storeOrder, 2)"><span class="text-success">Delivered</span></a></li>
												</ul>
											</div>
										</td>
										</tr>
										@endverbatim
										<tr v-if="storeOrders.length == 0">
											<td colspan="3" align="center">No Stores</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				</div>
		@if(isset($order))
			</div>
			<div class="tab-pane" id="products">

				<table class="table table-bordered" id="products-table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						@forelse($order->orderItems as $product)
						<tr>
							<td>{{ $product->name }}</td>
							<td>{{ $product->qty }}</td>
							<td>{{ $product->price }}</td>
							<td>{{ $product->price * $product->qty }}</td>
						</tr>
						@empty
						<tr>
							<td colspan="5" align="center">No data</td>
						</tr>
						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" align="right">Total</td>
							<td>{{ $order->total }}</td>
						</tr>
					</tfoot>
				</table>

			</div>
		</div>
		@endif
	</div>
	</template>
	<script>
		var OrderEdit = Vue.extend({
			data: function() {
				return {
					storeOrders: [],
					statusClasses: ['text-danger', 'text-warning', 'text-success']
				}
			},
			props: {
				'orderStatusMessage': {
					type: Function,
					required: true
				}
			},
			methods: {
				changeStatus: function(storeOrder, status) {
					$.post('/admin/store-order/' + storeOrder.id, {
						_token: '{{ csrf_token() }}',
						_method: 'PATCH',
						status: status
					}).success(function() {
						storeOrder.status = status;
					});
				}
			},
			mounted: function() {
				var self = this;
				@if(isset($order))
				$.ajax({
					url: '/admin/orders/{{ $order->id }}/store-orders',
					method: 'GET',
					cache: false,
					success: function(response) {
						// console.log(response)
						self.storeOrders = response;
					}
				});
				@endif
			},
			template: '#orderedit'
		});
		var child = new Vue({
			parent: vm,
			el: '#v-container',
			props: ['orderStatusMessage'],
			render: function(createElement) {
				return createElement(OrderEdit, {
					props: {
						orderStatusMessage: this.$parent.orderStatusMessage
					}
				});
			}
		});
	</script>
	<script>
	</script>
@endsection