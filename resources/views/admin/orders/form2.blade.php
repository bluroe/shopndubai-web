@extends('layouts.admin-master')

@section('title', isset($order) ? 'Edit Order - #' . $order['id'] : 'Create Order')

@section('head')
<style>
	.table {
		margin-bottom: 0;
	}
	.table tbody tr td,
	.table tfoot tr td {
		vertical-align: middle;
	}
	.thumb {
		width: 20px;
		height: 20px;
		display: inline-block;
		margin-right: 5px;
		text-align: center;
	}
	.thumb img {
		height: 100%;
	}
	.input-row.last {
		margin-bottom: 15px;
	}
	.panel {
		margin-bottom: 0;
	}
</style>
@endsection

@section('content')
<order-form></order-form>
@endsection

@section('vue-components')
	<template id="orderform">
		<div>
			<div class="title">
				<h3>{{ isset($order) ? 'Edit Order#' . $order['id'] : 'Create Order' }}</h3>
			</div>
			<div class="col-md-9">
				<form class="form-horizontal" action="{{ route('orders.store') }}" method="POST" id="order-form" data-parsley-validate>
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-md-2 control-label">Address</label>
						<div class="col-md-10">
							<div class="input-row">
								<div class="input-col">
									<label>First Name</label>
									<input type="text" class="form-control" v-model="firstname" required>
								</div>
								<div class="input-col">
									<label>Last Name</label>
									<input type="text" class="form-control" v-model="lastname" required>
								</div>
							</div>
							<div class="input-row">
								<div class="input-col">
									<label>Address Line 1</label>
									<input type="text" class="form-control" v-model="address1" required>
								</div>
							</div>
							<div class="input-row">
								<div class="input-col">
									<label>Address Line 2</label>
									<input type="text" class="form-control" v-model="address2">
								</div>
							</div>
							<div class="input-row">
								<div class="input-col">
									<label>Area</label>
									<input type="text" class="form-control" v-model="area" required>
								</div>
								<div class="input-col">
									<label>City</label>
									<input type="text" class="form-control" v-model="city" required>
								</div>
							</div>
							<div class="input-row last">
								<div class="input-col">
									<label>Phone</label>
									<input type="text" class="form-control" v-model="phone" required>
								</div>
								<div class="input-col">
									<label>PIN</label>
									<input type="text" class="form-control" v-model="pin" data-parsley-type="digits" data-parsley-length="[6,6]" data-parsley-length-message="PIN code should be exactly six digits" data-parsley-required-message="PIN code is required" required>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									Items
								</div>
								<div class="panel-body">
									<div class="form-group">
							        	<label class="control-label col-md-2">Name</label>
							        	<div class="col-md-10">
							        		<input type="text" class="form-control" id="product_name" v-model="product.name">
											<div v-if="invalidProduct">
												<ul class="parsley-errors-list filled">
													<li class="parsley-required">Invalid product</li>
												</ul>
											</div>
							        	</div>
							        </div>
							        <div id="variantDiv" class="form-group">
							        	<label class="control-label col-md-2">Type
							        	</label>
							        	<div class="col-md-10">
							        		<select class="form-control" v-model="product.variant" id="variantSelect">
							        			<option v-bind:value="variant.id" v-for="variant in product.variants">@{{ variant.type + ' - ' + variant.price }}</option>
							        		</select>
							        	</div>
							        </div>
						        	<div class="form-group">
							        	<label class="control-label col-md-2">Qty</label>
							        	<div class="col-md-5">
							        		<input type="text" class="form-control" v-model="product.qty">
							        		<div v-if="product.qty == 0">
												<ul class="parsley-errors-list filled">
													<li class="parsley-required">Invalid Qty</li>
												</ul>
											</div>
							        	</div>
							        	<div id="units" v-if="!product.packet">
								        	<label class="control-label col-md-1">Units</label>
								        	<div class="col-md-3">
								        		<select class="form-control" v-model="product.orderUnit">
								        			<option value>Select a unit</option>
								        			<option value="kg">Kilogram</option>
								        			<option value="gm">Gram</option>
								        			<option value="l">Litre</option>
								        			<option value="ml">MilliLitre</option>
								        		</select>
								        		<div v-if="product.orderUnit == ''">
												<ul class="parsley-errors-list filled">
													<li class="parsley-required">Invalid Qty</li>
												</ul>
											</div>
								        	</div>
							        	</div>
							        </div>
						            <div id="storeDiv" class="form-group">
							        	<label class="control-label col-md-2">Store
							        	</label>
							        	<div class="col-md-10">
							        		<select class="form-control" id="store_id" name="store_id" v-model="store">
							        		<option value>Select a store</option>
												<option v-for="store in order_stores" v-bind:value="store.id">
									                @{{ store.name }}
									            </option>

									        </select>
							        	</div>
							        </div>
							        <div class="form-group">
							        	<div class="col-md-12">
							        		<button type="button" class="btn btn-primary" v-on:click="addProduct()" v-bind:disable="!addReady" style="float:right;">Add</button>
							        	</div>
							        </div>
							    </div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Items</label>
						<div class="col-md-10">
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Type</th>
										<th>Qty</th>
										<th>Price</th>
										<th>Store</th>
										<th>Total</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(orderItem, index) in orderItems">
										<td>
											<span class="thumb"><img v-bind:src="orderItem.image || '/img/no-image.png'"></span>
											@{{ orderItem.name }}
										</td>
										<td><span>@{{ orderItem.setType }}</span></td>
										<td><span>@{{ orderItem.qty }}</span></td>
										<td>@{{ orderItem.price }}</td>
										<td>@{{ orderItem.store }}</td>
										<td>@{{ calculatePrice(orderItem.price, orderItem.qty, orderItem.orderUnit, orderItem.type) }}</td>
										<th><button class="btn btn-danger btn-sm" tabindex="-1" type="button" v-on:click="removeItem(index, orderItem)"><i class="fa fa-close"></i></button></th>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td align="right">Delivery Charge</td>
										<td><input type="number" v-model="delivery_charge" style="width:42px;" /></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td align="right">Total</td>
										<td>@{{ total }}</td>
										<td></td>
									</tr>
								</tfoot>
							</table>
							<div v-if="itemsError">
								<ul class="parsley-errors-list filled">
									<li class="parsley-required">Add atleast one item</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-success" v-on:click="submit($event)">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</template>
	<script>
		var stub_product = {
			name:'',
			qty:0,
			price:0,
			variants:[],
			packet:false,
			orderUnit:'',
			variant: ''
		};
		var engine;
		Vue.component('order-form', {
			data: function() {
				return {
					firstname: '{{ isset($order) ? array_get($order, 'address.firstname') : '' }}',
					lastname: '{{ isset($order) ? array_get($order, 'address.lastname') : '' }}',
					address1: '{{ isset($order) ? array_get($order, 'address.address1') : '' }}',
					address2: '{{ isset($order) ? array_get($order, 'address.address2 ') : '' }}',
					area: '{{ isset($order) ? array_get($order, 'address.area') : '' }}',
					city: '{{ isset($order) ? array_get($order, 'address.city') : '' }}',
					phone: '{{ isset($order) ? array_get($order, 'address.phone') : '' }}',
					pin: '{{ isset($order) ? array_get($order, 'address.pin') : '' }}',
					orderItems: {!! json_encode(isset($order) ? array_get($order, 'order_items') : []) !!},
					product: stub_product,
					itemsError: false,
					invalidProduct: false,
					order: {{ $order['id'] or 'null' }},
					order_stores: {!! json_encode($stores) !!},
					delivery_charge: {{ $order['delivery_charge'] or 0 }},
					store: '',
					subTotal: ''
				}
			},
			methods: {
				calculatePrice: function(price, qty, orderUnit, productUnit) {
					if(!orderUnit) {
						orderUnit = "p";
						productUnit= "p";
					}
					var measures = {
						'p': 1,
						'kg': 1000,
						'gm': 1,
						'l': 1000,
						'ml': 1
					};
					return (price / measures[productUnit] * qty) * measures[orderUnit];
				},
				showModal: function() {
					$("#product-modal").modal('show');
				},
				addProduct: function() {
					var product = this.product;
					var curr_store = this.store;
					var prod_store = this.order_stores.filter(function(s) {
						return s.id == curr_store;
					})[0];
					var variant = product.variants.filter(function(p) {
						return p.id == product.variant;
					})[0];
					if(product.id) {
						this.invalidProduct = false;
						var item = {
							id: variant.id,
							name: product.name,
							qty: product.qty,
							image: product.image,
							price: variant.price,
							type: variant.type,
							setType: variant.type,
							orderUnit: product.orderUnit,
							setUnit: product.orderUnit,
							store: prod_store.name,
							store_id: prod_store.id
						};
						// if(prod_store) {
						// 	item.store = prod_store.name;
						// 	item.store_id = prod_store.id;
						// }
						this.orderItems.push(item);
						this.itemsError = false;
						this.product = {
							name:'',
							qty:0,
							price:0,
							variants:[],
							packet:false,
							orderUnit:'',
							variant: ''
						}
					} else {
						this.invalidProduct = true;
					}
				},
				removeItem: function(index) {
					this.orderItems = this.orderItems.slice(0, index).concat(this.orderItems.slice(index + 1));
				},
				submit: function(e) {
					e.preventDefault();
					var form = $("#order-form").parsley();
					var subTotal = this.total;
					form.validate();
					if(form.isValid()) {
						if(this.orderItems.length) {
							var self = this;
							var data = {
								firstname: this.firstname,
								lastname: this.lastname,
								address1: this.address1,
								address2: this.address2,
								area: this.area,
								city: this.city,
								phone: this.phone,
								pin: this.pin,
								total: subTotal,
								order_items: this.orderItems.map(function(item) {
									return {id: item.id, qty: item.qty, price: item.price, store_id: item.store_id};
								}),
								delivery_charge: this.delivery_charge,
								_token: '{{ csrf_token() }}'
							}
							if(this.order) data._method = 'PATCH';
							$.ajax({
								url: '/admin/orders/{{ $order['id'] or '' }}',
								method: 'POST',
								data: data,
								success: function(rsp) {
									console.log(rsp);
									window.location.href = '/admin/orders';
								}
							})
						} else {
							this.itemsError = true;
						}
					} else {
						// alert('Please add atleast 1 item');
					}
				}
			},
			computed: {
				total: function() {
					return this.orderItems.reduce(function(carry, orderItem) {
						if(!orderItem.orderUnit) {
							orderItem.orderUnit = "p";
							orderItem.type= "p";
						}
						var measures = {
							'p': 1,
							'kg': 1000,
							'gm': 1,
							'l': 1000,
							'ml': 1
						};
						return carry + ((orderItem.price / measures[orderItem.type] * orderItem.qty) * measures[orderItem.orderUnit]);
						// if(orderItem.orderUnit){
						// 	console.log("hiiiii");
						// } else {
							// return carry + orderItem.qty * orderItem.price;
						// }
					}, this.delivery_charge || 0);
				},
				addReady: function() {
					return this.product.id && this.product.qty <= this.product.max_qty;
				}
			},
			mounted: function() {
				var self = this;
				engine = new Bloodhound({
					datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					remote: {
						url: '/admin/products?q=%QUERY&thumb=true',
						wildcard: '%QUERY',
						transform: function(rsp) {
							return rsp.data;
						}
					}
				})
				$("#product_name").typeahead({
					highlight: true
				},
				{
					name: 'product-name',
					display: 'name',
					source: engine,
					limit: 10
				})
				.bind('typeahead:select', function(ev, suggestion) {
					self.product.price = suggestion.price;
					self.product.id = suggestion.id;
					self.product.name = suggestion.name;
					self.product.image = suggestion.image;
					self.product.packet = suggestion.packet;
					self.product.type = suggestion.type;
					self.product.variants = suggestion.variants;
					self.product.variant = suggestion.variants[0].id;
					// self.$forceUpdate();
				});
			},
			template: '#orderform'
		})
	</script>
@endsection