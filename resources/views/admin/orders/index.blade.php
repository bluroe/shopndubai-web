@extends('layouts.admin-master')

@section('title', 'Orders')

@section('head')
<style>
	.table .dropdown {
		display: inline-block;
	}
	.daterangepicker .calendar {
		float: left;
	}
	.daterangepicker .ranges {
		float: left;
	}
	.loading {
		position: fixed;
		bottom: -80px;
		right: 20px;
		z-index: 1;
		transition: .2s bottom ease-out;
		display: inline-block;
		width: 200px;
	}
	.loading.shown {
		bottom: 20px;
	}
	.loading .box {
		border-radius: 4px;
		box-shadow: 0 0 10px 1px rgba(0,0,0,.3);
		background-color: #fff;
		padding: 10px;
		display: flex;
	}
	.loading p {
		margin: 0;
	}
	.loading .spinner {
		padding-right: 10px;
	}
	.loading .spinner img {
		width: 40px;
	}
	.loading .head {
		font-size: 14px;
		margin-bottom: 5px;
		color: #555;
	}
	.loading .text {
		color: #999;
		font-size: 13px;
	}
	.buttons select {
		margin-right: 5px;
		width: 70px;
	}
	.select2-selection--multple {
		border-color: '#ccc';
	}
</style>
@endsection

@section('content')
<orders-view></orders-view>
@endsection

@section('vue-components')

<template id="order-row">
	@verbatim
	<tr>
		<td class="a-center ">
	      <input type="checkbox" name="table_records" value="1" v-model="order.checked">
	    </td>
		<td>#{{ order.id }}</td>
		<td><span title="Address" ref="popover" data-html="true" :data-content="addressFormat(order.address)" data-placement="bottom">{{ order.address.area }}</span></td>
		<td>{{ order.total }}</td>
		<td>{{ order.created_at }}</td>
		<td>
			<span v-bind:class="orderStatusClass(order)">{{ orderStatusText(order) }}</span>
		<td>
			<a :href="'/admin/orders/' + order.id" class="btn btn-xs btn-warning">View</a>
			<a :href="'/admin/orders/' + order.id + '/edit'" class="btn btn-xs btn-primary">Edit</a>
			<div class="dropdown">
				<a href="#" data-toggle="dropdown" class="btn btn-xs btn-info">Change Status <i class="caret"></i></a>
				<ul class="dropdown-menu">
					<li><a href="#"><span class="text-warning" @click="changeStatus(0)">Ordered</span></a></li>
					<li><a href="#"><span class="text-primary" @click="changeStatus(1)">In Transit</span></a></li>
					<li><a href="#"><span class="text-success" @click="changeStatus(2)">Delivered</span></a></li>
				</ul>
			</div>
			<div class="dropdown">
				<a href="#" data-toggle="dropdown" class="btn btn-xs btn-danger"><i class="fa fa-cog"></i> <i class="caret"></i></a>
				<ul class="dropdown-menu pull-right">
					<li><a href="#" @click="changeStore()">Change Store</a></li>
					<li><a href="#" @click="cancelOrder()">Cancel Order</a></li>
					<li><a href="#" @click="deleteOrder()"><span class="text-danger">Delete</span></a></li>
				</ul>
			</div>
		</td>
	</tr>
	@endverbatim
</template>

<template id="orderview">
	<div>
		<div v-bind:class="['loading', printing ? 'shown' : '']">
			<div class="box">
				<div class="spinner">
					<img src="/img/loading.svg" />
				</div>
				<div class="text">
					<p class="head">Loading</p>
					<p>Please wait</p>
				</div>
			</div>
		</div>
		<div class="toolbar">
		  <div class="leading">
			<div class="dropdown">
				<a class="head" href="#" data-toggle="dropdown">Orders <i class="caret"></i></a>
				<ul class="dropdown-menu">
                    <li class="dropdown-header">Type</li>
					<li><a href="#" v-on:click="changeType(null)">All<i class="fa fa-check" v-if="type == null"></i></a></li>
					<li><a href="#" v-on:click="changeType(4)">Pending<i class="fa fa-check" v-if="type == 4"></i></a></li>
					<li><a href="#" v-on:click="changeType(0)">Ordered<i class="fa fa-check" v-if="type == 0"></i></a></li>
					<li><a href="#" v-on:click="changeType(1)">In Transit<i class="fa fa-check" v-if="type == 1"></i></a></li>
					<li><a href="#" v-on:click="changeType(2)">Delivered<i class="fa fa-check" v-if="type == 2"></i></a></li>
					<li><a href="#" v-on:click="changeType(3)">Cancelled<i class="fa fa-check" v-if="type == 3"></i></a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-header">Order by</li>
					<li><a href="#" v-on:click="orderBy('latest')">Latest<i class="fa fa-check" v-if="orderby == 'latest'"></i></a></li>
					<li><a href="#" v-on:click="orderBy('oldest')">Oldest<i class="fa fa-check" v-if="orderby == 'oldest'"></i></a></li>
				</ul>
			</div>
			<button class="btn btn-default btn-xs" v-on:click="reloadOrders()" style="margin-left: 10px;"><i v-bind:class="['fa fa-refresh', loading ? 'fa-spin' : '']"></i></button>
		  </div>
	      <div class="form-control search-input">
	        <i class="fa fa-search"></i>
	        <input type="text" v-model="search" v-on:keyup="filterOrders" placeholder="Search #">
	        <button v-on:click="clearSearch()" class="clear" v-show="search">
	          <i class="fa fa-times-circle"></i>
	        </button>
	      </div>
		  <div class="buttons">
		    <a class="btn btn-success" href="{{ route('orders.create') }}"><i class="fa fa-plus"></i> Add new</a>
		  </div>
		</div>
		<div class="toolbar">
			<div class="leading">
				<div class="buttons">
					<select v-model="limit" class="form-control">
						<option>10</option>
						<option>25</option>
						<option>50</option>
						<option>100</option>
					</select>
			    	<button class="btn btn-default" v-on:click="print()">
		          		<i class="fa fa-print"></i> Print
		        	</button>
			  	</div>
			</div>
		  	<div class="form-control search-input">
		        <i class="fa fa-calendar"></i>
		        <input name="date_filter" type="text" placeholder="Enter date to search" ref="date">
		        <button class="clear" v-on:click="clearDate()" v-show="start || stop">
		          <i class="fa fa-times-circle"></i>
		        </button>
		    </div>
			<div class="buttons">
			    <a class="btn btn-default" v-on:click="$emit('openProductFilter')"><i class="fa fa-filter"></i> Product Filter <span class="prodlbl" v-if="products.length">@{{ products.length }}</span></a>
			 </div>
		</div>
	<div class="table-container">
		<table class="table">
			<thead>
				<tr>
					<th>
                        <input type="checkbox" v-bind:checked="orders.length && checked == orders.length" v-on:change="checkAll">
                    </th>
					<th class="column-title" v-if="!checked">ID</th>
					<th class="column-title" v-if="!checked">Area</th>
					<th class="column-title" v-if="!checked">Total</th>
					<th class="column-title" v-if="!checked">Order date</th>
					<th class="column-title" v-if="!checked">Status</th>
					<th class="column-title" v-if="!checked">Action</th>
					<th class="bulk-actions" colspan="7" v-if="checked">
		                <span>Bulk Actions ( <span>@{{ checked }} rows selected</span> ) <button class="btn btn-xs btn-danger" v-on:click="deleteRows()"><i class="fa fa-trash"></i> Delete</button></span>
		            </th>
				</tr>
			</thead>
			<tbody ref="table_body">
				<tr is="order-row" v-for="order in orders" v-bind:order="order" v-bind:remove-row="removeRow"></tr>
				<tr v-if="orders.length == 0">
					<td colspan="7" align="center">@{{ loading ? 'Loading...' : (search ? 'No results' : 'No orders') }}</td>
				</tr>
			</tbody>
		</table>
	</div><!-- table-container -->

      <div class="table-foot">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous" v-on:click="prevPage()">
              <i class="fa fa-chevron-left"></i>
            </a>
          </li>
          <li>
            <a href="#" aria-label="Next" v-on:click="nextPage()">
              <i class="fa fa-chevron-right"></i>
            </a>
          </li>
        </ul>
        <label>Page</label>
        <input class="form-control" v-model="page" type="number">
        <label>of @{{ Math.ceil(total / limit) }}</label>
      </div>

	<filter-modal></filter-modal>
	<store-modal></store-modal>
</template>

<template id="filter-modal">
	<div class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" v-on:click="show = false">&times;</button>
	        <h4 class="modal-title">Select products to filter</h4>
	      </div>
	      <div class="modal-body">
	        <select multiple ref="select">
	        	<option v-bind:value="product.id" v-for="product in products">@{{ product.name }}</option>
    		</select>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" v-on:click="show = false">Cancel</button>
	        <button type="button" class="btn btn-success" v-on:click="filterOrders()">Apply</button>
	      </div>
	    </div>

	  </div>
	</div>
</template>

<template id="store-modal">
	<div class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" v-on:click="show = false">&times;</button>
	        <h4 class="modal-title">Enter Store details</h4>
	      </div>
	      <div class="modal-body">
	      	<div style="display:flex;margin-bottom:10px;">
		      	<select class="form-control" v-model="store" >
	        		<option value>Select a store</option>
					<option v-for="s in stores" v-bind:value="s.id">
		                @{{ s.name }}
		            </option>

		        </select>
		        <button type="button" class="btn btn-default" style="margin-left:5px;" v-on:click="setStores()">Add</button>
	        </div>
	      	<table class="table">
	      		<thead>
		      		<th>
	                	<input type="checkbox" v-bind:checked="checked == order_items.length" v-on:change="checkAll">
	              	</th>
	      			<th>Product</th>
	      			<th>Store</th>
	      		</thead>
	      		<tbody>
	      			<tr v-for="item in order_items">
	      				<td class="a-center ">
	                      <input type="checkbox" v-model="item.checked">
	                    </td>
	      				<td>@{{ item.name }}</td>
	      				<td>@{{ item.store ? item.store.name : '' }}</td>
	      			</tr>
	      		</tbody>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" v-on:click="show = false">Cancel</button>
	        <button type="button" class="btn btn-info" v-on:click="skip()" v-if="callback">Skip</button>
	        <button type="button" class="btn btn-success" v-on:click="saveStores()">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
</template>
<script>
	var eventHub = new Vue();
	Vue.component('filter-modal', {
		data: function() {
			return {
				products: [],
				show: false
			}
		},
		watch: {
			show: function(val) {
				$(this.$el).modal(val ? 'show' : 'hide');
			}
		},
		methods: {
			filterOrders: function() {
				var products = $(this.$refs.select).val() || [];
				var params = getParams();
				delete params['products[]'];
				params.products = products;
		        history.replaceState({}, 'Orders', location.pathname + '?' + $.param(params));
				this.$parent.$emit('filterProducts', products);
				this.show = false;
			}
		},
		mounted: function() {
			var self = this;
			$.ajax({
				url: '/admin/products?fetch_all=true',
				method: 'GET',
				cache: false,
				success: function(response) {
					console.log(response);
					self.products = response.data;
				}
			});
		    $(this.$refs.select).select2({
		    	placeholder: "Search products",
		    	allowClear: true,
		    	width: '100%'
		    });
		    this.$parent.$on('openProductFilter', function() {
		    	self.show = true;
		    });
		},
		template: '#filter-modal'
	});
	Vue.component('store-modal', {
		data: function() {
			return {
				store: '',
				order_items: [],
				stores: [],
				show: false,
				callback: null
			}
		},
		computed: {
			checked: function() {
				return this.order_items.filter(function(oi) {
					return oi.checked;
				}).length;
			}
		},
		methods: {
			skip: function() {
				this.show = false;
				if(callback) callback();
			},
			checkAll: function() {
				var check = this.checked != this.order_items.length;
				this.order_items.forEach(function(oi) {
					oi.checked = check;
				});
			},
			openOrder: function(order, callback) {
				this.order_items = order.order_items.map(function(oi) {
					return {
						checked: false,
						id: oi.id,
						name: oi.name,
						store: oi.store
					}
				});
				this.show = true;
				this.callback = callback;
			},
			saveStores: function() {
				var self = this;
				var stores = {};
				this.order_items.forEach(function(oi) {
					if(oi.store) stores[oi.id] = oi.store.id;
				});
				if(Object.keys(stores).length === 0) {
					alert('Nothing to save');
					return;
				}
				var data = {
					orderStore: stores,
					_token: '{{ csrf_token() }}'
				}
				$.ajax({
					url: '/admin/orders/savestore?' + $.param(data),
					method: 'POST',
					cache: false,
					success: function(response) {
						console.log(response);
						self.show = false;
						self.order = null;
						if(self.callback) self.callback();
					}
				});
			},
			setStores: function() {
				var self = this;
				this.order_items.filter(function(oi) {
					return oi.checked;
				}).forEach(function(oi) {
					var store = self.stores.filter(function(s) {
						return s.id == self.store;
					})[0];
					oi.store = store;
				});
			},
		},
		watch: {
			show: function(val) {
				$(this.$el).modal(val ? 'show' : 'hide');
			}
		},
		mounted: function() {
			eventHub.$on('openStoreModal', this.openOrder);
			var self = this;
			$.ajax({
				url: '/admin/stores?active=true',
				method: 'GET',
				cache: false,
				success: function(response) {
					console.log(response);
					self.stores = response.stores;
				}
			});
		},
		template: '#store-modal'
	});
	Vue.component('order-row', {
		props: {
			order: {
				type: Object,
				required: true
			}
		},
		mounted: function() {
			$(this.$refs.popover).popover({
				content: this.addressFormat(this.order.address),
				html: true
			});
		},
		beforeUpdate: function() {
			$(this.$refs.popover).popover('hide');
		},
		beforeDestroy: function() {
			$(this.$refs.popover).popover('destroy');
		},
		methods: {
			addressFormat: function(address) {
				return [
					address.firstname + ' ' + address.lastname,
					address.address1 + (address.address2 ? '<br>' + address.address2 : ''),
					address.phone,
					address.city + ', ' + address.area,
					address.pin
				].join('<br>');
			},
			deleteOrder: function() {
				var self = this;
				var order = this.order;
				// console.log(order.id)
				if(confirm('Are you sure you want to delete this order? This cannot be undone!')) {
					$.post('/admin/orders/' + order.id, {
						_method: 'DELETE',
						_token: '{{ csrf_token() }}'
					}).success(function(data) {
						self.$parent.$emit('removeRow', order.id);
					}).error(function(err) {
						console.log(err.responseText);
					});
				}
			},
			cancelOrder: function() {
				var order = this.order;
				if(confirm('Are you sure you want to cancel this order? This cannot be undone!')) {
					$.post('/admin/orders/' + order.id + '/cancel-order', {
						_method: 'PATCH',
						_token: '{{ csrf_token() }}'
					}).success(function(data) {
						order.status = 3;
						console.log(data);
					}).error(function(err) {
						console.log(err.responseText);
					});
				}
			},
			orderStatusClass: function(order) {
				switch(order.status) {
					case 0:
						return 'label label-warning';
					case 1:
						return 'text-primary';
					case 2:
						return 'text-success';
					case 3:
						return 'text-default';
					case 4:
						return 'text-danger';
					default:
						return '';
				}
			},
			orderStatusText: function(order) {
				switch(order.status) {
					case 0:
						return 'Ordered';
					case 1:
						return 'In transit';
					case 2:
						return 'Delivered';
					case 3:
						return 'Cancelled';
					case 4:
						return 'Pending';
					default:
						return 'Error';
				}
			},
			changeStatus: function(status) {
				// current status is pending & new status is ordered
				if(status == 0 && this.order.status == 4) {
					// find out products with unassigned stores
					var items = this.order.order_items.filter(function(oi) {
						return !oi.store_id;
					});
					if(items.length) {
						// open modal for assigning stores
						return this.changeStore(function() {
							this.statusUpdate(status);
						}.bind(this));
					}
				}
				var confirmed = confirm('Are you sure you want to change the status?');
				if(!confirmed) return;
				this.statusUpdate(status);	
			},
			changeStore: function(callback) {
				eventHub.$emit('openStoreModal', this.order, callback);
			},
			statusUpdate: function(status) {
				var order = this.order;
				$.post('/admin/order/' + order.id, {
					_token: '{{ csrf_token() }}',
					_method: 'PATCH',
					status: status
				}).success(function(rsp) {
					console.log(rsp);
					order.status = status
				});
			},
		},
		template: '#order-row'
	});
  	var urlParams = new URLSearchParams(window.location.search);
  	var page = urlParams.get('page');
  	var start = urlParams.has('start_date') ? urlParams.get('start_date') : '';
  	var stop = urlParams.has('end_date') ? urlParams.get('end_date') : '';
  	var products = urlParams.has('products[]') ? urlParams.getAll('products[]') : [];
	Vue.component('orders-view', {
		data: function() {
			return {
				loading: false,
				orders: [],
				type: null,
				orderby: 'latest',
				search: '',
				products: products,
				start: start,
				stop: stop,
				stores: [],
				limit: 10,
				page: page ? parseInt(page) : 1,
				total: 0,
				printing: false
			}
		},
	    watch: {
	    	page: function(page) {
	    		var params = getParams();
	    		params.page = page;
	    		window.history.replaceState({}, 'Orders', '/admin/orders?' + $.param(params))
	    		this.reloadOrders();
	    	},
	    	limit: function() {
	    		this.reloadOrders();
	    	},
			loading: function(val, oldVal) {
				var table_body = this.$refs.table_body
				if(val) {
				    var height = table_body.getBoundingClientRect().height;
				    var lo = document.createElement('div');
				    lo.className = 'lo';
				    lo.style.height = height + 'px';
				    table_body.appendChild(lo);
				    this.$refs.lo = lo;
					} else {
					table_body.removeChild(this.$refs.lo);
				}
			}
	  	},
		computed: {
			checked: function() {
				return this.orders.filter(function(v) {
					return v.checked;
				}).length;
			}
		},
		methods: {
			removeRow: function(id) {
				this.orders = this.orders.filter(function(o) {
					return o.id != id;
				});
			},
			prevPage: function() {
				if(this.page > 1) {
					this.page = this.page - 1;
				}
			},
			nextPage: function() {
				var lastPage = Math.ceil(this.total / this.limit);
				if(this.page < lastPage) {
					this.page = this.page + 1;
				}
			},
			checkAll: function() {
				var check = true;
				if(this.checked == this.orders.length) check = false;
				for(i in this.orders) {
					this.orders[i].checked = check;
				}
			},
			deleteRows: function() {
				var self = this;
				if(confirm('Are you sure you want to delete this order(s)? This cannot be undone!')) {
					var ids = this.orders.filter(function(o) {
						return o.checked;
					}).map(function(o) {
						return o.id;
					});
					$.post('/admin/orders/' + ids.join(','), {
						_method: 'DELETE',
						_token: '{{ csrf_token() }}'
					}).success(function(data) {
						self.orders = self.orders.filter(function(o) {
							return ids.indexOf(o.id) == -1;
						});
					}).error(function(err) {
						console.log(err.responseText);
					});
					
				}
			},
			clearSearch: function() {
				this.search = '';
				this.reloadOrders();
			},
			clearDate: function() {
				$(this.$refs.date).val('');
				this.start = '';
				this.stop = '';
				this.reloadOrders();
			},
			filterOrders: _.debounce(function() {
				this.reloadOrders();
			}, 500),
			orderBy: function(order) {
				this.orderby = order;
				this.reloadOrders();
			},
			changeType: function(type) {
				this.type = type;
				this.reloadOrders();
			},
			reloadOrders: function() {
				var self = this;
				this.loading = true;
				var offset = (this.page - 1) * this.limit;
				var data = {
					orderby: this.orderby,
					type: this.type,
					q: this.search,
					product: this.products,
					startDate: this.start,
					endDate: this.stop,
					offset: offset,
					limit: this.limit
				}
				if(this.xhr) this.xhr.abort();
				this.xhr = $.ajax({
					url: '/admin/orders?' + $.param(data),
					method: 'GET',
					cache: false,
					success: function(response) {
						var orders = response.orders;
						orders.forEach(function(order) {
							order.checked = 0;
							order.created_at = moment.utc(order.created_at)
								.zone('+05:30')
								.format('DD/MM/YYYY h:mm:ss');
						});
						self.loading = false;
						self.orders = orders;
						self.total = response.length;
					}
				});
			},
			print: function() {
				var self = this;
				this.printing = true;
				var data = {
					orderby: this.orderby,
					type: this.type,
					q: this.search,
					product: this.products,
					startDate: this.start,
					endDate: this.stop
				}
				$.ajax({
					url: '/admin/orders?' + $.param(data),
					method: 'GET',
					cache: false,
					success: function(response) {
						self.orders = response.orders;
						self.products = response.products;
						var rows = [];
						if(response.orders.length != 0) {
							for(var i in response.orders) {
								var row = response.orders[i];
								rows.push([
									'<tr>',
										'<td> #' +row.id + '</td>',
										'<td>' + row.address.area + '</td>',
										'<td>' + row.total + '</td>',
										'<td>' + row.created_at + '</td>',
										'<td>' + self.orderStatusText(row) + '</td>',
									'</tr>'
								].join(''));
							}
							var table = [
								'<div>',
								'<table class="table">',
									'<thead>',
										'<th> Order ID </th>',
										'<th> Area </th>',
										'<th> Total </th>',
										'<th> Order Date </th>',
										'<th> Status </th>', 
									'</thead>',
									'<tbody>',
										rows.join('') ,
									'</tbody>',
								'</table>',
								'</div>'
							].join('');
							var printWindow = window.open();
							printWindow.document.write([
								'<html>',
								'<head><title>Order Report</title>',
								'<link rel="stylesheet" href="/css/bootstrap.css">',
								'</head><body>',
								'<h3> Orders </h3>',
								table,
								'</body>',
								'</html>'
							].join(''));
							printWindow.onload = function() {
					    		printWindow.print();
					    		printWindow.close();
							}
							printWindow.document.close();
						} else {
							alert('No Records to print');
						}
						self.printing = false;
					}
				});
			},
		},
		mounted: function() {
			this.reloadOrders();
			this.$parent.$on('reloadOrders', this.reloadOrders);
			this.$on('filterProducts', function(products) {
				this.products = products;
				this.reloadOrders();
			});
			this.$on('removeRow', this.removeRow);
			var self = this;
			$(this.$refs.date).daterangepicker({
		      calender_style: "picker_1",
		  	  format: 'YYYY-MM-DD',
		  	  opens: 'left',
		    }, function(start, end, label) {
		      console.log(start.toISOString(), end.toISOString(), label);
		    }).on('apply.daterangepicker', function(ev, picker) {
		        self.start = picker.startDate.format('YYYY-MM-DD');
		        self.stop = picker.endDate.format('YYYY-MM-DD');
		        self.reloadOrders();
		        var params = getParams();
		        params.start_date = self.start;
		        params.end_date = self.stop;
		        history.replaceState({}, 'Orders', location.pathname + '?' + $.param(params));
		    });
		},
		template: '#orderview',
	});
</script>

@endsection