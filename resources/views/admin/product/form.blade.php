@extends('layouts.admin-master')

@section('title', isset($product) ? 'Edit Product - ' . $product->name : 'Create Product')

@section('head')
<style>
  .table tbody tr td {
    vertical-align: middle;
  }
  /*.table input {
    max-width: 100px;
  }*/
</style>
@endsection

@section('scripts')
<template id="productform">
<div class="row">
  <div class="col-md-12">
    @include('admin.partials.alerts')
    @if(isset($product))
    <form method="POST" action="{{ route('products.update', $product) }}" class="form-horizontal">
      <input type="hidden" name="_method" value="PATCH">
    @else
    <form method="POST" action="{{ route('products.store') }}" class="form-horizontal">
    @endif
      {{ csrf_field() }}

      <div class="toolbar">
        <div class="buttons">
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
          <a type="button" class="btn" href="{{ route('products.index', ['q' => Request::get('q')]) }}">{{ Request::path() == 'admin/products' && Request::get('store') ? 'All ' : '' }}Cancel<span class="btn btn-close"></span></a>
        </div>
        <div class="clearfix"></div>
      </div>

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#data" data-toggle="tab">Product Details</a>
        </li>
        <li role="presentation">
          <a href="#images" data-toggle="tab">Images</a>
        </li>
        <li role="presentation">
          <a href="#filters" data-toggle="tab">Filters</a>
        </li>
      </ul>

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="data">

          <div class="col-lg-10 col-md-10">
            <div class="form-group">
              <label class="col-md-4 control-label">Name <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="name" value="{{ $product->name or old('name') }}" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Description</label>
              <div class="col-md-8">
                <textarea class="form-control" name="description" rows="5">{{ $product->description or old('description') }}</textarea>
              </div>
            </div>
            <div class="hide">
              <div class="form-group">
                <label class="col-md-4 control-label">Min Qty</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="min_qty" value="{{ $product->min_qty or old('min_qty') }}" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Max Qty</label>
                <div class="col-md-8">
                  <input type="number" class="form-control" name="max_qty" value="{{ $product->max_qty or old('max_qty') }}" />
                </div>
              </div>
            </div>
            <div class="hide">
              <div class="form-group">
                <label class="col-md-4 control-label">Qty <span class="text-danger">*</span></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="qty" value="{{ $product->qty or old('qty') }}" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Price <span class="text-danger">*</span></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="purchase_price" value="{{ $product->purchase_price or old('purchase_price') }}" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Sale Price <span class="text-danger">*</span></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="price" value="{{ $product->price or old('price') }}" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Offer Price</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="offer_price" value="{{ $product->offer_price or old('offer_price') }}" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">MRP</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="mrp" value="{{ $product->mrp or old('mrp') }}" />
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Category <span class="text-danger">*</span></label>
              <div class="col-md-8">
                {!! Form::select('category_id', ['' => 'Select a category'] + $categories, isset($product) ? $product->category_id : old('category_id'), ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Min Qty</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="min_qty" value="{{ $product->min_qty or old('min_qty') }}" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Tags </label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="tags" value="{{ $product->tags or old('tags') }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Default Offer </label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="offer" value="{{ $product->offer or old('offer') }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Active</label>
              <div class="col-md-8 checkbox">
                <label>
                  <input type="hidden" name="active" value="0">
                  {!! Form::checkbox('active', 1, isset($product) ? $product->active : true) !!}
                </label>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Packet</label>
              <div class="col-md-8 checkbox">
                <label>
                  <input type="checkbox" name="packet" v-model="packet">
                </label>
              </div>
            </div>
            <div class="form-group" id="units">
              <div class="col-md-12">
                  <h3>Units</h3>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Buy Price</th>
                        <th>Price</th>
                        <th>MRP</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="row, key in rows">
                        <td>
                          <input type="hidden" v-model="row.id" v-bind:name="'variant[' + key + '][id]'">
                          <input type="text" class="form-control" placeholder="eg. 1KG" v-model="row.type" v-bind:name="'variant[' + key + '][type]'" v-if="packet" required>
                          <select class="form-control" v-if="!packet" v-model="row.type" v-bind:name="'variant[' + key + '][type]'">
                            <option value="">Select a unit</option>
                            <option value="kg">Kilogram</option>
                            <option value="gm">Gram</option>
                            <option value="l">Litre</option>
                            <option value="ml">MilliLitre</option>
                          </select>
                        </td>
                        <td><input type="text" class="form-control" placeholder="Buy Price" v-model="row.purchase_price" v-bind:name="'variant[' + key + '][purchase_price]'" required></td>
                        <td><input type="text" class="form-control" placeholder="Unit Price" v-model="row.price" v-bind:name="'variant[' + key + '][price]'" required></td>
                        <td><input type="text" class="form-control" placeholder="MRP" v-model="row.mrp" v-bind:name="'variant[' + key + '][mrp]'" required></td>
                        <td><a type="button" class="btn btn-danger btn-sm" v-on:click="removeRow(key)"><i class="fa fa-close"></i></a></td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="5">
                          <button type="button" class="btn btn-success btn-sm" v-on:click="addRow()"><i class="fa fa-plus"></i></button>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
              </div>
            </div>
          </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="images">
          <div class="col-lg-8 col-md-10">
            <table class="table table-bordered" id="images-table">
              <thead>
                <tr>
                  <td>Image</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <tr v-for="(image, index) in images">
                  <td><button type="button" class="img-btn" v-on:click="showModal(image)"><input type="hidden" name="images[]" v-model="image.id" /><img v-bind:src="image.filename ? image.filename : '/img/no-image.png'" /></button></td>
                  <td><button type="button" class="btn btn-danger" v-on:click="removeImage(index)"><i class="fa fa-close"></i></button></td>
                </tr>
                <tr>
                  <td colspan="2"><button class="btn btn-primary" type="button" v-on:click="showModal()"><i class="fa fa-plus"></i></button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="filters">
          <div class="col-lg-8 col-md-10">
            <table class="table table-bordered" id="filters-table">
              <thead>
                <tr>
                  <td>Filter</td>
                  <td>Value</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                @if(isset($product))
                @foreach($product->filters as $filter)
                @foreach(json_decode($filter->pivot->options) as $option)
                <tr>
                  <td><input type="text" class="form-control" value="{{ $filter->name }}" /></td>
                  <td><input type="text" class="form-control" name="{{ 'filters[' . $filter->id . '][]' }}" value="{{ $option }}" /></td>
                  <td><button type="button" class="btn btn-danger rem-btn"><i class="fa fa-close"></i></button></td>
                </tr>
                @endforeach
                @endforeach
                @endif
                <tr>
                  <td colspan="3"><button class="btn btn-primary" type="button" id="add-new-filter-btn"><i class="fa fa-plus"></i></button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </form>
  </div>
</div>
</template>
@include('admin.partials.imagemanager')
<script>

  var bus = new Vue(); // comm bus

  var ProductForm = Vue.extend({
    template: '#productform',
    data: function() {
      return {
        images: [],
        selected: null,
        rows: {!! old('variant') ? json_encode(old('variant')) : (isset($variants) ? $variants : '[]') !!},
        packet: {{ isset($product) ? $product->packet : 1 }}
      }
    },
    methods: {
      showModal: function(image) {
        $('#images-modal').modal('show');
        this.selected = image;
      },
      removeImage: function(i) {
        this.images = this.images.slice(0, i).concat(this.images.slice(i + 1));
      },
      addImage: function() {
        this.images.push({filename:undefined,id:undefined});
      },
      addRow: function(){
        this.rows.push({id:"", type:"", purchase_price:"", price:"", mrp: ""});
      },
      removeRow: function(row){
        //console.log(row);
        this.rows = this.rows.slice(0, row).concat(this.rows.slice(row + 1));
      }
    },
    mounted: function() {
      var self = this;
      @if(isset($product))
      $.ajax({
        url: '/admin/product/{{ $product->id }}/images',
        method: 'GET',
        cache: false,
        success: function(response) {
          // console.log(response)
          self.images = response;
        }
      });
      @endif
      bus.$on('changeImage', function(image) {
        if(self.selected) {
          self.selected.id = image.id;
          self.selected.filename = image.filename;
        } else {
          self.images.push(image);
        }
      });
      bus.$on('submitImages', function(images) {
        self.images = self.images.concat(images);
      });
    }
  });

  var child = new Vue({
    parent: vm,
    el: '#v-container',
    render: function(createElement) {
      return createElement('div', {}, [
        createElement(ProductForm),
        createElement(ImageManager, {
          props: {
            bus: bus
          }
        })
      ]);
    }
  });
</script>
@endsection