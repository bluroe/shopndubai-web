@extends('layouts.admin-master')

@section('title', 'Products')

@section('head')
<style>
  .table > tbody > tr.expand_row > td {
    padding: 0;
  }
  .expand_row table {
    margin-bottom: 0;
  }
  .expand_row .tablecontent {
    margin: 10px;
  }
  #product-table tbody td:nth-child(2) {
    max-width: 40px;
  }
  .thumb {
    width: 100%;
  }
</style>
@endsection

@section('content')
<products-view></products-view>
@endsection

@section('vue-components')
<template id="products">
  <div class="row">
    <div class="col-md-12">
      <div class="toolbar">
        <div class="leading">
          <div class="dropdown">
            <a class="head" href="#" data-toggle="dropdown">Products <i class="caret"></i></a>
            <ul class="dropdown-menu">
              <li class="dropdown-header">Type</li>
              <li><a href="#" v-on:click="status = null">All<i class="fa fa-check" v-if="!status"></i></a></li>
              <li><a href="#" v-on:click="status = 'active'">Active<i class="fa fa-check" v-if="status == 'active'"></i></a></li>
              <li><a href="#" v-on:click="status = 'inactive'">Inactive<i class="fa fa-check" v-if="status == 'inactive'"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="form-control search-input">
          <i class="fa fa-search"></i>
          <input type="text" v-model="search" v-on:input="loading = true" id="search" v-on:keyup.enter="pushQuery">
          <button v-on:click="search = ''" class="clear" v-show="search">
            <i class="fa fa-times-circle"></i>
          </button>
        </div>
        <div class="buttons">
          <a class="btn btn-success" v-bind:href="'{{ route('products.create') }}?q=' + search"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
      <div class="table-container">
        <table id="product-table" class="table">
          <thead>
            <tr>
              <th></th>
              <th></th>
              <th>Name</th>
              <th>Category</th>
              <th>Active</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody ref="table_body">
            <template v-for="(product, index) in products">
              <tr v-on:click="product.expanded = !product.expanded">
                <td><button type="button" class="btn btn-success btn-sm"><i v-bind:class="['fa', product.expanded ? 'fa-minus' : 'fa-plus']"></i></button></td>
                <td><img v-bind:src="product.image ? product.image : '/img/no-image.png'" class="thumb" /></td>
                <td>@{{ product.name }}</td>
                <td>@{{ product.category }}</td>
                <td><span v-bind:class="[product.active == 1 ? 'text-success' : 'text-danger']">@{{ product.active == 1 ? 'Yes' : 'No' }}</span></td>
                <td>
                  <a v-bind:href="'/admin/products/' + product.id + '/edit?q=' + search + '&page=' + page" class="btn btn-xs btn-primary" onclick="event.stopPropagation()">Edit</a>&nbsp;<button type="button" class="btn btn-xs btn-danger" v-on:click="deleteProduct($event, product, index)">Delete</button>
                </td>
              </tr>
              <tr class="expand_row" v-if="product.expanded">
                <td colspan="6">
                  <div class="tablecontent">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Type</th>
                          <th>Buy Price</th>
                          <th>Price</th>
                          <th>MRP</th>
                        </tr>
                      </thead>
                      <tbody>
                        @verbatim
                        <tr v-for="variant in product.variants">
                            <td>{{ variant.type }}</td>
                            <td>{{ variant.purchase_price }}</td>
                            <td>{{ variant.price }}</td>
                            <td>{{ variant.mrp }}</td>
                        </tr>
                        @endverbatim
                      </tbody>
                    </table>
                  </div>
                <td>
              </tr>
            </template>
            <tr v-if="products.length == 0">
              <td colspan="8" align="center">No items</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-foot">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous" v-on:click="prevPage()">
              <i class="fa fa-chevron-left"></i>
            </a>
          </li>
          <li>
            <a href="#" aria-label="Next" v-on:click="nextPage()">
              <i class="fa fa-chevron-right"></i>
            </a>
          </li>
        </ul>
        <label>Page</label>
        <input v-model="page" type="number" class="form-control">
        <label>of @{{ Math.ceil(total / limit) }}</label>
      </div>
    </div>
  </div>
</template>

<script>
  var urlParams = new URLSearchParams(window.location.search);
  Vue.component('products-view', {
    data: function() {
      return {
        store_name: '',
        type: 'all',
        products: [],
        search: urlParams.has('q') ? urlParams.get('q') : '',
        total: 0,
        status: null, // active status
        product: {},
        limit: 10,
        page: urlParams.has('page') ? parseInt(urlParams.get('page')) : 1,
        loading: false
      }
    },
    watch: {
      page: function() {
        this.loadProducts();
      },
      loading: function(val, oldVal) {
        var table_body = this.$refs.table_body
        if(val) {
            var height = table_body.getBoundingClientRect().height;
            var lo = document.createElement('div');
            lo.className = 'lo';
            lo.style.height = height + 'px';
            table_body.appendChild(lo);
            this.$refs.lo = lo;
          } else {
            table_body.removeChild(this.$refs.lo);
        }
      },
      search: _.debounce(function() {
        this.loadProducts();
      }, 500),
      status: function() {
        this.loadProducts();
      }
    },
    computed: {
      checked: function() {
        return this.orders.filter(function(v) {
          return v.checked;
        }).length;
      }
    },
    methods: {
      pushQuery: function() {
        history.pushState(null, null, location.pathname + '?' + $.param({q: this.search}));
      },
      prevPage: function() {
        if(this.page > 1) {
          this.page = this.page - 1;
        }
      },
      nextPage: function() {
        var lastPage = Math.ceil(this.total / this.limit);
        if(this.page < lastPage) {
          this.page = this.page + 1;
        }
      },
      loadProducts: function() {
        var self = this;
        this.loading = true;
        var offset = (this.page - 1) * this.limit;
        var conditionals = {offset:offset};
        if(this.search) conditionals.q = this.search;
        if(this.status) conditionals.status = this.status;
        if(this.limit) conditionals.limit = this.limit;
        if(this.xhr) this.xhr.abort();
        this.xhr = $.ajax({
          url: '/admin/products?thumb=true&' + $.param(conditionals),
          method: 'get',
          dataType: 'json',
          cache: false,
          success: function(rsp) {
            // console.log('found products',rsp.count)
            _.each(rsp.data, function(p) {
              p.expanded = false;
            });
            self.products = rsp.data;
            // if(self.total != rsp.count) self.offset = 0;
            self.total = rsp.count;
            self.loading = false;
          }
        })
      },
      deleteProduct: function(event, product, index) {
        event.stopPropagation();
        var self = this;
        var message = 'Are you sure you want to delete this product? This cannot be undone!';
        if(confirm(message)) {
          $.ajax({
            url: `/admin/products/${product.id}`,
            method: 'POST',
            data: {
              _method: 'DELETE',
              _token: '{{ csrf_token() }}'
            },
            success: function(rsp) {
              console.log(rsp);
              self.products = self.products.slice(0, index).concat(self.products.slice(index + 1));
            },
            error: function(rsp) {
              if(rsp.responseJSON) {
                var rsp = rsp.responseJSON;
                if(rsp.status == 'error') {
                  alert(rsp.message);
                }
              } else {
                console.log(rsp.responseText);
              }
            }
          })
        }
      }
    },
    mounted: function() {
      var self = this;
      this.loadProducts();
    },
    template: '#products'
  });

</script>
@endsection
