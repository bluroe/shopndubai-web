@extends('layouts.admin-master')

@section('title', 'Dashboard')

@section('head')
<style>
    .spinholder {
        display: flex;
        justify-content: center;
        align-items: center;
        background: rgba(255,255,255,.4);
        font-size: 20px;
        width: 100%;
        position: absolute;
    }
    .cont {
        padding-top: 15px;
    }
</style>
@endsection

@section('content')
<dashboard></dashboard>
@endsection

@section('vue-components')
<template id="dashboard">
<div class="row">
    <div class="cont">
        <div class="col-md-6">
            <installs-chart></installs-chart>
        </div>
        <div class="col-md-6">
            <sales-chart></sales-chart>
        </div>
    </div>
</div>
</template>
<script src="{{ asset('/js/vendor/Chart.js') }}"></script>
<script>
    Vue.component('installs-chart', {
        data: function() {
            return {
                loading: true,
                days: 30,
                chart: null
            }
        },
        watch: {
            days: function() {
                this.loadChartData();
            }
        },
        methods: {
            loadChartData: function() {
                var self = this;
                this.loading = true;
                $.ajax({
                    url: '/admin/dashboard/installs-data?days=' + this.days,
                    method: 'GET',
                    success: function(rsp) {
                        self.loading = false;
                        if(self.chart) {
                            var chart = self.chart;
                            chart.data.datasets[0].data = rsp.data;
                            chart.data.labels = rsp.labels;
                            chart.update();
                            return;
                        }
                        self.chart = new Chart(self.$refs.canvas.getContext('2d'), {
                            type: 'line',
                            data: {
                                labels: rsp.labels,
                                datasets: [
                                    {
                                        label: 'App installs',
                                        borderColor: 'rgb(46,204,64)',
                                        backgroundColor: 'rgba(46,204,64,.2)',
                                        data: rsp.data,
                                        lineTension: 0
                                    }
                                ]
                            }
                        });
                    }
                })
            }
        },
        mounted: function() {
            this.loadChartData();
        },
        template: `
            <div ref="cont">
                <select v-model="days">
                    <option value="7">Last 7 days</option>
                    <option value="30">Last 30 days</option>
                </select>
                <div class="box">
                    <div v-if="loading" class="spinholder" style="height:300px;">
                        <i class="fa fa-spin fa-spinner"></i>
                    </div>
                    <canvas height="200" ref="canvas" v-show="chart"></canvas>
                </div>
            </div>
        `
    });
    Vue.component('sales-chart', {
        data: function() {
            return {
                loading: true,
                days: 30,
                chart: null
            }
        },
        watch: {
            days: function() {
                this.loadChartData();
            }
        },
        methods: {
            loadChartData: function() {
                var self = this;
                this.loading = true;
                $.ajax({
                    url: '/admin/dashboard/sales-data?days=' + this.days,
                    method: 'GET',
                    success: function(rsp) {
                        self.loading = false;
                        if(self.chart) {
                            var chart = self.chart;
                            chart.data.datasets[0].data = rsp.data;
                            chart.data.labels = rsp.labels;
                            chart.update();
                            return;
                        }
                        self.chart = new Chart(self.$refs.canvas.getContext('2d'), {
                            type: 'line',
                            data: {
                                labels: rsp.labels,
                                datasets: [
                                    {
                                        label: 'Sales',
                                        borderColor: 'rgb(0,116,217)',
                                        backgroundColor: 'rgba(0,116,217,.2)',
                                        data: rsp.data,
                                        lineTension: 0
                                    }
                                ]
                            }
                        });
                    }
                })
            }
        },
        mounted: function() {
            this.loadChartData();
        },
        template: `
            <div ref="cont">
                <select v-model="days">
                    <option value="7">Last 7 days</option>
                    <option value="30">Last 30 days</option>
                </select>
                <div class="box">
                    <div v-if="loading" class="spinholder" style="height:300px;">
                        <i class="fa fa-spin fa-spinner"></i>
                    </div>
                    <canvas height="200" ref="canvas" v-show="chart"></canvas>
                </div>
            </div>
        `
    });

    Vue.component('dashboard', {
        data: function() {
            return {

            }
        },
        methods: {

        },
        mounted: function() {

        },
        template: '#dashboard'
    })
</script>
@endsection