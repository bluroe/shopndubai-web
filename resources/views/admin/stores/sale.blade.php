@extends('layouts.admin-master')

@section('title', 'Sales')

@section('head')
<style>
	.table .dropdown {
		display: inline-block;
	}
	.daterangepicker .calendar {
		float: left;
	}
	.daterangepicker .ranges {
		float: left;
	}
</style>
@endsection

@section('scripts')

<template id="saleview">
	<div>
		<div class="toolbar">
		  <div class="leading">
			<div class="dropdown">
				<a class="head" href="#" data-toggle="dropdown">Sales - {{ Request::get('name') }}<i class="caret"></i></a>
				<ul class="dropdown-menu">
                    <li class="dropdown-header">Status</li>
                    <li><a href="#" v-on:click="changeType(null)">All<i class="fa fa-check" v-if="type == null"></i></a></li>
					<li><a href="#" v-on:click="changeType('paid')">Paid<i class="fa fa-check" v-if="type == 'paid'"></i></a></li>
					<li><a href="#" v-on:click="changeType('unpaid')">Unpaid<i class="fa fa-check" v-if="type == 'unpaid'"></i></a></li>
                    <li class="dropdown-header">Order by</li>
					<li><a href="#" v-on:click="orderBy('latest')">Latest<i class="fa fa-check" v-if="orderby == 'latest'"></i></a></li>
					<li><a href="#" v-on:click="orderBy('oldest')">Oldest<i class="fa fa-check" v-if="orderby == 'oldest'"></i></a></li>
				</ul>
			</div>
		  </div>
	      <div class="form-control search-input">
	        <i class="fa fa-search"></i>
	        <input type="text" v-model="search" v-on:keyup="filterSales" id="search" placeholder="Enter ID to search">
	        <button v-on:click="clearSearch()" class="clear" v-show="search">
	          <i class="fa fa-times-circle"></i>
	        </button>
	      </div>
		</div>
		<div class="toolbar">
			<div class="leading">
				<div class="buttons">
			    	<button class="btn btn-default" v-on:click="print()">
		          		<i class="fa fa-print"></i> Print
		        	</button>
			  	</div>
			</div>
		  	<div class="form-control search-input">
		        <i class="fa fa-calendar"></i>
		        <input id="date" name="date_filter" type="text" placeholder="Enter date to search">
		        <button class="clear" v-on:click="clearDate()">
		          <i class="fa fa-times-circle"></i>
		        </button>
		    </div>
			<div class="buttons">
			    <a class="btn btn-default" data-toggle="modal" data-target="#productModal"><i class="fa fa-filter"></i> Product Filter <span class="prodlbl"></span></a>
			 </div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Order ID</th>
					<th>Item</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Date</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="sale in sales">
					@verbatim			
					<td>#{{ sale.order_id }}</td>
					<td>{{ sale.name }}</td>
					<td>{{ sale.price }}</td>
					<td>{{ sale.qty }}</td>
					<td>{{ sale.created_at }}</td>
					<td>
						<button id="btnStatus" style="width: 60px;" data-toggle="popover" v-bind:class="['btn', (sale.status == 'unpaid' ? 'btn-danger' : 'btn-success' )]" v-on:click="changeStatus(sale)">{{ (sale.status == 'unpaid')  ? 'Unpaid' : 'Paid' }}</button>
					</td>
					@endverbatim
				</tr>
				<tr v-if="sales.length == 0">
					<td colspan="7" align="center">@{{ search ? 'No results' : 'No sales' }}</td>
				</tr>
			</tbody>
		</table>
	<div>
	<div id="productModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Select products to filter</h4>
	      </div>
	      <div class="modal-body">
	      	<label class="control-label">Products </label>
	        <select multiple id="productSelect" style="width:400px">
		        	<option v-bind:value="product.id" v-for="product in products">@{{ product.name }}</option>
    		</select>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-success" v-on:click="reloadSales()">OK</button>
	      </div>
	    </div>

	  </div>
	</div>
</template>
<script>

	var SaleView = Vue.extend({
		data: function() {
			return {
				sales: [],
				type: null,
				orderby: 'latest',
				deliveredClass: 'text-success',
				notDeliveredClass: 'label label-danger',
				search: '',
				products: [],
				start: '',
				stop: '',
				store: {{ Request::get('store') }}
			}
		},
		methods: {
			clearSearch: function() {
				this.search = '';
				this.reloadSales();
			},
			clearDate: function() {
				$('#date').val('');
				this.start = '';
				this.stop = '';
				this.reloadSales();
			},
			filterSales: _.debounce(function() {
				this.reloadSales();
			}, 500),
			orderBy: function(sale) {
				this.orderby = sale;
				this.reloadSales();
			},
			changeType: function(type) {
				this.type = type;
				this.reloadSales();
			},
			changeStatus: function(sale) {
		        var status = sale.status == 'paid' ? 'unpaid' : 'paid';
		        $.post('status?id=' + sale.id + '&status=' + status, {
		          _token: $('[name=csrf_token]').attr('content')
		        }).success(function() {
		          	if(status == 'paid') {
		          		sale.status = "paid";
		          	} else {
		          		sale.status = "unpaid";
		          	}
		        });
			},
			reloadSales: function() {
				var self = this;
				var productId = $( "#productSelect" ).val();
				var data = {
					orderby: this.orderby,
					type: this.type,
					q: this.search,
					product: productId,
					startDate: this.start,
					endDate: this.stop,
					store: this.store
				}
				$.ajax({
					url: '/admin/orders/sale?' + $.param(data),
					method: 'GET',
					cache: false,
					success: function(response) {
						self.sales = response.sales;
						self.products = response.products;
					}
				});
				$('#productModal').modal('hide');
				$('.prodlbl').html($('#productModal option:selected').length);
			},
			print: function() {
				var self = this;
				var productId = $( "#productSelect" ).val();
				var data = {
					orderby: this.orderby,
					type: this.type,
					q: this.search,
					product: productId,
					startDate: this.start,
					endDate: this.stop,
					store: this.store
				}
				$.ajax({
					url: '/admin/orders/sale?' + $.param(data),
					method: 'GET',
					cache: false,
					success: function(response) {
						console.log(response)
						self.sales = response.sales;
						self.products = response.products;
						var rows = [];
						if(response.sales.length != 0) {
							for(var i in response.sales) {
								var row = response.sales[i];
								rows.push([
									'<tr>',
										'<td> #' +row.order_id + '</td>',
										'<td>' + row.name + '</td>',
										'<td>' + row.price + '</td>',
										'<td>' + row.qty + '</td>',
										'<td>' + row.created_at + '</td>',
										'<td>' + (row.status == 'paid' ? 'Paid' : 'Unpaid') + '</td>',
									'</tr>'
								].join(''));
							}
							var table = [
								'<table class="table">',
									'<thead>',
										'<th> Order ID </th>',
										'<th> Product </th>',
										'<th> Price </th>',
										'<th> Qty </th>',
										'<th> Sale Date </th>',
										'<th> Status </th>', 
									'</thead>',
									'<tbody>',
										rows ,
									'</tbody>',
								'</table>'
							].join('');
							var printWindow = window.open();
							printWindow.document.write(
								'<html>' +
								'<head><title>Sale Report</title>' +
								'<link rel="stylesheet" href="/css/bootstrap.css">' +
								'</head><body>' +
								'<h3> Sale </h3>' +
								table +
								'</body>' +
								'</html>'
							)
							printWindow.document.close();
							// printWindow.onload = function() {
					    		printWindow.print();
					    		printWindow.close();
							// }
						} else {
							alert('No Records to print');
						}
					}
				});
			}
		},
		mounted: function() {
			vm.$on('reloadSales', this.reloadSales);
			this.reloadSales();
			var self = this;
			$('#date').daterangepicker({
		      calender_style: "picker_1",
		  	  format: 'YYYY-MM-DD',
		  	  opens: 'left',
		    }, function(start, end, label) {
		      console.log(start.toISOString(), end.toISOString(), label);
		    }).on('apply.daterangepicker', function(ev, picker) {
		        self.start = picker.startDate.format('YYYY-MM-DD');
		        self.stop = picker.endDate.format('YYYY-MM-DD');
		        self.reloadSales();
		    });

		    $("#productSelect").select2({
		    	placeholder: "Select product",
		    	allowClear: true
		    });
		},
		template: '#saleview',
	});
	var child = new Vue({
		parent: vm,
		el: '#v-container',
		render: function(createElement) {
			return createElement(SaleView);
		}
	});
</script>

@endsection
