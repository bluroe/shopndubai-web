@extends('layouts.admin-master')

@section('title', isset($store) ? 'Edit Store - ' . $store->name : 'Create Store')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="title">
			<h3>{{ isset($store) ? 'Edit Store - ' . $store->name : 'Create Store' }}</h3>
		</div>

		@include('admin.partials.alerts')

		@if(isset($store))
		<form method="POST" action="{{ route('stores.update', $store) }}" class="form-horizontal">
			<input type="hidden" name="_method" value="PATCH">
		@else
		<form method="POST" action="{{ route('stores.store') }}" class="form-horizontal">
		@endif
			<div class="col-md-8">
				{{ csrf_field() }}
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" name="name" class="form-control" value="{{ $store->name or old('name') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Street Address</label>
					<div class="col-md-8">
						<input type="text" name="address" class="form-control" value="{{ $store->address or old('address') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Area</label>
					<div class="col-md-8">
						<input type="text" name="area" class="form-control" value="{{ $store->area or old('area') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">PIN</label>
					<div class="col-md-8">
						<input type="text" name="pin" class="form-control" value="{{ $store->pin or old('pin') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Active</label>
					<div class="col-md-8 checkbox">
						<label>
							<input type="hidden" name="active" value="0">
							{!! Form::checkbox('active', 1, isset($store) ? $store->active : old('active')) !!}
						</label>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="form-group">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('scripts')
    <script>
	    @if(isset($store))
	    function renderActive(active) {
	    	var active = parseInt(active);
			return '<span class="' + (active ? 'text-success' : 'text-danger') + '">' + (active ? 'Yes' : 'No') + '</span>';
	    }
	    function renderDelBtn(id) {
	    	return '&nbsp;<button type="button" class="btn btn-danger btn-xs del-btn" data-id=' + id + '>Remove</button>';
	    }
	    var Product = function(product) {
	    	this.product = product;
	    	this._get = function() {
	    		if(product.stores.length) {
	    			var masked = product;
	    			masked.qty = product.stores[0].pivot.qty;
	    			masked.price = product.stores[0].pivot.price;
	    			masked.active = product.stores[0].pivot.active;
	    			return masked;
	    		} else {
	    			return this.product;
	    		}
	    	}
	    	this.get = function(attr) {
	    		if(attr) {
	    			return this._get()[attr];
	    		} else {
	    			return this._get();
	    		}
	    	}
	    };
	    var Container = {};
	    // set values to modal
	    $('#product-modal').on('show.bs.modal', function(e) {
	    	var btn = $(e.relatedTarget);
	    	Container.row = btn.closest('tr');
	    	var id = btn.attr('data-id');
	    	$("#product-modal input[name=product_id]").val(id);
	    	var modal = $(this);
	    	$.getJSON('/admin/products/' + id + '?store={{ $store->id }}', function(data) {
	    		if(data.stores.length) {
		    		modal.find('input[name=qty]').val(data.stores[0].pivot.qty);
		    		modal.find('input[name=price]').val(data.stores[0].pivot.price);
		    		modal.find('input[name=reduce_qty][type=checkbox]').prop('checked', data.stores[0].pivot.reduce_qty ? true : null);
		    		modal.find('input[name=active][type=checkbox]').prop('checked', data.stores[0].pivot.active ? true : null);
	    		} else {
		    		modal.find('input[name=qty]').val('');
		    		modal.find('input[name=price]').val('');
		    		modal.find('input[name=reduce_qty][type=checkbox]').prop('checked', true);
		    		modal.find('input[name=active][type=checkbox]').prop('checked', true);
	    		}
	    	});
	    });
	    // save rule
	    $("#product-modal .btn-primary").click(function() {
	    	var data = {};
	    	$.each($("#product-modal form").serializeArray(), function(index, field) {
	    		data[field.name] = field.value;
	    	});
	    	data._token = '{{ csrf_token() }}';
	    	var id = data.product_id;
	    	delete data.product_id;
	    	$.post('/admin/stores/{{ $store->id }}/products/' + id, data)
	    		.success(function(rsp) {
	    			console.log(rsp);
	    			// console.log(data);
	    			var cols = Container.row.find('td');
	    			cols.eq(1).text(data.qty);
	    			cols.eq(2).text(data.price);
	    			cols.eq(3).html(renderActive(data.active));
	    			$("#product-modal").modal('hide');
	    		})
	    		.error(function(err) {
	    			console.log(err.responseText);
	    		})
	    });
	    @endif
    </script>
@endsection