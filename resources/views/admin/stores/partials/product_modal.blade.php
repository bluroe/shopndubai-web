
			<div class="modal" id="product-modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" @click="closeModal()">&times;</button>
							<h4 class="modal-title">Edit Product Rule</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<form class="form-horizontal">
										<input type="hidden" name="product_id">
										<div class="form-group">
										    <label class="col-md-4 control-label">Qty</label>
										    <div class="col-md-8">
										        <input type="text" class="form-control" v-model="product.qty">
										    </div>
										</div>
										<div class="form-group">
										    <label class="col-md-4 control-label">Price</label>
										    <div class="col-md-8">
										        <input type="text" class="form-control" v-model="product.price">
										    </div>
										</div>
										<div class="form-group">
										    <label class="col-md-4 control-label">Reduce Qty</label>
										    <div class="col-md-8 checkbox">
										    	<label>
											        <input type="checkbox" name="reduce_qty" v-model="product.reduce_qty">
										    	</label>
										    </div>
										</div>
										<div class="form-group">
										    <label class="col-md-4 control-label">Active</label>
										    <div class="col-md-8 checkbox">
										    	<label>
										       		<input type="checkbox" name="active" v-model="product.active">
										        </label>
										    </div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" @click="closeModal()">Close</button>
							<button type="button" class="btn btn-primary" @click="updateProduct(product)">Save</button>
						</div>
					</div>
				</div>
			</div>
