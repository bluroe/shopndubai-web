@extends('layouts.admin-master')

@section('title', 'Stores')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="toolbar">
			<div class="leading">
				<a href="#" class="head">Stores</a>
			</div>
		  <div class="buttons">
		    <a class="btn btn-success" href="{{ route('stores.create') }}"><i class="fa fa-plus"></i> Add new</a>
		  </div>
		  <div class="clearfix"></div>
		</div>
		<table class="table" id="stores-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Location</th>
					<th>Active</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')

<script>
	var storesTable = $('#stores-table').DataTable({
	    ajax: {
	      url: '{{ route('stores.index') }}',
	      dataSrc: 'stores'
	    },
	    columns: [
	      {data:'name'},
	      {data:'area'},
	      {data:'active'},
	      {data:null}
	    ],
	    columnDefs: [
	      {
	        targets: 2,
	        render: function(data, type, full, meta) {
          		return data ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>' ;
	        }
	      },
	      {
	        targets: 3,
	        render: function(data, type, full, meta) {
	          var buttons = '<a href="/admin/orders/sale?store=' + data.id + '&name=' + data.name + '" class="btn btn-xs btn-warning">View Sale</a>&nbsp;'  +
	          '<a href="/admin/stores/' + data.id + '/edit" class="btn btn-xs btn-primary">Edit</a>&nbsp;' +
	            '<button type="button" class="btn btn-xs btn-danger del-btn" data-id="' + data.id + '">Delete</button>';
	          return buttons;
	        }
	      }
	    ]
	});
	storesTable.on('draw.dt', function() {
		$('.del-btn').click(function() {
			var self = $(this);
			var id = self.attr('data-id');
			if(confirm('Are you sure you want to delete this store? This cannot be undone!')) {
				$.post('/admin/stores/' + id, {
					_method: 'DELETE',
					_token: '{{ csrf_token() }}'
				}).success(function(data) {
					console.log(data);
					storesTable.row(self.closest('tr')).remove().draw();
				}).error(function(err) {
					console.log(err.responseText);
				});
			}
		})
	});
</script>

@endsection
