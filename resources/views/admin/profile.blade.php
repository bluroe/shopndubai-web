@extends('layouts.admin-master')

@section('title', 'Profile')

@section('head')
@endsection

@section('scripts')
<template id="settings">
	<div>
		<div class="title">
			<h3>Profile Settings</h3>
		</div>
		<div>
			<div class="row">
				<div class="col-md-8">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-md-4">Firebase Token</label>
							<div class="col-md-8">
								<textarea rows="5" class="form-control" name="fcm_token" v-model="server_token"></textarea>
								<div v-bind:class="'text-success ' + messageClass" v-if="message">@{{ message }}</div>
								<button v-on:click="refreshToken()" style="margin-top:10px;">Refresh</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<h3>Change Password</h3>
      				@include('admin.partials.alerts')
					@if(session('success'))
						<div class="alert alert-success">
							<p><i class="fa fa-check"></i> {{ session('success') }}</p>
						</div>
					@endif
					<form action="{{ route('users.change-password', auth()->user()->id) }}" method="POST" class="form-horizontal">
						<input type="hidden" name="_method" value="PATCH">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="control-label col-md-4">New Password</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="password">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Repeat Password</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</template>
<script>
	var child = new Vue({
		el: '#v-container',
		template: '#settings',
		data: function() {
			return {
				server_token: '{{ auth()->user()->fcm_token }}',
				message: '',
				messageClass: 'text-default'
			}
		},
		methods: {
			refreshToken: function() {
				var self = this;
	            messaging.requestPermission()
	            .then(function() {
	                messaging.getToken()
	                .then(function(currentToken) {
	                	sendTokenImmediately(currentToken);
	                	self.message = 'Token updated.';
	                	self.messageClass = 'text-success';
	                })
	                .catch(function(err) {
	                	self.message = 'Error';
	                	self.messageClass = 'text-danger';;
	                    console.log('An error occurred while retrieving token. ', err);
	                });
	            })
	            .catch(function(err) {
                	self.message = 'Token updated.';
                	self.messageClass = 'text-danger';;
	            });
			}
		},
		parent: vm
	});
</script>
@endsection
