@extends('layouts.admin-master')

@section('title', 'User Management')

@section('content')
  	<div>
      	<div class="toolbar">
      		<div class="leading">
      			<a class="head" href="#">Users</a>
      		</div>
			<div class="buttons">
				<a class="btn btn-success" href="{{ route('users.create') }}"><i class="fa fa-plus"></i> Add new</a>
			</div>
      	</div>

		@if(session('success'))
		<div class="alert alert-success">
			<p><i class="fa fa-info-circle"></i> {{ session('success') }}</p>
		</div>
		@endif

      	<div class="table-container">
	      	<table id="user-table" class="table">
	        	<thead>
		          	<tr>
		            	<th>Email</th>
		            	<th>Level</th>
		            	<th>Created At</th>
		            	<th>Updated At</th>
            			@if(auth()->user()->level == 0)
		            	<th></th>
		            	@endif
		          	</tr>
	        	</thead>
	        	<tbody ref="table_body">
	        		@foreach($users as $user)
	          		<tr>
          				<td>{{ $user->email }}</td>
	            		<td>{{ $user->level == 0 ? 'Admin' : 'User' }}</td>
	            		<td>{{ $user->created_at }}</td>
	            		<td>{{ $user->updated_at }}</td>
            			@if(auth()->user()->level == 0)
	            		<td>
            				@if($user->level != 0 && auth()->user()->id != $user->id)
	            			<form class="form-inline delete-form" action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST" >
	            				<input type="hidden" name="_method" value="DELETE">
	            				{{ csrf_field() }}
	              				<a href="#" data-toggle="modal" data-target="#passwordModal" class="btn btn-xs btn-warning" data-id="{{ $user->id }}">Change Password</a>&nbsp;<button type="submit" class="btn btn-xs btn-danger">Delete</button>
	              			</form>
	              			@endif
	            		</td>
            			@endif
	          		</tr>
	          		@endforeach
	        	</tbody>
	      	</table>
	    </div>
	    <div id="passwordModal" class="modal {{ $errors->any() ? '' : 'fade' }}" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <form class="modal-content form-horizontal" action="" method="POST">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Change Password</h4>
		      </div>
		      <div class="modal-body">
		      		{{ csrf_field() }}
		      		<input type="hidden" name="_method" value="PATCH">

					@if($errors->any())
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
						<p><i class="fa fa-info-circle"></i> {{ $error }}</p>
						@endforeach
					</div>
					@endif
					<div class="form-group">
						<label class="control-label col-md-4">New Password</label>
						<div class="col-md-8">
							<input type="password" class="form-control" name="password">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Repeat Password</label>
						<div class="col-md-8">
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-success">Save</button>
		      </div>
		    </form>

		  </div>
		</div>
    </div>
@endsection

@section('scripts')
<script>
    @if($errors->any())
        $('#passwordModal').modal('show');
    @endif
	$('.delete-form').on('submit', function() {
		return confirm('Are you sure you want to delete this user?');
	});
	$('#passwordModal').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		var url = '/admin/users/' + id + '/change-password';
		$(this).find('form').attr('action', url);
	});
</script>
@endsection