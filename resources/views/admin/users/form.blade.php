@extends('layouts.admin-master')

@section('title', 'Create User')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="title">
        <h3>Create User</h3>
      </div>
      @include('admin.partials.alerts')
      <form method="POST" action="{{ route('users.store') }}" class="form-horizontal">
      	{{ csrf_field() }}
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-4">Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="name" value="{{ old('name') }}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Email</label>
				<div class="col-md-8">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">New Password</label>
				<div class="col-md-8">
					<input type="password" class="form-control" name="password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Repeat Password</label>
				<div class="col-md-8">
					<input type="password" class="form-control" name="password_confirmation">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-8 col-md-offset-4">
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
      	</div>
      </form>
    </div>
  </div>
@endsection