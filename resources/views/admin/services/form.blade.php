@extends('layouts.admin-master')

@section('content')
<div>
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h3>{{ (isset($service) ? 'Edit' : 'Create') . ' Service' }}</h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			@if(isset($service))
			<form class="form-horizontal" method="POST" action="{{ route('services.update', $service->id) }}">
				<input type="hidden" name="_method" value="PATCH">	
			@else
			<form class="form-horizontal" method="POST" action="{{ route('services.store') }}">
			@endif
				{{ csrf_field() }}
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="name" value="{{ $service->name or old('name') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Area</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="area" value="{{ $service->area or old('area') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Total Units</label>
					<div class="col-md-8">
						<input type="number" class="form-control" name="total" value="{{ $service->total or old('total') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Active</label>
					<div class="col-md-8 checkbox">
						<label>
							<input type="hidden" name="active" value="0">
							{!! Form::checkbox('active', 1, isset($service) ? $service->active : old('active')) !!}
						</label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-offset-4">
						<button class="btn btn-success" type="submit">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection