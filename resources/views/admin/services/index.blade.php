@extends('layouts.admin-master')

@section('head')
<style>
.table .form-control {
	width: 50px;
	display: inline-block;
	height: 22px;
	padding-left: 2px;
	padding-right: 2px;
	margin-left: 2px;
	margin-right: 2px;
}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="toolbar">
			<div class="leading">
				<a href="#" class="head">Services</a>
			</div>
		  <div class="buttons">
		    <a class="btn btn-success" href="{{ route('services.create') }}"><i class="fa fa-plus"></i> Add new</a>
		  </div>
		</div>
		<table class="table" id="services-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Area</th>
					<th>Available Units</th>
					<th>Active</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')

<script>
	var storesTable = $('#services-table').DataTable({
	    ajax: {
	      url: '{{ route('services.index') }}',
	      dataSrc: 'services'
	    },
	    rowCallback: function(row, data, index) {
	    	$(row).data('id', data.id);
	    },
	    columns: [
	      {
	      	data: 'name',
	      },
	      {
	      	data: 'area'
	      },
	      {
	      	data: null,
	        render: function(data, type, full, meta) {
          		return [
          			'<button class="btn btn-xs btn-default"><i class="fa fa-chevron-left"></i></button>',
      				'<input class="form-control" type="number" value="' + data.available + '" data-max=' + data.total + '>',
          			'<button class="btn btn-xs btn-default"><i class="fa fa-chevron-right"></i></button>',
          			'of ' + data.total
          		].join('');
	        }
	      },
	      {
	      	data: 'active',
	        render: function(data, type, full, meta) {
	        	var label = data ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>'
          		return '<input type="checkbox" ' + (data ? 'checked' : null) + ' data-id="' + full.id +'"/>';
	        }
	      },
	      {
	      	data: null,
	        render: function(data, type, full, meta) {
	          var buttons = '<a href="/admin/services/' + data.id + '/edit" class="btn btn-xs btn-primary">Edit</a>&nbsp;' +
	            '<button type="button" class="btn btn-xs btn-danger del-btn" data-id="' + data.id + '">Delete</button>';
	          return buttons;
	        }
	      }
	    ]
	}).on('draw.dt', function() {
		function showOkey(el) {
			var okey = $('<span>&nbsp;<i class="fa fa-check text-success"></i></span>');
			setTimeout(function() {
				el.parent().append(okey);
				setTimeout(function() {
					okey.remove();
				}, 2000);
			}, 250);
		}
		function changeAvailability(increase) {
			return function() {
				$(this).unbind().click(function() {
					var btn = $(this);
					var progress = $('<span>&nbsp;<i class="fa fa-refresh fa-spin"></i></span>');
					var input = btn[increase ? 'prev' : 'next']();
					var nextVal = parseInt(input.val()) + (increase ? +1 : -1);
					var within_bounds = input.data('max') >= nextVal && nextVal >= 0;
					if(within_bounds) {
						btn.parent().append(progress)
						$.ajax({
							url: '/admin/services/' + btn.closest('tr').data('id') + '/update-availability',
							data: {
								increase: +increase,
								_token: '{{ csrf_token() }}'
							},
							method: 'POST',
							success: function() {
								input.val(nextVal);
								progress.remove();
								showOkey(btn)
							}
						});
					}
				})
			};
		}
		$(this).find('button:has(.fa-chevron-right)').each(changeAvailability(true));
		$(this).find('button:has(.fa-chevron-left)').each(changeAvailability(false));
		$(this).find('input[type=number]').each(function() {
			$(this).unbind().on('input', _.debounce(function() {
				var progress = $('<span>&nbsp;<i class="fa fa-refresh fa-spin"></i></span>');
				var input = $(this);
				if(this.value != '') {
					var max = input.data('max');
					if(this.value > max || this.value < 0) {
						input.attr('title', 'Error')
							.data('content', 'This field should be between 0 to ' + max)
							.data('placement', 'top')
							.data('trigger', 'focus')
							.popover('show');
						return;
					} else {	
						input.popover('destroy');
					}
					input.parent().append(progress)
					$.ajax({
						url: '/admin/services/' + input.closest('tr').data('id') + '/update-availability',
						data: {
							available: this.value,
							_token: '{{ csrf_token() }}'
						},
						method: 'POST',
						success: function() {
							progress.remove();
							showOkey(input);
						},
						error: function(rsp) {
							if(rsp.status == 422) {
								progress.remove();
							}
						}
					});
				}
			}, 500));
		})
		$(this).find('input[type=checkbox]:visible').each(function() {
			new Switchery(this, {size: 'small'});
			var progress = $('<span>&nbsp;<i class="fa fa-refresh fa-spin"></i></span>');
			this.onchange = function() {
				var sw = $(this);
				sw.parent().append(progress)
				$.ajax({
					url: '/admin/services/' + sw.closest('tr').data('id') + '/update-status',
					data: {
						active: +this.checked,
						_token: '{{ csrf_token() }}'
					},
					method: 'POST',
					success: function() {
						progress.remove();
						showOkey(sw);
					}
				})
			};
		});
	});
	$(document).on('click', '.del-btn', function() {
		var self = $(this);
		var id = self.attr('data-id');
		if(confirm('Are you sure you want to delete this service? This cannot be undone!')) {
			$.post('/admin/service/' + id, {
				_method: 'DELETE',
				_token: '{{ csrf_token() }}'
			}).success(function(data) {
				console.log(data);
				storesTable.row(self.closest('tr')).remove().draw();
			}).error(function(err) {
				console.log(err.responseText);
			});
		}
	});
</script>

@endsection