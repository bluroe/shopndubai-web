@extends('layouts.admin-master')

@section('title', 'Banners')

@section('scripts')

<template id="banners">
	<div>
		<div class="modal" tabindex="-1" role="dialog" id="banner-modal">
		  <div class="modal-dialog" role="document">
		    <form class="modal-content" v-bind:action="'/admin/banners/' + (banner ? banner.id : '')" method="POST" enctype="multipart/form-data">
		      <div class="modal-header">
		        <button type="button" class="close" @click="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">@{{ modal_title }}</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="form-horizontal">
		      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<input type="hidden" name="_method" value="PATCH" v-if="banner">
			        <div class="form-group">
			        	<label class="control-label col-md-4">Name</label>
			        	<div class="col-md-8">
			        		<input type="text" name="name" class="form-control" />
			        	</div>
			        </div>
			        <div class="form-group">
			        	<label class="control-label col-md-4">Image</label>
			        	<div class="col-md-8">
			        		<div>
			        			<input type="file" name="image" @change="changeImage($event)">
			        		</div>
			        		<img v-bind:src="preview ? preview : '/img/no-image.png'" style="max-width:100%;margin-top:10px;">
			        	</div>
			        </div>
			    </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" @click="closeModal()">Close</button>
		        <button type="submit" class="btn btn-primary">Save changes</button>
		      </div>
		    </form><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="toolbar">
		  <div class="pull-right">
		    <button class="btn btn-success" @click="openModal()"><i class="fa fa-plus"></i> Add New</button>
		  </div>
		  <div class="clearfix"></div>
		</div>

		<table id="advertisements-table" class="table">
		  <thead>
		    <tr>
		    	<th></th>
		      	<th>Name</th>
		      	<th>Date</th>
		      	<th>Actions</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<tr v-for="(bn, index) in bans">
		  		<td><img :src="bn.image" style="max-height:60px;"></td>
		  		<td>@{{ bn.name }}</td>
		  		<td>@{{ bn.created_at }}</td>
		  		<td><button class="btn btn-danger btn-xs" @click="deleteBn(bn, index)">Delete</button></td>
		  	</tr>
		  	<tr v-if="!bans.length">
		  		<td colspan="5" align="center">No banners</td>
		  	</tr>
		  </tbody>
		</table>
	</div>
</template>

<script type="text/javascript">
	var child = new Vue({
		el: '#v-container',
		data: {
			bans: [],
			modal_title: 'Create Banner',
			preview: null,
			banner: null
		},
		methods: {
			openModal: function() {
				$('#banner-modal').modal('show');
			},
			closeModal: function() {
				this.name = null;
				this.image = null;
				this.preview = null;
				$('#banner-modal').modal('hide');
			},
			changeImage: function(e) {
				var self = this;
				var file = e.target.files[0]
				var reader = new FileReader();
				reader.onloadend = function() {
					self.preview = reader.result;
					console.log('loaded')
				}
				reader.readAsDataURL(file);
			},
			saveBanner: function() {
				var data = {
					_token: '{{ csrf_token() }}',
					image: this.image,
					name: this.name
				}
				console.log(data)
				$.ajax({
					url: '/admin/banners',
					method: 'POST',
					data: data,
    				contentType: 'multipart/form-data',
					success: function(rsp) {
						console.log(rsp);
					}
				})
			},
			deleteBn: function(bn, index) {
				var self = this;
				if(confirm('Are you sure you want to delete this banner?')) {
					$.ajax({
						url: '/admin/banners/' + bn.id,
						method: 'POST',
						data: {
							_token: '{{ csrf_token() }}',
							_method: 'DELETE'
						},
						success: function(rsp) {
							console.log(rsp)
							self.bans = self.bans.slice(0, index).concat(self.bans.splice(index + 1));
						}
					})
				}
			}
		},
		mounted: function() {
			var self = this;
        	$.ajax({
        		method: 'GET',
        		url: '/admin/banners',
        		success: function(rsp) {
        			console.log(rsp)
        			self.bans = rsp;
        		}
        	})
		},
		template: '#banners'
	});
</script>

@endsection