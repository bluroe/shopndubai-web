<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="{{ csrf_token() }}">

        <title>Tely Bazaar Admin Panel | @yield('title')</title>
        <link rel="icon" href="{{ asset('/favicon.png') }}">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
        <!-- Select 2 -->
        <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/css/notie.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/dropzone.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap-colorpicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/iziToast.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/switchery.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/main.css') }}">
        <link rel="manifest" href="{{ asset('/manifest.json') }}">
        <script src="{{ asset('/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        @yield('head')
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="root">
            <header class="navbar navbar-default navbar-fixed-top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#head-nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="logo"><img src="{{ asset('/img/tely_strip.png') }}" style="display: inline-block;vertical-align: top;"> Admin Panel</a>
                </div>
                <div class="collapse navbar-collapse" id="head-nav">
                    <ul class="nav navbar-nav navbar-right">
                        <notifications></notifications>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }} <i class="caret"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('/admin/profile') }}">Account</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </header>
            <div id="wrapper" class="toggled">
                <aside class="sidebar">
                  <div class="menu_section" style="padding-top:10px;">
                    <ul class="primary-menu">
                        <li>
                          <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="fa fa-list"></i> Catalog<span class="icon"><i class="fa fa-chevron-right"></i></span></a>
                            <ul class="child_menu">
                              <li><a href="{{ route('categories.index') }}">Categories</a></li>
                              <li><a href="{{ route('products.index') }}">{{ Request::path() == 'admin/products' && Request::get('store') ? 'All ' : '' }}Products<span class="shortcut-highlight">T + P</span></a></li>
                              <li><a href="{{ route('filters.index') }}">Filters</a></li>
                            </ul>
                        </li>
                        <li>
                          <a href="{{ route('orders.index') }}"><i class="fa fa-bars"></i> Orders<span class="shortcut-highlight">T + O</span></a>
                        </li>
                        <li>
                          <a href="{{ route('stores.index') }}"><i class="fa fa-building"></i> Stores<span class="shortcut-highlight">T + S</span></a>
                        </li>
                        <li>
                          <a href="{{ route('customers.index') }}"><i class="fa fa-users"></i> Customers</a>
                        </li>
                        <li>
                          <a href="{{ route('services.index') }}"><i class="fa fa-briefcase"></i>  Services</a>
                        </li>

                        <li>
                            <a href="{{ route('offers.index') }}"><i class="fa fa-star"></i> Offers</a>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="fa fa-flag"></i>  Advertising <span class="icon"><i class="fa fa-chevron-right"></i></span></a>
                            <ul class="child_menu">
                                <li>
                                    <a href="{{ route('advertisements.index') }}"><i class="fa fa-default"></i> Ads</a>
                                </li>
                                <li>
                                    <a href="{{ route('banners.index') }}"><i class="fa fa-default"></i> Banners</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;"><i class="fa fa-cog"></i>  Settings <span class="icon"><i class="fa fa-chevron-right"></i></span></a>
                            <ul class="child_menu">
                                <li>
                                    <a href="{{ route('settings.index') }}"><i class="fa fa-default"></i> Settings</a>
                                </li>
                                <li>
                                    <a href="{{ route('users.index') }}"><i class="fa fa-default"></i> User Management</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                  </div>

                </aside>
                <main class="content">
                    @yield('wrapper')
                    <div class="container-fluid">
                        <div id="v-container">
                        @yield('content')
                        </div>
                    </div>
                </main>
            </div>
        </div>

        <script src="{{ asset('/js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/dataTables.bootstrap.min.js') }}"></script>

        <script src="{{ asset('/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/parsley.min.js') }}"></script>

        <script src="{{ asset('/js/vendor/notie.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/holder.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/dropzone.js') }}"></script>
        <script src="{{ asset('/js/vendor/moment.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/moment-timezone.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/vue.js') }}"></script>
        <script src="{{ asset('/js/vendor/daterangepicker.js') }}"></script>
        <script src="{{ asset('/js/vendor/bootstrap-colorpicker.min.js') }}"></script>
        {{-- <script src="{{ asset('/js/vendor/bootstrap3-typeahead.min.js') }}"></script> --}}
        <script src="{{ asset('/js/vendor/bloodhound.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/typeahead.jquery.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/iziToast.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/lodash.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/select2.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/switchery.min.js') }}"></script>
        <script src="{{ asset('/js/custom.js') }}"></script>
        <script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>
        @yield('vue-components')
        <template id="notifications">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell"></i>
                    <span class="lbl" v-if="notifications">@{{ notifications }}</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="dropdown-header">Orders</li>
                    <li><a href="{{ route('orders.index') }}">Pending  <span class="label label-danger">@{{ orders }}</span></a></li>
                </ul>
            </li>
        </template>
        <script>
            Vue.component('notifications', {
                data: function() {
                    return {
                        orders: 0,
                        fcm_token: ''
                    }
                },
                computed: {
                    notifications: function() {
                        return this.orders;
                    }
                },
                methods: {
                    loadNotifications: function() {
                        var self = this;
                        console.log('loading notifications')
                        $.ajax({
                            method: 'GET',
                            url: '{{ url('/admin/notifications') }}',
                            success: function(rsp) {
                                self.orders = rsp.pending_orders;
                            }
                        });
                    }
                },
                mounted: function() {
                    this.loadNotifications();
                    this.$parent.$on('reloadOrders', this.loadNotifications);
                },
                template: '#notifications'
            });
            var vm = new Vue({
                el: '#root',
                methods: {
                    orderStatusMessage: function(status) {
                        switch(status) {
                            case 0:
                                return 'Ordered';
                            case 1:
                                return 'In transit';
                            case 2:
                                return 'Delivered';
                            default:
                                return 'N/A';
                        }
                    }
                }
            });
            function sendTokenToServer(currentToken) {
                var userToken = '{{ auth()->user()->fcm_token }}';
                if(currentToken != userToken) {
                    sendTokenImmediately(currentToken);
                }
            }
            function sendTokenImmediately(token, callback) {
                $.post('/admin/update-token', {
                    token: token,
                    _token: '{{ csrf_token() }}'
                })
                .success(function() {
                    console.log('token updated');
                    if(typeof callback == 'function') callback();
                });
            }
            function playSound() {
                var audio = new Audio('/sound/ding.ogg');
                audio.play();
            }

            var config = {
                apiKey: "AIzaSyBv_M3sd0toR-grFYVhsD96kNEUs9mMvV4",
                authDomain: "shopndubai-1ae37.firebaseapp.com",
                databaseURL: "https://shopndubai-1ae37.firebaseio.com",
                storageBucket: "shopndubai-1ae37.appspot.com",
                messagingSenderId: "970348644866"
            };
            firebase.initializeApp(config);
            var messaging = firebase.messaging();

            messaging.requestPermission()
            .then(function() {
                messaging.getToken()
                .then(function(currentToken) {
                    sendTokenToServer(currentToken);
                })
                .catch(function(err) {
                    console.log('An error occurred while retrieving token. ', err);
                });
            })
            .catch(function(err) {
                console.log('Unable to get permission to notify.', err);
            });

            messaging.onTokenRefresh(function() {
              messaging.getToken()
              .then(function(refreshedToken) {
                sendTokenToServer(refreshedToken);
              })
              .catch(function(err) {
                console.log('Unable to retrieve refreshed token ', err);
              });
            });

            messaging.onMessage(function(payload) {
                iziToast.info({
                    title: payload.notification.title,
                    message: payload.notification.body
                });
                playSound();
                vm.$emit('reloadOrders');
            });
      </script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-80054129-6', 'auto');
          ga('send', 'pageview');
        </script>

      @yield('scripts')

    </body>
</html>
