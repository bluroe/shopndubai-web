<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<!-- Latest compiled and minified CSS -->
	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/theme.css') }}" rel="stylesheet">
	<style type="text/css">
		#loginBox {
			width: 100%;
			max-width: 400px;
		}
		.panel-head {
			padding: 10px 15px;
		}
		.panel-head h3 {
			margin: 0;
		}
		.button {
			background-color: #F2544F;
			border-color: #E6504B;
			color: #fff;
		}
		.button:hover, .button:focus, .button:hover:focus {
			background-color: #E6504B;
			border-color: #D94B47;
			color: #fff;
		}
		.button:active {
			background-color: #D94B47;
			border-color: #CC4743;
		}
		.vertical-offset-100{
		    padding-top:100px;
		}
		.box {
			background: #fff;
			margin: 100px auto;
			padding: 10px;
		}
		.box {
			position: relative;
		}
		.box:before, .box:after {
			z-index: -1;
			position: absolute;
			content: "";
			bottom: 15px;
			left: 10px;
			width: 50%;
			top: 80%;
			max-width: 300px;
			background: #777;
			box-shadow: 0 15px 10px #777;
			transform: rotate(-3deg);
		}
		.box:after {
			transform: rotate(3deg);
			right: 10px;
			left: auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="panel panel-default box" id="loginBox">
		  	<div class="panel-head">
		    	<h3>Please sign in</h3>
		 	</div>
		  	<div class="panel-body">
		  		@if($errors->any())
		  		<div class="alert alert-danger">
		  			<i class="glyphicon glyphicon-exclamation-sign"></i> {{ $errors->first() }}
		  		</div>
		  		@endif
		    	<form accept-charset="UTF-8" role="form" action="{{ url('/login') }}" method="POST">
			    	{{ csrf_field() }}
			    	@if(Request::has('next'))
			    	<input type="hidden" name="next" value="{{ Request::get('next') }}" />
			    	@endif
	                <fieldset>
			    	  	<div class="form-group">
			    		    <input class="form-control" placeholder="Email" name="email" type="email" value="{{ old('email') }}">
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control" placeholder="Password" name="password" type="password" value="">
			    		</div>
			    		<div class="checkbox">
			    	    	<label>
			    	    		<input name="remember" type="checkbox" value="Remember Me"> Remember Me
			    	    	</label>
			    	    </div>
			    		<input class="btn btn-lg button btn-block" type="submit" value="Login">
			    	</fieldset>

		    	    <div style="padding-top:10px;">
		    	    	<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot password?</a>
		    	    </div>
		      	</form>
		    </div>
		</div>
	</div>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-80054129-6', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
